import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Receipts from '@/pages/Receipts'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Receipts', () => {
  const getAllReceipts = jest.fn()

  const store = new Vuex.Store({
    modules: {
      AllReceipts: {
        state: {
          isLoading: false,
          receipts: []
        },
        actions: { getAllReceipts }
      }
    }
  })

  const wrapper = shallowMount(Receipts, {
    localVue,
    store
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should call "getAllReceipts" on mounting', () => {
    expect(getAllReceipts).toHaveBeenCalledTimes(1)
  })
})
