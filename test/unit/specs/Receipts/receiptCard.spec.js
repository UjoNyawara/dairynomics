import { shallowMount } from '@vue/test-utils'
import ReceiptCard from '@/pages/Receipts/ReceiptCard'
import * as helperFunctions from '@/utils/helperFunctions'

const receipt = {
  invoiceNumber: '#12345',
  amountToBePaid: 9000,
  createAt: '2019-07-01T06:51:46.357Z',
  order: {
    comments: 'Lorem Ipsum sit ammet dolor'
  }
}

const wrapper = shallowMount(ReceiptCard, {
  propsData: {
    receipt
  }
})
describe('ReceiptCard', () => {
  helperFunctions.openModal = jest.fn()
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
  it('calls "openModal" when handleOpenModalOnClick is called with "receipt"', () => {
    wrapper.vm.handleOpenModalOnClick('receipt', 'c79a1062-d5d4-4df4-a41c-65b6c054c582')
    expect(helperFunctions.openModal).toHaveBeenCalledWith('receipt', 'c79a1062-d5d4-4df4-a41c-65b6c054c582')
  })
})
