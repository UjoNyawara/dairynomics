import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import ViewIndividualFarmPage from '@/pages/ViewIndividualFarm'
import mockStore from '@/store/modules/mocks/IndividualFarmStore'

const localVue = createLocalVue()
const router = new VueRouter()
localVue.use(Vuex)
localVue.use(VueRouter)

jest.mock('@/store')

describe('Individual Farm Page.vue', () => {
  const wrapper = shallowMount(ViewIndividualFarmPage, {store: mockStore, localVue, router})

  it('should render without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
