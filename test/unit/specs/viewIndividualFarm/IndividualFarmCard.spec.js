import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import IndividualFarmCard from '@/pages/ViewIndividualFarm/IndividualFarmCard'
import mockStore from '@/store/modules/mocks/IndividualFarmStore'
const localVue = createLocalVue()

localVue.use(Vuex)
jest.mock('@/store')

describe('Individual Farm Card.vue', () => {
  const wrapper = shallowMount(IndividualFarmCard, { store: mockStore, localVue })

  it('should render without errors', (done) => {
    expect(wrapper.vm.$el).toMatchSnapshot()
    done()
  })
})
