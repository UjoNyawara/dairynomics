import { shallowMount } from '@vue/test-utils'
import IndividualItemHeader from '@/pages/IndividualItem/IndividualItemHeader.vue'

describe('IndividualItemHeader', () => {
  let wrapper

  it('should render the IndividualItemHeader component', () => {
    wrapper = shallowMount(IndividualItemHeader, { propsData: {
      headerInfo: {
        weigth: 6,
        fieldTagNum: 3456,
        bodyCondition: 'Medium',
        sex: 'Male',
        description: 'Dairy cow',
        market: '',
        postOn: { date: '' },
        breed: '6',
        price: '1200'
      }
    }})
    expect(wrapper.vm).toBeTruthy()
  })
})
