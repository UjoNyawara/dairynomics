import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Vuelidate from 'vuelidate'
import AverageMilkPerformance from '@/pages/AverageMilkProductionPage/AverageMilkPerformance.vue'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Vuelidate)

jest.mock('@/mixins/isLoggedInMixin')

describe('AverageMilkPreformance', () => {
  let wrapper
  beforeAll(() => {
    wrapper = shallowMount(AverageMilkPerformance, {
      data: {
        salesPerformance: 80,
        responseHeader: '',
        responseBody: '',
        socials: ''
      }
    })
  })
  it('should update responseHeader salesPerformance is > 94', () => {
    wrapper = shallowMount(AverageMilkPerformance)
    wrapper.setData({ salesPerformance: '' })
    expect(wrapper.vm.responseData.result).toBe('Below Average')
    wrapper.setData({ salesPerformance: 100 })
    wrapper.vm.updateResponse()
    expect(wrapper.vm.responseData.result).toBe('Amazing!')
  })
  it('should update responseHeader salesPerformance is >= 85', () => {
    wrapper = shallowMount(AverageMilkPerformance)
    expect(wrapper.vm.responseData.result).toBe('Below Average')
    wrapper.setData({ salesPerformance: 86 })
    wrapper.vm.updateResponse()
    expect(wrapper.vm.responseData.result).toBe('Er..Ok')
  })
  it('should update responseHeader salesPerformance is < 85', () => {
    wrapper = shallowMount(AverageMilkPerformance)
    expect(wrapper.vm.responseData.result).toBe('Below Average')
    wrapper.setData({ salesPerformance: 80 })
    wrapper.vm.updateResponse()
    expect(wrapper.vm.responseData.result).toBe('Below Average')
  })
  // it('should trigger the reload method', () => {
  //   wrapper = shallowMount(AverageMilkPerformance)
  //   wrapper.find('a').trigger('click')
  //   // wrapper.vm.reloadPage()
  //   // expect(spy).toBeCalled()
  // })
})
