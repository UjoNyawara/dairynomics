import { shallowMount, createLocalVue } from '@vue/test-utils'
import Message from '@/pages/Message/index'
import Vuex from 'vuex'
import PeerModel from '@/utils/peerModel/index.js'

let getters
let actions
let wrapper
let localVue = createLocalVue()
localVue.use(Vuex)

const mockMessages = [
  {
    message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
    date: '25 October 2018, 12:35am',
    doReverse: false,
    status: true
  },
  {
    message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
    date: '25 October 2018, 12:35am',
    doReverse: true
  }
]
const mockUsers = [{
  'id': '3564',
  'first_name': 'Kristoffer',
  'last_name': 'Stoltenberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}, {
  'id': '3554',
  'first_name': 'Jolly',
  'last_name': 'Gratham',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}]

describe('Message', () => {
  beforeAll(() => {
    actions = {
      postMessageAction: jest.fn(),
      receiveMessageAction: jest.fn(),
      loadUsersAction: jest.fn(),
      openConnectionAction: jest.fn(),
      sendTypingSignal: jest.fn(),
      loadMessagesAction: jest.fn()
    }
    getters = {
      allMessages: () => mockMessages,
      isFetching: state => ({
        page: false,
        messages: false
      }),
      chatMate: state => mockUsers[1],
      USER_PROFILE: state => mockUsers[0],
      peerModel: state => new PeerModel(),
      isTyping: state => false,
      getMessagesMeta: state => {},
      allUsers: () => mockUsers
    }
    wrapper = shallowMount(Message, {
      localVue,
      store: new Vuex.Store({ actions, getters })
    })
  })

  it('renders without an error', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('hide sideBar when show sidebar button is clicked', () => {
    wrapper.setProps({
      isShow: true
    })
    const toggleSideBarSpy = jest.spyOn(wrapper.vm, 'toggleSideBar')
    toggleSideBarSpy()
    expect(wrapper.vm.sideBarClasses).toBe('sidebar')
  })

  it('show sidebar when show sidebar button is clicked', () => {
    wrapper.setProps({
      isShow: false
    })
    const toggleSideBarSpy = jest.spyOn(wrapper.vm, 'toggleSideBar')
    toggleSideBarSpy()
    expect(wrapper.vm.sideBarClasses).toBe('sidebar')
  })

  it('should set the appropriate chat-wrapper class if messages is empty', () => {
    wrapper.setProps({
      messages: []
    })
    expect(wrapper.vm.getChatWrapperClasses).toBe('chat-wrapper')
  })

  it('should call postMessage', () => {
    const postMessageSpy = jest.spyOn(wrapper.vm, 'postMessage')
    postMessageSpy()
    expect(wrapper.vm.postMessage).toBeCalled()
  })

  it('should call postMessage when send button is clicked', async () => {
    wrapper.setData({
      message: 'Joshua is not in the building'
    })
    const postMessageSpy = jest.spyOn(wrapper.vm, 'postMessage')
    jest.spyOn(wrapper.vm, 'postMessageAction')
    await postMessageSpy()
    expect(Message.data().message).toEqual('')
    expect(Message.data().isPosting).toBeFalsy()
    expect(wrapper.vm.postMessage).toBeCalled()
  })

  it('should call sendSignal', () => {
    const sendSignalSpy = jest.spyOn(wrapper.vm, 'sendSignal')
    sendSignalSpy()
    expect(wrapper.vm.sendSignal).toBeCalled()
  })

  it('should call updatePageData', () => {
    const updatePageDataSpy = jest.spyOn(wrapper.vm, 'updatePageData')
    updatePageDataSpy()
    expect(wrapper.vm.updatePageData).toBeCalled()
  })

  it('should call scrollHandler', () => {
    wrapper.setProps({
      getMessagesMeta: {
        last_page: 2
      }
    })
    const event = {
      target: {
        scrollTop: 0,
        scrollHeight: 50,
        clientHeight: 70
      }
    }
    const scrollHandlerSpy = jest.spyOn(wrapper.vm, 'scrollHandler')
    scrollHandlerSpy(event)
    expect(wrapper.vm.scrollHandler).toBeCalled()
  })

  it('should call changeHandler', () => {
    const event = { target: { value: 'Some text' } }
    const changeHandlerSpy = jest.spyOn(wrapper.vm, 'changeHandler')
    changeHandlerSpy(event)
    expect(wrapper.vm.changeHandler).toBeCalled()
  })

  it('should call showUnsentMessageIcon but not show unsent message icon', () => {
    const showUnsentMessageIconSpy = jest.spyOn(wrapper.vm, 'showUnsentMessageIcon')
    showUnsentMessageIconSpy(mockUsers[0].id)
    expect(mockUsers[0].hasUnsentMessage).toBeFalsy()
    expect(wrapper.vm.showUnsentMessageIcon).toBeCalled()
  })

  it('should call showUnsentMessageIcon and show unsent message icon', () => {
    const showUnsentMessageIconSpy = jest.spyOn(wrapper.vm, 'showUnsentMessageIcon')
    wrapper.setData({
      messages: {[mockUsers[0].id]: 'Something else'}
    })
    showUnsentMessageIconSpy(mockUsers[0].id)
    expect(mockUsers[0].hasUnsentMessage).toBeTruthy()
    expect(wrapper.vm.showUnsentMessageIcon).toBeCalled()
  })

  it('should call sendSignal', () => {
    const sendSignalSpy = jest.spyOn(wrapper.vm, 'sendSignal')
    sendSignalSpy(true)
    expect(wrapper.vm.sendSignal).toBeCalled()
  })

  it('should call scrollToBeginning', () => {
    const scrollToBeginningSpy = jest.spyOn(wrapper.vm, 'scrollToBeginning')
    scrollToBeginningSpy()
    expect(wrapper.vm.scrollToBeginning).toBeCalled()
  })

  it('should call scrollToBeginning with page number greater than one', () => {
    wrapper.setData({
      page: 2
    })
    const scrollToBeginningSpy = jest.spyOn(wrapper.vm, 'scrollToBeginning')
    scrollToBeginningSpy()
    expect(wrapper.vm.scrollToBeginning).toBeCalled()
  })
})
