
import { shallowMount } from '@vue/test-utils'
import ContentHeader from '@/pages/Message/ContentHeader'
import TopContainer from '@/pages/Message/TopContainer'
import UserDetails from '@/pages/Message/UserDetails'

describe('ContentHeader', () => {
  let wrapper
  beforeAll(() => (wrapper = shallowMount(ContentHeader)))

  it('renders without error', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('renders with props', () => {
    wrapper = shallowMount(ContentHeader, {
      propsData: {
        toggleSideBar: jest.fn()
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(typeof wrapper.props().toggleSideBar).toBe('function')
  })

  it('renders with two child components', () => {
    wrapper = shallowMount(ContentHeader)
    expect(wrapper.find(TopContainer).exists()).toBeTruthy()
    expect(wrapper.find(UserDetails).exists()).toBeTruthy()
  })
})
