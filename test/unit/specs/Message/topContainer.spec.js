import { shallowMount } from '@vue/test-utils'
import TopContainer from '@/pages/Message/TopContainer'

describe('TopContainer', () => {
  let wrapper
  beforeAll(() => (wrapper = shallowMount(TopContainer, {
    propsData: {
      isSideBar: false
    }
  })))

  it('renders without an error', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should return top-container content as computed classes', () => {
    expect(wrapper.vm.getSideBarClasses).toBe('top-container content')
  })

  it('should return top-container sideBar as computed classes', () => {
    wrapper.setProps({
      isSideBar: true
    })
    expect(wrapper.vm.getSideBarClasses).toBe('top-container sidebar')
  })
})
