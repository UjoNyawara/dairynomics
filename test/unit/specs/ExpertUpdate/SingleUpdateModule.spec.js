import SingleUpdateModule from '@/store/modules/SingleUpdateModule'
import { mock } from '@/store/modules/mocks/mockInstance'

jest.mock('@/store')
const state = {}

const apiUrl = 'https://beta.cowsoko.com/api/v1'

const successResponse = {
  success: true,
  update: [],
  meta: 'joshua',
  updateId: 234,
  data: {
    comments: ['joshua', 'fredrick']
  }

}

const failureResponse = {
  success: false,
  error: 'error',
  message: 'error'
}

describe('SingleUpdate Module test', () => {
  describe('Mutations test for singleupdate', () => {
    const {mutations: {POPULATE_UPDATE_DATA, POPULATE_UPDATE_ERROR, populateComments, SET_COMMENTING_ERROR, SET_UPDATE_LIKING_ERROR, changeCommentEditError, changeReplyEditError, updateCommentId, setCurrentUpdateId, changeUserId, changeReplyId, changeCommentDeleteError, changeUpdateCreatorId, POPULATE_CREATOR_DETAILS, POPULATE_CREATOR_ERROR, COMMENT_LIKE_ERROR, SET_COMMENT_REPLY_ERROR, SET_COMMENT_UNLIKE_ERROR, SET_UPDATE_UNLIKE_ERROR,
      loadingComments,
      SET_UPDATE_REPLY_UNLIKE_ERROR}} = SingleUpdateModule

    it('POPULATE_UPDATE_DATA should return set userupdate as payload', () => {
      POPULATE_UPDATE_DATA(state, 'joshua')
      expect(state.UserUpdate).toEqual('joshua')
    })

    it('POPULATE_UPDATE_ERROR test', () => {
      POPULATE_UPDATE_ERROR(state, 'error')
      expect(state.updateFetchError).toEqual('error')
    })

    it('populateComments test', () => {
      populateComments(state, {comments: ['joshua'],
        meta: {
          current_page: 1
        }})
      expect(state.updateComments[0]).toEqual('joshua')
    })

    it('populateComments test', () => {
      const newState = {
        updateComments: 'joshua'
      }
      populateComments(newState, {comments: ['joshua'],
        meta: {
          current_page: 5
        }})
      expect(state.updateComments[0]).toEqual('joshua')
    })

    it('SET_COMMENTING_ERROR TEST', () => {
      SET_COMMENTING_ERROR(state, 'error')
      expect(state.commentingError).toEqual('error')
    })

    it('loadingComments TEST', () => {
      loadingComments(state, false)
      expect(state.updateCommentsLoading).toBeFalsy()
    })

    it('SET_UPDATE_LIKING_ERROR', () => {
      SET_UPDATE_LIKING_ERROR(state, 'error')
      expect(state.updateLikingError).toEqual('error')
    })

    it('changeCommentEditError test', () => {
      changeCommentEditError(state, 'error2')
      expect(state.commentEditError).toEqual('error2')
    })

    it('changeReplyEditError test', () => {
      changeReplyEditError(state, 'error')
      expect(state.replyEditError).toEqual('error')
    })

    it('updateCommentId test', () => {
      updateCommentId(state, 45)
      expect(state.commentId).toEqual(45)
    })

    it('setCurrentUpdateId test', () => {
      setCurrentUpdateId(state, 21)
      expect(state.updateId).toEqual(21)
    })

    it('changeUserId test', () => {
      changeUserId(state, 20)
      expect(state.userId).toEqual(20)
    })

    it('changeReplyId test', () => {
      changeReplyId(state, 27)
      expect(state.replyId).toEqual(27)
    })

    it('changeCommentDeleteError test', () => {
      changeCommentDeleteError(state, 'error2')
      expect(state.commentDeleteError).toEqual('error2')
    })

    it('changeUpdateCreatorId test', () => {
      changeUpdateCreatorId(state, 99)
      expect(state.creatorId).toEqual(99)
    })

    it('POPULATE_CREATOR_DETAILS test', () => {
      POPULATE_CREATOR_DETAILS(state, 'joshua')
      expect(state.updateCreatorDetails).toEqual('joshua')
    })

    it('POPULATE_CREATOR_ERROR test', () => {
      POPULATE_CREATOR_ERROR(state, 'error')
      expect(state.creatorError).toEqual('error')
    })

    it('COMMENT_LIKE_ERROR test', () => {
      COMMENT_LIKE_ERROR(state, 'error')
      expect(state.commentLikeError).toEqual('error')
    })

    it('SET_COMMENT_REPLY_ERROR test', () => {
      SET_COMMENT_REPLY_ERROR(state, 'error')
      expect(state.commentReplyError).toEqual('error')
    })

    it('SET_COMMENT_UNLIKE_ERROR TEST', () => {
      SET_COMMENT_UNLIKE_ERROR(state, 'error')
      expect(state.commentUnlikeError).toEqual('error')
    })

    it('SET_UPDATE_UNLIKE_ERROR test', () => {
      SET_UPDATE_UNLIKE_ERROR(state, 'error')
      expect(state.updateUnlikeError).toEqual('error')
    })

    it('SET_UPDATE_REPLY_UNLIKE_ERROR test', () => {
      SET_UPDATE_REPLY_UNLIKE_ERROR(state, 'error')
      expect(state.updateReplyUnlikeError).toEqual('error')
    })
  })

  describe('SingleUpdateModule Getters', () => {
    const state = {
      UserUpdate: {name: 'joshua'},
      updateComments: ['joshua'],
      commentId: 345,
      replyId: 123,
      updateCommentsMeta: 'joshua',
      updateCreatorDetails: {name: 'fredrick'},
      updateCommentsLoading: true
    }
    const {getUpdate, getComments, getSelectedComment, getSelectedReply, getUpdateCreator, getCommentsMeta, getCommentsLoading} = SingleUpdateModule.getters

    it('getUpdate getter', () => {
      expect(getUpdate(state).name).toEqual('joshua')
    })

    it('getCommentsMeta getter', () => {
      expect(getCommentsMeta(state)).toEqual('joshua')
    })

    it('getCommentsLoading getter', () => {
      expect(getCommentsLoading(state)).toBeTruthy()
    })

    it('getComments Getters', () => {
      expect(getComments(state).length).toBeTruthy()
    })

    it('getSelectedComment', () => {
      expect(getSelectedComment(state)).toEqual(345)
    })

    it('getSelectedReply', () => {
      expect(getSelectedReply(state)).toEqual(123)
    })

    it('getUpdateCreator', () => {
      expect(getUpdateCreator(state).name).toEqual('fredrick')
    })
  })

  describe('SingleUpdateModule Actions', () => {
    beforeEach(() => {
      mock.reset()
      Storage.prototype.getItem = jest.fn(() => 'joshua')
    })
    const commit = jest.fn()
    const dispatch = jest.fn()

    const {fetchUpdate, fetchComments, commentOnUpdate, editUpdateComment, likeAnUpdate, unlikeAnUpdate, likeUpdateComment, unlikeUpdateComment, likeUpdateReply, unlikeUpdateReply, setCommentId, setUserId, setSelectedUpdate, setReplyId, setUpdateCreatorId, replytoComment, editReplyToComment, deleteAComment, deleteReply, fetchUpdateCreatorProfile} = SingleUpdateModule.actions

    it('fetchUpdate success test', () => {
      mock.onGet(`${apiUrl}/updates/34`).reply(200, successResponse)
      fetchUpdate({commit}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })
    it('fetchUpdate failure test', () => {
      mock.onGet(`${apiUrl}/updates/34`).reply(200, failureResponse)
      fetchUpdate({commit}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('fetchComments', () => {
      const state = {
        updateId: 34
      }
      mock.onGet(`${apiUrl}/updates/34/comments?page=1`).reply(200, successResponse)
      fetchComments({commit, state}).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('fetchComments on error', () => {
      const state = {
        updateId: 34
      }
      mock.onGet(`${apiUrl}/updates/34/comments?page=1`).reply(200, failureResponse)
      fetchComments({commit, state}).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('commentOnUpdate', () => {
      const state = {
        updateId: 34
      }
      mock.onPost(`${apiUrl}/updates/34/comments`).reply(200, successResponse)
      commentOnUpdate({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('commentOnUpdate on error', () => {
      const state = {
        updateId: 34
      }
      mock.onPost(`${apiUrl}/updates/34/comments`).reply(200, failureResponse)
      commentOnUpdate({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('editUpdateComment', () => {
      const state = {
        commentId: 34
      }
      mock.onPut(`${apiUrl}/comments/34`).reply(200, successResponse)
      editUpdateComment({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('editUpdateComment on error', () => {
      const state = {
        commentId: 34
      }
      mock.onPut(`${apiUrl}/comments/34`).reply(200, failureResponse)
      editUpdateComment({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('likeAnUpdate', () => {
      const state = {
        updateId: 34
      }
      mock.onPut(`${apiUrl}/updates/34/like`).reply(200, successResponse)
      likeAnUpdate({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('likeAnUpdate on error', () => {
      const state = {
        updateId: 34
      }
      mock.onPut(`${apiUrl}/updates/34/like`).reply(200, failureResponse)
      likeAnUpdate({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('unlikeAnUpdate', () => {
      const state = {
        updateId: 34
      }
      mock.onPut(`${apiUrl}/updates/34/unlike`).reply(200, successResponse)
      unlikeAnUpdate({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('unlikeAnUpdate on error', () => {
      const state = {
        updateId: 34
      }
      mock.onPut(`${apiUrl}/updates/34/unlike`).reply(200, failureResponse)
      unlikeAnUpdate({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('likeUpdateComment', () => {
      const state = {
        commentId: 34
      }
      mock.onPut(`${apiUrl}/comments/34/like`).reply(200, successResponse)
      likeUpdateComment({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('likeUpdateComment on error', () => {
      const state = {
        commentId: 34
      }
      mock.onPut(`${apiUrl}/comments/34/like`).reply(200, failureResponse)
      likeUpdateComment({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('unlikeUpdateComment', () => {
      const state = {
        commentId: 34
      }
      mock.onPut(`${apiUrl}/comments/34/unlike`).reply(200, successResponse)
      unlikeUpdateComment({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('unlikeUpdateComment on error', () => {
      const state = {
        commentId: 34
      }
      mock.onPut(`${apiUrl}/comments/34/unlike`).reply(200, failureResponse)
      unlikeUpdateComment({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('likeUpdateReply', () => {
      const state = {
        replyId: 34
      }
      mock.onPut(`${apiUrl}/comments/34/like`).reply(200, successResponse)
      likeUpdateReply({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('likeUpdateReply on error', () => {
      const state = {
        replyId: 34
      }
      dispatch.mockReset()
      mock.onPut(`${apiUrl}/comments/34/like`).reply(200, failureResponse)
      likeUpdateReply({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).not.toHaveBeenCalled()
      })
    })

    it('unlikeUpdateReply', () => {
      const state = {
        replyId: 34
      }
      mock.onPut(`${apiUrl}/comments/34/unlike`).reply(200, successResponse)
      unlikeUpdateReply({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('unlikeUpdateReply on error', () => {
      const state = {
        replyId: 34
      }
      dispatch.mockReset()
      mock.onPut(`${apiUrl}/comments/34/unlike`).reply(200, failureResponse)
      unlikeUpdateReply({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('setCommentId', () => {
      const payload = {
        replyId: 34
      }
      setCommentId({commit}, payload)
      expect(commit).toHaveBeenCalled()
    })

    it('setUserId', () => {
      setUserId({commit}, 34)
      expect(commit).toHaveBeenCalled()
    })

    it('setSelectedUpdate', () => {
      setSelectedUpdate({commit}, 34)
      expect(commit).toHaveBeenCalled()
    })

    it('setReplyId', () => {
      setReplyId({commit}, 34)
      expect(commit).toHaveBeenCalled()
    })

    it('setUpdateCreatorId', () => {
      setUpdateCreatorId({commit}, 34)
      expect(commit).toHaveBeenCalled()
    })

    it('replytoComment', () => {
      const state = {
        commentId: 34
      }
      mock.onPost(`${apiUrl}/comments/34/reply`).reply(200, successResponse)
      replytoComment({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('replytoComment on error', () => {
      const state = {
        commentId: 34
      }
      mock.onPost(`${apiUrl}/comments/34/reply`).reply(200, failureResponse)
      replytoComment({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('editReplytoComment', () => {
      const state = {
        replyId: 34
      }
      mock.onPut(`${apiUrl}/comments/34`).reply(200, successResponse)
      editReplyToComment({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('editReplytoComment on error', () => {
      const state = {
        replyId: 34
      }
      mock.onPut(`${apiUrl}/comments/34`).reply(200, failureResponse)
      editReplyToComment({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('deleteAComment when updateComments.length is greater than 0', () => {
      const state = {
        commentId: 34,
        updateComments: [{
          id: 23
        }]
      }
      mock.onDelete(`${apiUrl}/comments/34`).reply(200, successResponse)
      deleteAComment({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('deleteAComment when updateComments.length = 0', () => {
      const state = {
        commentId: 34,
        updateComments: []
      }
      mock.onDelete(`${apiUrl}/comments/34`).reply(200, successResponse)
      deleteAComment({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('deleteAComment on error', () => {
      const state = {
        commentId: 34
      }
      mock.onDelete(`${apiUrl}/comments/34`).reply(200, failureResponse)
      deleteAComment({commit, dispatch, state}, 34).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('deleteAReply', () => {
      const state = {
        commentId: 34,
        replyId: 34
      }
      mock.onDelete(`${apiUrl}/comments/34`).reply(200, successResponse)
      deleteReply({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('deleteAReply on error', () => {
      const state = {
        commentId: 34,
        replyId: 34
      }
      mock.onDelete(`${apiUrl}/comments/34`).reply(200, failureResponse)
      deleteReply({commit, dispatch, state}, 34).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })

    it('fetchUpdateCreatorProfile', () => {
      const state = {
        creatorId: 34,
        replyId: 34
      }
      mock.onGet(`${apiUrl}/experts/34`).reply(200, successResponse)
      fetchUpdateCreatorProfile({commit, state}).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('fetchUpdateCreatorProfile on error', () => {
      const state = {
        creatorId: 34,
        replyId: 34
      }
      mock.onGet(`${apiUrl}/experts/34`).reply(200, failureResponse)
      fetchUpdateCreatorProfile({commit, state}).then(() => {
        expect(dispatch).toHaveBeenCalled()
      })
    })
  })
})
