
import UserPublicationModule from '@/store/modules/UserPublicationModule'
import router from '@/router/router'
import { mock } from '@/store/modules/mocks/mockInstance'
import mockPublications from '@/store/modules/mocks/publications'

jest.mock('@/router/router')

router.push = jest.fn()

jest.mock('@/store')

describe('mutation for blogs module', () => {
  const state = {
    publications: [],
    randomPublication: {},
    download: '',
    loading: false,
    error: ''
  }
  it('calls the SET_USER_PUBLICATION mutation', () => {
    UserPublicationModule.mutations.SET_USER_PUBLICATION(state, [{ key: 'value' }])
    expect(state.publications).toEqual([{ key: 'value' }])
  })
  it('calls the SET_RANDOM_USER_PUBLICATION mutation', () => {
    UserPublicationModule.mutations.SET_RANDOM_USER_PUBLICATION(state, {key: 'value'})
    expect(state.randomPublication).toEqual({key: 'value'})
  })
  it('calls the PUBLICATION_ERROR mutation', () => {
    UserPublicationModule.mutations.PUBLICATION_ERROR(state, 'some error')
    expect(state.error).toEqual('some error')
  })
  it('calls the DOWNLOAD_PUBLICATION mutation', () => {
    UserPublicationModule.mutations.DOWNLOAD_PUBLICATION(state, 'some download')
    expect(state.download).toEqual('some download')
  })
  it('calls the IS_LOADING mutation', () => {
    UserPublicationModule.mutations.IS_LOADING(state, true)
    expect(state.loading).toEqual(true)
  })
})
describe('Getters for User publication module', () => {
  const state = {
    publications: [],
    randomPublication: {},
    download: '',
    loading: false,
    error: ''
  }
  it('tests the USER_PUBLICATION getter', () => {
    const publications = UserPublicationModule.getters.USER_PUBLICATION(state)
    expect(publications).toEqual(state.publications)
  })
  it('tests the RANDOM_USER_PUBLICATION getter', () => {
    const publication = UserPublicationModule.getters.RANDOM_USER_PUBLICATION(state)
    expect(publication).toEqual(state.randomPublication)
  })
  it('tests the DOWNLOAD_PUBLICATION getter', () => {
    const download = UserPublicationModule.getters.DOWNLOAD_PUBLICATION(state)
    expect(download).toEqual(state.download)
  })
  it('tests the DOWNLOAD_LOADER getter', () => {
    const loader = UserPublicationModule.getters.DOWNLOAD_LOADER(state)
    expect(loader).toEqual(state.loading)
  })
  it('tests the DOWNLOAD_ERROR getter', () => {
    const error = UserPublicationModule.getters.DOWNLOAD_ERROR(state)
    expect(error).toEqual(state.error)
  })
})
describe('Actions for UserPublication module', () => {
  afterEach(() => {
    mock.reset()
  })
  it('successfully fetches recommended publications from the API', () => {
    const commit = jest.fn()

    const response = {
      data: [
        {
          title: 'Some cool title'
        }
      ]
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/publications').reply(200, response)
    UserPublicationModule.actions.GET_USER_PUBLICATION({commit}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('should return a 404 page when a page is not found', () => {
    const commit = jest.fn()
    const response = {
      success: false
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/publications').reply(404, response)
    UserPublicationModule.actions.GET_USER_PUBLICATION(
      { commit }).then(() => {
      expect(router.push).toHaveBeenCalled()
    })
  })

  it('should return to home page when the status code is not 404 or 401', () => {
    const commit = jest.fn()
    const response = {
      success: false
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/publications').reply(400, response)
    UserPublicationModule.actions.GET_USER_PUBLICATION(
      { commit }).then(() => {
      expect(router.push).toHaveBeenCalled()
    })
  })

  it('should call the GET_DOWNLOAD_PUBLICATION to download a publication', () => {
    window.URL.createObjectURL = jest.fn()
    const commit = jest.fn()
    const config = {
      responseType: 'blob'
    }
    mock.onPut('https://beta.cowsoko.com/api/v1/publications/3/download', config).reply(200)
    UserPublicationModule.actions.GET_DOWNLOAD_PUBLICATION({ commit }, mockPublications[0]).then(() => {
      expect(commit).toHaveBeenCalled()
      expect(window.URL.createObjectURL).toHaveBeenCalled()
    })
  })

  it('should call commit when the status code is above 200 to download a publication', () => {
    const commit = jest.fn()
    const config = {
      responseType: 'blob'
    }
    mock.onPut('https://beta.cowsoko.com/api/v1/publications/3/download', config).reply(400, {error: 'There is error'})
    UserPublicationModule.actions.GET_DOWNLOAD_PUBLICATION({ commit }, mockPublications[0]).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })
})
