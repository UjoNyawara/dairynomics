import uploadProductStore from '@/store/modules/uploadProductStore'
import { mock } from '@/store/modules/mocks/mockInstance'

jest.mock('@/store')

describe('mutations for uploadProductStore module', () => {
  const state = {
    loading: false,
    error: {},
    message: '',
    userData: {},
    currentComponent: 'ProductSection',
    products: []
  }

  it('calls the SUBMIT_FORM_LOADING mutation', () => {
    uploadProductStore.mutations.SUBMIT_FORM_LOADING(state)
    expect(state.loading).toEqual(true)
  })

  it('calls the SUBMIT_FORM_SUCCESS mutation', () => {
    uploadProductStore.mutations.SUBMIT_FORM_SUCCESS(state, 'CELLFAM')
    expect(state.userData).toEqual('CELLFAM')
    expect(state.loading).toEqual(false)
  })

  it('calls the SUBMIT_FORM_ERROR mutation', () => {
    uploadProductStore.mutations.SUBMIT_FORM_ERROR(state, 'CELLFAM')
    expect(state.userData).toEqual('CELLFAM')
    expect(state.loading).toEqual(false)
  })

  it('calls the CHANGE_COMPONENT mutation', () => {
    uploadProductStore.mutations.CHANGE_COMPONENT(state, 'ProductSection')
    expect(state.currentComponent).toEqual('ProductSection')
  })

  it('calls the FETCH_PRODUCTS_LOADING mutation', () => {
    uploadProductStore.mutations.FETCH_PRODUCTS_LOADING(
      state
    )
    expect(state.loading).toEqual(true)
  })

  it('calls the FETCH_PRODUCTS_SUCCESS mutation', () => {
    uploadProductStore.mutations.FETCH_PRODUCTS_SUCCESS(state, {
      products: [],
      message: ''
    })
    expect(state.products).toEqual([])
    expect(state.message).toEqual('')
    expect(state.loading).toEqual(false)
  })

  it('calls the FETCH_PRODUCTS_ERROR mutation', () => {
    uploadProductStore.mutations.FETCH_PRODUCTS_ERROR(state, 'error')
    expect(state.error).toEqual('error')
    expect(state.loading).toEqual(false)
  })
})

describe('Getters for uploadProductStore module', () => {
  const state = {
    loading: false,
    error: {},
    message: '',
    userData: {},
    currentComponent: 'ProductSection',
    products: []
  }

  it('tests the submitFormDetails getter', () => {
    const submitFormDetails = uploadProductStore.getters.submitFormDetails(state)
    expect(submitFormDetails).toEqual(state)
  })
})

describe('Actions for uploadProductStore module', () => {
  afterEach(() => {
    mock.reset()
  })

  it('successfully fetches user products from the API', () => {
    const commit = jest.fn()
    const response = {
      data: {
        success: true,
        message: 'success',
        products: [1, 2, 3]
      }
    }
    mock
      .onGet('https://beta.cowsoko.com/api/v1/users/null/products')
      .reply(200, response)
    uploadProductStore.actions.fetchMyProducts({ commit }).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('it throws error when fetching user products from the API', () => {
    const commit = jest.fn()
    const response = {
      success: false
    }
    mock
      .onGet('https://beta.cowsoko.com/api/v1/users/null/products')
      .reply(400, response)
    uploadProductStore.actions.fetchMyProducts({ commit }).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('successfully submit user products to the API', () => {
    const commit = jest.fn()
    const dispatch = jest.fn()
    const context = {commit, dispatch}
    const payload = 'cellfam'
    mock
      .onPost('https://beta.cowsoko.com/api/v1/products')
      .reply(201)
    uploadProductStore.actions.submitForm(context, payload).then(() => {
      expect(context.commit).toHaveBeenCalled()
    })
  })

  it('throws an error when submitting user products to the API', () => {
    const commit = jest.fn()
    const dispatch = jest.fn()
    const context = { commit, dispatch }
    mock.onPost('https://beta.cowsoko.com/api/v1/products').reply(400)
    uploadProductStore.actions.submitForm(context).then(() => {
      expect(context.commit).toHaveBeenCalled()
    })
  })
})
