import UserLivestockModule from '@/store/modules/UserLivestockModule'
import { mock } from '@/store/modules/mocks/mockInstance'
import router from '@/router/router'
jest.mock('@/router/router')

router.push = jest.fn()

jest.mock('@/store')

describe('mutation for Livestock module', () => {
  const state = {
    livestock: [],
    randomLivestock: {}
  }
  it('calls the SET_USER_LIVESTOCK mutation', () => {
    UserLivestockModule.mutations.SET_USER_LIVESTOCK(state, [{ key: 'value' }])
    expect(state.livestock).toEqual([{ key: 'value' }])
  })
  it('calls the SET_RANDOM_USER_LIVESTOCK mutation', () => {
    UserLivestockModule.mutations.SET_RANDOM_USER_LIVESTOCK(state, {key: 'value'})
    expect(state.randomLivestock).toEqual({key: 'value'})
  })
})
describe('Getters for livestock module', () => {
  const state = {
    livestock: [],
    randomLivestock: {}
  }
  it('tests the USER_LIVESTOCK getter', () => {
    const livestock = UserLivestockModule.getters.USER_LIVESTOCK(state)
    expect(livestock).toEqual(state.livestock)
  })
  it('tests the RANDOM_USER_LIVESTOCK getter', () => {
    const randomLivestock = UserLivestockModule.getters.RANDOM_USER_LIVESTOCK(state)
    expect(randomLivestock).toEqual(state.randomLivestock)
  })
})
describe('Actions for Livestock module', () => {
  afterEach(() => {
    mock.reset()
  })

  it('successfully fetches recommended livestock from the API', () => {
    const commit = jest.fn()
    const response = {
      data: {
        livestocks: {}
      }
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user_livestocks').reply(200, response)
    UserLivestockModule.actions.GET_USER_LIVESTOCK({commit}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('should return a 404 page when an error occurs', () => {
    const commit = jest.fn()
    const response = {
      success: false,
      error: 'some error'
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user_livestocks').reply(404, response)
    UserLivestockModule.actions.GET_USER_LIVESTOCK({ commit }).then(() => {
      expect(router.push).toHaveBeenCalledWith('/404')
    })
  })

  it('should return to home page when an unknown error occurs', () => {
    const commit = jest.fn()
    const response = {
      success: false,
      error: 'some error'
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user_livestocks').reply(400, response)
    UserLivestockModule.actions.GET_USER_LIVESTOCK({ commit }).then(() => {
      expect(router.push).toHaveBeenCalledWith('/')
    })
  })
})
