import 'jest-localstorage-mock'
import AllReceipts from '@/store/modules/AllReceipts'
import { mock } from '@/store/modules/mocks/mockInstance'
import router from '@/router/router'
import { ecommerceBaseUrl } from '@/utils/api/urls'

jest.mock('@/store')

afterEach(() => {
  mock.reset()
  localStorage.clear()
})

const url = `${ecommerceBaseUrl}/api/receipts`

describe('all receipts store module', () => {
  describe('All receipts mutation', () => {
    describe('SET_RECEIPTS mutation', () => {
      const state = { receipts: [] }
      it('it should set the receipts', () => {
        AllReceipts.mutations.SET_RECEIPTS(state, ['receipts1', 'receipts2'])
        expect(state).toEqual({ receipts: ['receipts1', 'receipts2'] })
      })
    })

    describe('SET_LOADER mutation', () => {
      const state = { isLoading: false }
      it('it should set the loader to true', () => {
        AllReceipts.mutations.SET_LOADER(state, true)
        expect(state).toEqual({isLoading: true})
      })
    })
  })

  describe('All Receipts actions', () => {
    let commit
    beforeEach(() => {
      commit = jest.fn()
      router.push = jest.fn()
      jest.mock('@/store')
    })

    it('should make an api call if receipts are empty', () => {
      mock.onGet(url).reply(200, 'testing')
      let state = {
        receipts: [],
        isLoading: false
      }
      AllReceipts.actions.getAllReceipts({commit, state}, 1)
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit).toHaveBeenCalledWith('SET_LOADER', true)
    })
    it('should redirect to 404 page when status code is 404', () => {
      mock.onGet(url).reply(404)
      AllReceipts.actions.getAllReceipts({commit}).then(() => {
        expect(router.push).toHaveBeenCalledWith('/404')
      })
    })

    it('should redirect to / page when other errors are thrown', () => {
      router.push.mockReset()
      mock.onGet(url).reply(400)
      AllReceipts.actions.getAllReceipts({commit}).then(() => {
        expect(router.push).toHaveBeenCalledWith('/')
      })
    })
  })
})
