import uploadShareImageStore from '@/store/modules/uploadShareImageStore'
import { mock as mocked } from '@/store/modules/mocks/mockInstance'

jest.mock('@/store')

const state = {
  sharedImage: '',
  shareStatus: '',
  errorMessage: '',
  loading: false
}
describe('Social share mutations', () => {
  it('test UPLOAD_SHARE_IMAGE mutation', () => {
    const imageUrl = 'mocked-image.jpg'
    uploadShareImageStore.mutations.UPLOAD_SHARE_IMAGE(state, imageUrl, 'status')
    expect(state.sharedImage).toEqual(imageUrl)
  })
  it('test HANDLE_ERRORS mutation', () => {
    const errorMessage = 'error occured'
    uploadShareImageStore.mutations.HANDLE_ERRORS(state, errorMessage)
    expect(state.errorMessage).toEqual(errorMessage)
  })
  it('test FACEBOOK_LOADING mutation', () => {
    const loader = true
    uploadShareImageStore.mutations.FACEBOOK_LOADING(state, true)
    expect(state.loading).toEqual(loader)
  })
  it('test social GETTERS', () => {
    const state = {randomState: 'state'}
    uploadShareImageStore.getters.shareGetterData(state)
    uploadShareImageStore.getters.imageGetterData(state)
    expect(state).toEqual(state)
  })
})

describe('uploadShareImage actions', () => {
  afterEach(() => {
    mocked.reset()
  })
  it('test successfully upload image to cloudinary', async () => {
    const commit = jest.fn()
    const CLOUDINARY_PRESET = 'dcdwqul3'
    let formData = new FormData()
    formData.append('file', 'mocked-image.jpg')
    formData.append('upload_preset', CLOUDINARY_PRESET)
    const image = 'mocked-image.jpg'
    mocked.onPost('https://api.cloudinary.com/v1_1/dvixzojo2/image/upload').reply(201)
    await uploadShareImageStore.actions.imageUpload({commit}, image)
    expect(commit).toHaveBeenCalledWith(
      'FACEBOOK_LOADING', true)
  })
})
