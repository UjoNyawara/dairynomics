import { mock } from '@/store/modules/mocks/mockInstance'
import IndividualPublicationModule from '@/store/modules/IndivualPublicationModule'

afterEach(() => {
  mock.reset()
})

jest.mock('@/store')

describe('Individual Publication Module test', () => {
  describe('individual publication mutations test', () => {
    const state = {}
    const { mutations } = IndividualPublicationModule
    it('View_single_publication mutation test', () => {
      mutations.VIEW_SINGLE_PUBLICATION(state, 'joshua')
      expect(state.publication).toEqual('joshua')
    })

    it('publication_error mutation', () => {
      mutations.PUBLICATION_ERROR(state, 'error')
      expect(state.error).toEqual('error')
    })

    it('download_publication', () => {
      mutations.DOWNLOAD_PUBLICATION(state, 'download')
      expect(state.download).toEqual('download')
    })

    it('loading Icon', () => {
      mutations.LOADING_ICON(state, false)
      expect(state.loading).toBeFalsy()
    })

    it('is_page_loading test', () => {
      mutations.IS_PAGE_LOADING(state, false)
      expect(state.isLoading).toBeFalsy()
    })
  })

  describe('IndividualPublicationModule getters Test', () => {
    const state = {
      publication: 'joshua_publication',
      download: 'joshua_download',
      loading: false,
      error: 'joshua_error',
      isLoading: true
    }
    const { getters } = IndividualPublicationModule
    it('publicationGetter', () => {
      expect(getters.publicationGetter(state)).toEqual('joshua_publication')
    })

    it('downloadGetter', () => {
      expect(getters.downloadGetter(state)).toEqual('joshua_download')
    })

    it('downloadLoader', () => {
      expect(getters.downloadLoader(state)).toBeFalsy()
    })

    it('downloadError', () => {
      expect(getters.downloadError(state)).toEqual('joshua_error')
    })

    it('isLoading', () => {
      expect(getters.isLoading(state)).toBeTruthy()
    })
  })

  describe('IndividualPublication Action test', () => {
    const commit = jest.fn()
    const { actions: { singlePublication, downloadPublication } } = IndividualPublicationModule
    it('singlePublication test', () => {
      const response = {
        success: true
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/publications/43').reply(200, response)
      singlePublication({commit}, 43).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('singlepublication test when success is false', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/publications/43').reply(200, {success: false})
      singlePublication({commit}, 43).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })
    it('downloadPublication test', () => {
      const publication = {
        id: 56,
        title: 'joshua'
      }
      window.URL.createObjectURL = jest.fn()
      Storage.prototype.getItem = jest.fn(() => 456)
      mock.onPut('https://beta.cowsoko.com/api/v1/publications/56/download').reply(200, {success: true})
      downloadPublication({commit}, publication).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('downloadPublication test when api returns errro', () => {
      const publication = {
        id: 56,
        title: 'joshua'
      }
      Storage.prototype.getItem = jest.fn(() => 456)
      mock.onPut('https://beta.cowsoko.com/api/v1/publications/56/download').reply(400)
      downloadPublication({commit}, publication).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })
  })
})
