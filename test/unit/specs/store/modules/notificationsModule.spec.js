import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import 'jest-localstorage-mock'
import notificationsModule from '@/store/modules/notificationsModule'

jest.mock('@/store')

const mock = new AxiosMockAdapter(axios)

describe('updateNotifications sets notifications with items in payload', () => {
  const dispatch = jest.fn()
  const commit = jest.fn()
  afterEach(() => {
    mock.reset()
    localStorage.clear()
  })

  it('updatesNotifactions when response is a success', () => {
    let payload = [ {id: '122', 'title': 'ddsdsd', viewed: '0'} ]
    notificationsModule.mutations.updateNotifications(notificationsModule.state, payload)
    expect(notificationsModule.state.notifications).toBe(payload)
  })
  it('updatesNotifactions sets errorMessage when response is not a success', () => {
    let message = 'error no notifications'
    notificationsModule.mutations.updateErrorMessage(notificationsModule.state, message)
    expect(notificationsModule.state.errorMessage).toBe(message)
  })
  it('displays notifications', () => {
    notificationsModule.getters.getNotifications(notificationsModule.state)
    expect(notificationsModule.state.notifications.length).toBe(1)
  })
  it('fetches notications', () => {
    const commit = jest.fn()
    const resp = {
      success: true,
      data: [],
      links: [],
      unviewed: 0,
      viewed: 1
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user/null/notifications').reply(200, resp)
    notificationsModule.actions.fetchNotifications({commit}).then(() => {
      expect(localStorage.getItem).toHaveBeenCalled()
      expect(commit).toHaveBeenCalledTimes(1)
    })
  })

  it('fetches notications', () => {
    const resp = {
      success: false,
      data: {
        message: 'no notifications'
      }
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user/null/notifications').reply(200, resp)
    notificationsModule.actions.fetchNotifications({commit}).then(() => {
      expect(commit).toHaveBeenCalled()
      expect(localStorage.getItem).toHaveBeenCalled()
    })
  })

  it('fetch notifications should call commit when success is false and there is error message', () => {
    const resp = {
      success: false,
      data: {
        message: 'no notifications'
      },
      error: {
        message: 'error'
      }
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user/null/notifications').reply(200, resp)
    notificationsModule.actions.fetchNotifications({commit}).then(() => {
      expect(commit).toHaveBeenCalled()
      expect(localStorage.getItem).toHaveBeenCalled()
    })
  })

  it('fetch notifications should call commit when status code is greater than 200', () => {
    mock.onGet('https://beta.cowsoko.com/api/v1/user/null/notifications').reply(400)
    notificationsModule.actions.fetchNotifications({commit}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('marks notifications as read', () => {
    const resp = {
      success: true
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user/null/notifications/unread').reply(200, resp)
    notificationsModule.actions.markAsRead({commit, dispatch}).then(() => {
      expect(dispatch).toHaveBeenCalledWith('fetchNotifications')
    })
  })

  it('marks notifications as read should not call dispatch when markasread is not successful', () => {
    dispatch.mockReset()
    commit.mockReset()
    const resp = {
      success: false
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user/null/notifications/unread').reply(200, resp)
    notificationsModule.actions.markAsRead({commit, dispatch}).then(() => {
      expect(dispatch).not.toHaveBeenCalled()
    })
  })

  it('marks notifications as read should call commit when there is error from the API call', () => {
    dispatch.mockReset()
    commit.mockReset()
    mock.onGet('https://beta.cowsoko.com/api/v1/user/null/notifications/unread').reply(400)
    notificationsModule.actions.markAsRead({commit, dispatch}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })
})
