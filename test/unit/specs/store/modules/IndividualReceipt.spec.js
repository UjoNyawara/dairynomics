import IndividualReceipt from '@/store/modules/IndividualReceipt'
import { mock } from '@/store/modules/mocks/mockInstance'
import router from '@/router/router'
import { ecommerceBaseUrl } from '@/utils/api/urls'

jest.mock('@/store')

afterEach(() => {
  mock.reset()
})

const url = `${ecommerceBaseUrl}/api/receipts`

describe('Individual receipt store module', () => {
  describe('Individual receipt mutation', () => {
    describe('SET_RECEIPT mutation', () => {
      const state = { receipt: {} }
      it('it should set the receipt', () => {
        IndividualReceipt.mutations.SET_RECEIPT(state, {
          'id': 'f66dff30-1759-4733-9032-87f39442f22e',
          'invoiceNumber': '#24590',
          'createdAt': '2019-07-01T06:51:46.357Z',
          'amountToBePaid': 3500
        })
        expect(state).toEqual({ receipt: {
          'id': 'f66dff30-1759-4733-9032-87f39442f22e',
          'invoiceNumber': '#24590',
          'createdAt': '2019-07-01T06:51:46.357Z',
          'amountToBePaid': 3500
        }})
      })
    })

    describe('SET_ORDER_ITEMS mutation', () => {
      const state = { orderItems: [] }
      it('it should set the receipt', () => {
        IndividualReceipt.mutations.SET_ORDER_ITEMS(state, {
          'id': '2a0160f0-ca30-4f43-a153-d783a37381a2',
          'orderId': '9ddb64b2-b3a6-49be-a2d2-0f05d7872c6d',
          'userId': 1878,
          'productId': '351043bb-3460-4937-902c-e450ab2afc46',
          'quantity': 5,
          'amount': 5,
          'createdAt': '2019-08-21T19:15:00.193Z',
          'updatedAt': '2019-08-21T19:15:00.193Z',
          'product': {
            'categoryId': 58556,
            'productName': 'omnis at accusamus',
            'description': 'Rerum et nobis quia magnam quis eos culpa quibusdam consequatur.',
            'photos': []
          }
        })
        expect(state).toEqual({ orderItems: {
          'id': '2a0160f0-ca30-4f43-a153-d783a37381a2',
          'orderId': '9ddb64b2-b3a6-49be-a2d2-0f05d7872c6d',
          'userId': 1878,
          'productId': '351043bb-3460-4937-902c-e450ab2afc46',
          'quantity': 5,
          'amount': 5,
          'createdAt': '2019-08-21T19:15:00.193Z',
          'updatedAt': '2019-08-21T19:15:00.193Z',
          'product': {
            'categoryId': 58556,
            'productName': 'omnis at accusamus',
            'description': 'Rerum et nobis quia magnam quis eos culpa quibusdam consequatur.',
            'photos': []
          }
        }})
      })
    })

    describe('SET_BILLING mutation', () => {
      const state = { billing: {} }
      it('it should set the receipt', () => {
        IndividualReceipt.mutations.SET_BILLING(state, {
          'address': 'Nairobi, Kenya',
          'fullName': 'Ryan Wire',
          'phoneNumber': '+254724374281'
        })
        expect(state).toEqual({ billing: {
          'address': 'Nairobi, Kenya',
          'fullName': 'Ryan Wire',
          'phoneNumber': '+254724374281'
        }})
      })
    })

    describe('SET_SPINNER mutation', () => {
      const state = { isLoading: false }
      it('it should set the loader to true', () => {
        IndividualReceipt.mutations.SET_SPINNER(state, true)
        expect(state).toEqual({isLoading: true})
      })
    })

    describe('SET_IS_DOWNLOADING mutation', () => {
      const state = { isDownloading: false }
      it('it should set isDownloading to true', () => {
        IndividualReceipt.mutations.SET_IS_DOWNLOADING(state, true)
        expect(state).toEqual({isDownloading: true})
      })
    })

    describe('RESET_RECEIPT mutation', () => {
      const state = {
        receipt: {
          'invoiceNumber': '------',
          'amountToBePaid': 0
        }
      }
      it('it should reset the receipt', () => {
        IndividualReceipt.mutations.RESET_RECEIPT(state)
        expect(state).toEqual({receipt: {
          'invoiceNumber': '------',
          'amountToBePaid': 0
        }})
      })
    })

    describe('RESET_ORDER_ITEMS mutation', () => {
      const state = {
        orderItems: []
      }
      it('it should reset the order items', () => {
        IndividualReceipt.mutations.RESET_ORDER_ITEMS(state)
        expect(state).toEqual({orderItems: []})
      })
    })

    describe('RESET_BILLING mutation', () => {
      const state = {
        billing: []
      }
      it('it should reset the billing information', () => {
        IndividualReceipt.mutations.RESET_BILLING(state)
        expect(state).toEqual({billing: []})
      })
    })
  })

  describe('Individual receipt actions', () => {
    let commit
    beforeEach(() => {
      commit = jest.fn()
      router.push = jest.fn()
      jest.mock('@/store')
    })

    it('should make an api call if receipt is empty', () => {
      mock.onGet(`${url}/f66dff30-1759-4733-9032-87f39442f22e`).reply(200, 'testing')
      IndividualReceipt.actions.getReceipt({commit}, 'f66dff30-1759-4733-9032-87f39442f22e')
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit).toHaveBeenCalledWith('SET_SPINNER', true)
    })

    it('should reset the receipt', () => {
      IndividualReceipt.actions.resetReceipt({commit})
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit).toHaveBeenCalledWith('RESET_RECEIPT')
    })

    it('should reset the billing information', () => {
      IndividualReceipt.actions.resetBilling({commit})
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit).toHaveBeenCalledWith('RESET_BILLING')
    })

    it('should reset the order items', () => {
      IndividualReceipt.actions.resetOrderItems({commit})
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit).toHaveBeenCalledWith('RESET_ORDER_ITEMS')
    })

    it('should redirect to 404 page when status code is 404', () => {
      mock.onGet(`${url}/c79a1062-d5d4-4df4-a41c-65b6c054c582`).reply(404, {
        'status': 404,
        'message': 'Invoice was not found'
      })
      IndividualReceipt.actions.getReceipt({commit}, 'c79a1062-d5d4-4df4-a41c-65b6c054c582').then(() => {
        expect(router.push).toHaveBeenCalledWith('/404')
      })
    })

    it('should get an individual receipt', () => {
      mock.onGet(`${url}/f66dff30-1759-4733-9032-87f39442f22e`).reply(200, {receipt: {
        id: 'f66dff30-1759-4733-9032-87f39442f22e',
        order: {
          billing: {
            'address': 'Nairobi, Kenya',
            'fullName': 'Ryan Wire',
            'phoneNumber': '+254724111111'
          }
        }
      }})
      const receiptId = 'f66dff30-1759-4733-9032-87f39442f22e'
      IndividualReceipt.actions.getReceipt({commit}, {receiptId})
      expect(commit).toHaveBeenCalled()
    })

    it('should download an individual receipt', () => {
      mock.onGet(`${url}/download/f66dff30-1759-4733-9032-87f39442f22e`).reply(200, 'blob')
      const receiptId = 'f66dff30-1759-4733-9032-87f39442f22e'
      IndividualReceipt.actions.downloadReceipt({commit}, receiptId)
      expect(commit).toHaveBeenCalled()
    })
  })
})
