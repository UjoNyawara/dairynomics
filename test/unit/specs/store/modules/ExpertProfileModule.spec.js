import ExpertProfileModule from '@/store/modules/ExpertProfileModule'
import { mock } from '@/store/modules/mocks/mockInstance'
import router from '@/router/router'

router.push = jest.fn()

jest.mock('@/store')
jest.mock('@/router/router')

describe('mutation for blogs module', () => {
  const state = {
    experts: [],
    randomExpert: {},
    isLoading: false
  }
  it('calls the SET_EXPERT_PROFILES mutation', () => {
    ExpertProfileModule.mutations.SET_EXPERT_PROFILES(state, [{ key: 'value' }])
    expect(state.experts).toEqual([{ key: 'value' }])
  })
  it('calls the SET_RANDOM_EXPERT mutation', () => {
    ExpertProfileModule.mutations.SET_RANDOM_EXPERT(state, { key: 'value' })
    expect(state.randomExpert).toEqual({ key: 'value' })
  })
  it('calls the SET_LOADING mutation', () => {
    ExpertProfileModule.mutations.SET_ISLOADING(state, true)
    expect(state.isLoading).toEqual(true)
  })
})
describe('Actions for ExpertProfile module', () => {
  afterEach(() => {
    mock.reset()
  })
  it('successfully fetches recommended expert from the API', () => {
    const commit = jest.fn()
    const response = {
      data: [
        {
          first_name: 'Frank'
        }
      ]
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user_experts').reply(200, response)
    ExpertProfileModule.actions.GET_EXPERT_PROFILES({ commit }).then(() => {
      expect(commit).toHaveBeenCalled(1)
    })
  })
  it('should return a 404 page when an error occurs', () => {
    const commit = jest.fn()
    const response = {
      success: false
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user_experts').reply(404, response)
    ExpertProfileModule.actions.GET_EXPERT_PROFILES({ commit }, { router }).then(() => {
      expect(router.push).toHaveBeenCalledWith('/404')
    })
  })

  it('should return a Home page when an an unknown error occurs', () => {
    const commit = jest.fn()
    const response = {
      success: false
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user_experts').reply(400, response)
    ExpertProfileModule.actions.GET_EXPERT_PROFILES({ commit }, { router }).then(() => {
      expect(router.push).toHaveBeenCalledWith('/')
    })
  })

  it('should redirect to the home page for other errors from the server', () => {
    const commit = jest.fn()
    const response = {
      success: false,
      error: 'Some error'
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user_experts').reply(401, response)
    ExpertProfileModule.actions.GET_EXPERT_PROFILES({ commit }, { router }).then(() => {
      expect(router.push).toHaveBeenCalledWith('/')
    })
  })
})

describe('Getters for Expert Profile Module', () => {
  const state = {
    experts: [],
    randomExpert: {},
    isLoading: false
  }
  it('gets a list of expert profiles', () => {
    const experts = ExpertProfileModule.getters.EXPERT_PROFILES(state)
    expect(experts).toEqual(state.experts)
  })
  it('gets a random expert profile', () => {
    const expert = ExpertProfileModule.getters.RANDOM_EXPERT(state)
    expect(expert).toEqual(state.randomExpert)
  })
  it('gets a loading spinner', () => {
    const isLoading = ExpertProfileModule.getters.IS_LOADING(state)
    expect(isLoading).toEqual(state.isLoading)
  })
})
