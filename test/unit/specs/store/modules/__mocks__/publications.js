const publications = [
  {
    'id': 3,
    'title': 'Base of the Pyramid (BoP) Study Synthesis Report',
    'author': 'Research Solutions Africa (RSA)',
    'description': 'In the context of the Kenya Market-led Development Programme (KMDP), SNV contracted Research Solutions Africa (RSA) to prepare a study on current and potential business models for sales and d',
    'uploaded_by': {
      'id': '841',
      'first_name': 'Victor',
      'last_name': 'Otieno',
      'email': 'votieno@snvworld.org',
      'country': '',
      'country_code': '254',
      'phone': '0726711853',
      'account_type': 'Service provider (expert)',
      'county': 'Kirinyaga',
      'profile': {
        'pic': 'https://beta.cowsoko.com/storage/img/avatar_305_1554300224_5d05f.jpeg',
        'bio': 'A farmer at heart.',
        'social': {
          'facebook': null,
          'twitter': null
        }
      },
      'verified_account': 0
    },
    'thumbnail': 'https://beta.cowsoko.com/storage/image/1453730089BoP_Report-1.png',
    'section': 'kmdp',
    'is_premium': '0',
    'price': '0',
    'topic': 'Modular Cow House Design for Smallholder Dairy Entrepreneurs',
    'category': 'Publication',
    'likes': 0,
    'number_of_comments': 0,
    'contact_info': null,
    'year_of_publish': '2013',
    'download_count': '14',
    'view_count': '26',
    'reference': '7T89DUI1218',
    'featured': null,
    'featured_at': null,
    'created_at': {
      'date': '-0001-11-30 00:00:00.000000',
      'timezone_type': 3,
      'timezone': 'Africa/Nairobi'
    },
    'updated_at': {
      'date': '-0001-11-30 00:00:00.000000',
      'timezone_type': 3,
      'timezone': 'Africa/Nairobi'
    },
    'deleted_at': null
  }
]

export default publications
