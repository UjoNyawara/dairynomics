import UpdatesModule from '@/store/modules/UpdatesModule'
import { mock } from '@/store/modules/mocks/mockInstance'
import router from '@/router/router'

jest.mock('@/router/router')
router.push = jest.fn()

jest.mock('@/store')

describe('mutation for Updates module', () => {
  const state = {
    updates: [],
    randomUpdate: {}
  }

  it('calls the SET_UPDATES mutation', () => {
    UpdatesModule.mutations.SET_UPDATES(state, [{ key: 'value' }])
    expect(state.updates).toEqual([{ key: 'value' }])
  })
  it('calls the SET_RANDOM_UPDATE mutation', () => {
    UpdatesModule.mutations.SET_RANDOM_UPDATE(state, {key: 'value'})
    expect(state.randomUpdate).toEqual({key: 'value'})
  })
})

describe('Getters for Updates module', () => {
  const state = {
    updates: [],
    randomUpdate: {}
  }

  it('tests the UPDATES getter', () => {
    const updates = UpdatesModule.getters.UPDATES(state)
    expect(updates).toEqual(state.updates)
  })

  it('tests the RANDOMUPDATE getter', () => {
    const update = UpdatesModule.getters.RANDOMUPDATE(state)
    expect(update).toEqual(undefined)
  })
})
describe('Actions for ExpertProfile module', () => {
  afterEach(() => {
    mock.reset()
  })

  it('successfully fetches recommended updates from the API', () => {
    const commit = jest.fn()
    const response = {
      success: true,
      updates: []
    }

    mock.onGet('https://beta.cowsoko.com/api/v1/user_updates').reply(200, response)
    UpdatesModule.actions.GET_UPDATES({commit}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('should return a 404 page when an error occurs', () => {
    const commit = jest.fn()
    const response = {
      success: false,
      error: 'some error'
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user_updates').reply(404, response)
    UpdatesModule.actions.GET_UPDATES({ commit }).then(() => {
      expect(router.push).toHaveBeenCalledWith('/404')
    })
  })

  it('should return to home page when an unknown error occurs', () => {
    const commit = jest.fn()
    const response = {
      success: false,
      error: 'some error'
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user_updates').reply(400, response)
    UpdatesModule.actions.GET_UPDATES({ commit }).then(() => {
      expect(router.push).toHaveBeenCalledWith('/')
    })
  })
})
