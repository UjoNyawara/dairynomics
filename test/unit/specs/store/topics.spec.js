import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import topicsModule from '@/store/modules/topics'
jest.mock('@/store')

const mock = new AxiosMockAdapter(axios)

const mockState = {
  topics: ['fredrick']
}
const context = {
  commit: jest.fn()
}
const payload = {
  token: 'joshua'
}
describe('Topics Module Test', () => {
  beforeEach(() => {
    mock.reset()
  })
  describe('Mutations test', () => {
    it('SET_TOPICS should return state.topics', () => {
      const payload = {
        topics: ['joshua']
      }
      topicsModule.mutations.SET_TOPICS(topicsModule.state, payload)
      expect(topicsModule.state.topics).toEqual(['joshua'])
    })
  })

  describe('Getters Test', () => {
    it('should return state.topics', () => {
      const expectedResult = topicsModule.getters.TOPICS(mockState)
      expect(expectedResult).toEqual(['fredrick'])
    })
  })

  describe('Actions Test', () => {
    it('should call GET_TOPICS', () => {
      const response = {
        name: 'joshua'
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/topics').reply(200, response)
      topicsModule.actions.GET_TOPICS(context, payload).then(() => {
        expect(context.commit).toHaveBeenCalled()
      })
    })

    it('should catch error when the api throw an error', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/topics').reply(500)
      topicsModule.actions.GET_TOPICS(context, payload).then({}).catch((error) => {
        expect(error).toBeDefined()
      })
    })
  })
})
