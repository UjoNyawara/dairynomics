import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Vuelidate from 'vuelidate'
import UserAuthModal from '@/components/ui/UserAuthModal.vue'

jest.mock('@/store')
const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Vuelidate)

describe('UserAuthSignUp', () => {
  let getters
  let store
  let wrapper
  let actions

  beforeEach(() => {
    getters = {
      signedUpUser: () => 2,
      LoginDetails: () => {
        return {
          loggingIn: false,
          loginError: null,
          loginSuccessful: false

        }
      },
      authError: () => '',
      isLoggedIn: () => false,
      getForgotPin: () => ''
    }

    actions = {
      signUpUser: jest.fn(),
      openValidationPage: jest.fn()
    }

    store = new Vuex.Store({
      actions, getters

    })
    wrapper = shallowMount(UserAuthModal, { store,
      localVue })

    wrapper.setData({
      form: 'login',
      buttonTitle: 'Send Pin'
    })
  })

  describe('Authmodal snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should toggle submitSignUpForm method', () => {
    wrapper.setData({
      form: 'login',
      buttonTitle: 'Send Pin',
      showModal: false
    })
    wrapper.find('button').trigger('click')
    wrapper.find('#signupButton').trigger('click')
    expect(wrapper.vm.form).toEqual('signup')
    wrapper.find('#loginButton').trigger('click')
    expect(wrapper.vm.form).toEqual('login')
  })

  it('should set form to validation', () => {
    wrapper = shallowMount(UserAuthModal, { store,
      localVue })
    wrapper.setData({
      form: 'login',
      buttonTitle: 'Send Pin'
    })
    let data = {success: true}
    wrapper.vm.$options.watch.signedUpUser.call(wrapper.vm, data)
    expect(wrapper.vm.form).toEqual('validation')
  })

  it('should render when the new data.success is false', () => {
    let data = {success: false}
    wrapper.vm.$options.watch.signedUpUser.call(wrapper.vm, data)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should call getter signedUpUser method', () => {
    wrapper = shallowMount(UserAuthModal, { store,
      localVue })
    wrapper.setData({
      form: 'login',
      buttonTitle: 'Send Pin'
    })
    let data = {forgotpin: 'forgotpin'}
    wrapper.vm.$options.watch.getForgotPin.call(wrapper.vm, data)
    expect(wrapper.vm.form).toEqual('forgotpin')
  })

  it('close Modal should call emit closeModal', () => {
    const closeModalSpy = jest.spyOn(wrapper.vm, 'closeModal')
    closeModalSpy()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
