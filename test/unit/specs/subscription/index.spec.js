import { shallowMount, createLocalVue } from '@vue/test-utils'
import Subscription from '@/pages/Subscriptionoption/index'
import Vuex from 'vuex'
import Router from 'vue-router'

jest.mock('@/router/router')
const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [{
    path: '/'
  }]
})

const actions = {}
const getters = {}

const store = new Vuex.Store({
  actions,
  getters
})

describe('testing the index file in subscription folder', () => {
  it('testing to see if index page renders html', () => {
    const wrapper = shallowMount(Subscription, { localVue, store, router })
    wrapper.vm.$router.push = jest.fn()
    expect(wrapper.contains('div')).toBe(true)
  })
})
