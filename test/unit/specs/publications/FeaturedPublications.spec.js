import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'
import Router from 'vue-router'
import mockPublications from './__mock__/publications'
import mockUser from './__mock__/user'
import testHelpers from '@/utils/testHelpers'
import FeaturedPublications from '@/pages/Publications/FeaturedPublications'
import PublicationListItem from '@/pages/Publications/PublicationListItem'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Router)
const router = new Router()

describe('FeaturedPublications Component', () => {
  let wrapper
  let store
  let actions
  let currentPage = 1
  let lastPage = 5
  let state = {
    user: mockUser,
    allPublications: {
      data: mockPublications,
      pagination: {
        currentPage,
        lastPage,
        total: 5
      },
      paginatedData: {}
    },
    featuredPublications: {
      data: [],
      pagination: {
        currentPage,
        lastPage,
        total: 5
      },
      paginatedData: {}
    },
    requestStatuses: {
      isDownloading: false,
      isFetching: false
    }
  }

  const getters = {
    getPaginatedFeaturedPublications: publicationsState => publicationsState.featuredPublications.paginatedData,
    getFeaturedPublicationsPagination: publicationsState => publicationsState.featuredPublications.pagination,
    getRequestStatuses: publicationsState => publicationsState.requestStatuses,
    USER_PROFILE: profileState => profileState.user
  }

  const mountComponent = () => {
    actions = {
      fetchPublications: jest.fn(),
      downloadPublication: jest.fn(),
      incrementPage: jest.fn(),
      decrementPage: jest.fn(),
      GET_USER_PROFILE: jest.fn()
    }

    state.featuredPublications.paginatedData[`page-${currentPage}`] = mockPublications
    store = new Vuex.Store({ state, getters, actions })
    wrapper = mount(FeaturedPublications, { store, localVue, router })
    testHelpers.wrapper = wrapper
    return wrapper
  }

  it('does not show publication items when publication list is empty', () => {
    currentPage = undefined
    mountComponent()
    testHelpers.notSeeElement('.pub-list-item')
    testHelpers.notSeeElement(PublicationListItem)
  })

  it('lists publications items when item exist in list ', () => {
    currentPage = 1
    wrapper = mountComponent()
    testHelpers.seeElement(PublicationListItem)
    testHelpers.seeString(mockPublications[0].title)
    testHelpers.seeElement('.pub-list-item')
  })

  it('handles actions called on clicks', async () => {
    currentPage = 2
    wrapper = mountComponent()
    expect(actions.fetchPublications).toHaveBeenCalled()

    // download button actions
    let button = wrapper.find('.pub-list-item .action button')
    await button.trigger('click')
    expect(actions.downloadPublication).toHaveBeenCalled()

    // pagination buttons
    // previous button click
    button = wrapper.find('.pagination-actions .align-left button')
    await button.trigger('click')
    expect(actions.decrementPage).toHaveBeenCalled()

    // next button click
    button = await wrapper.find('.pagination-actions .align-right button')
    await button.trigger('click')
    expect(actions.incrementPage).toHaveBeenCalled()
  })

  it('handles pagination scenario where currentPage === lastPage', () => {
    currentPage = lastPage
    state.featuredPublications.pagination.currentPage = currentPage
    wrapper = mountComponent()

    testHelpers.seeElement(PublicationListItem)
    testHelpers.seeElement('.pub-list-item')
  })

  it('handles pagination scenario where currentPage === 1', () => {
    currentPage = lastPage
    state.featuredPublications.pagination.currentPage = currentPage
    wrapper = mountComponent()

    testHelpers.seeElement(PublicationListItem)
    testHelpers.seeElement('.pub-list-item')
  })
})
