import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuelidate from 'vuelidate'
import LostPinRecovery from '@/components/ui/userAuth/LostPinRecovery.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Vuelidate)
let actions, getters, store, wrapper

describe('test LostPinRecovery Component', () => {
  beforeEach(() => {
    getters = {
      getForgotPin: () => {
        return {
          loading: false,
          user_id: '',
          forgotpin: '',
          verification_code: ''
        }
      }
    }
    actions = { sendVerification: jest.fn() }

    store = new Vuex.Store({
      actions,
      getters
    })
    wrapper = shallowMount(LostPinRecovery, {localVue, store})
  })

  it('test should return verification code with valid input', () => {
    wrapper.setData({
      recoveryForm: {
        country_code: '+563',
        phone: '0708357482'
      }
    })
    wrapper.find('button').trigger('click')
    expect(actions.sendVerification).toHaveBeenCalled()
  })

  it('test should return verification code with incorrect input', () => {
    wrapper.setMethods({sendPhoneDetails: jest.fn()})
    wrapper.setData({
      recoveryForm: {
        country_code: '',
        phone: ''
      }
    })
    wrapper.find('button').trigger('click')
    expect(wrapper.vm.sendPhoneDetails).toHaveBeenCalled()
  })

  it('sendPhone Details should add error when the credentials input are invalid', () => {
    wrapper.setData({
      recoveryForm: {
        phone: 34333
      }
    })
    wrapper.vm.sendPhoneDetails()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should remove buttonTitle when data is loading', () => {
    wrapper.vm.$options.watch.getForgotPin.call(wrapper.vm, {loading: true})
    expect(wrapper.vm.recoveryForm.buttonTitle).toEqual('')
  })

  it('should set buttonTitle to Send Code when data has fully loaded', () => {
    wrapper.vm.$options.watch.getForgotPin.call(wrapper.vm, {loading: false, success: false})
    expect(wrapper.vm.recoveryForm.buttonTitle).toEqual('Send Code')
  })

  it('should set buttonTitle to Send Code when data has fully loaded', () => {
    wrapper.vm.$options.watch.getForgotPin.call(wrapper.vm, {loading: false, success: true})
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
