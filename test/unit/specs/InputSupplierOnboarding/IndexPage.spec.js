import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import indexPage from '@/pages/InputSuppliersOnboarding/index'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Input Supplier Onboarding', () => {
  let wrapper

  it('should render the component Input Suppliers Onbaording Page', () => {
    wrapper = shallowMount(indexPage, {
      localVue,
      stubs: ['router-link', 'router-view']
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
    wrapper.vm.$options.watch.inputSupplyCategorySelected()
  })

  it('should call the watch method on inputSupplyCategory', () => {
    wrapper = shallowMount(indexPage, {
      localVue,
      stubs: ['router-link', 'router-view']
    })
    wrapper.setData({ inputSupplyCategory: [] })
    wrapper.setData({ inputSupplyCategorySelected: 'Fodder solutions' })
    expect(Array.isArray(wrapper.vm.optionValues)).toBe(true)
    wrapper.setData({
      inputSupplyCategorySelected: 'Dairy meal, minerals & supplements'
    })
    expect(Array.isArray(wrapper.vm.optionValues)).toBe(true)
    wrapper.setData({
      inputSupplyCategorySelected: 'Farm Machinery & Equipment'
    })
    expect(Array.isArray(wrapper.vm.optionValues)).toBe(true)
    wrapper.setData({
      inputSupplyCategorySelected: 'Drugs and Vet Products'
    })
    expect(Array.isArray(wrapper.vm.optionValues)).toBe(true)
    wrapper.setData({ inputSupplyCategorySelected: 'Irrigation solutions' })
    expect(Array.isArray(wrapper.vm.optionValues)).toBe(true)
  })

  it('should call submitForm method', () => {
    wrapper = shallowMount(indexPage, {
      localVue,
      stubs: ['router-link', 'router-view']
    })
    const spy = jest.spyOn(wrapper.vm, 'submitForm')
    wrapper.find('button').trigger('click')
    wrapper.vm.submitForm()
    expect(spy).toHaveBeenCalled()
  })
})
