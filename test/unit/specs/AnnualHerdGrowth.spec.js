import HerdAnnual from '@/pages/AnuualHerdGrowthPage/HerdAnnual.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuelidate from 'vuelidate'

const localVue = createLocalVue()
localVue.use(Vuelidate)

describe('HerdAnnual.vue', () => {
  let data
  beforeEach(() => {
    data = {herdGrowth: {
      calvingPercentage: '',
      numberOfCows: 0
    }}
  })
  it('Tests for the user inputing number of starting cows and calving percentage', () => {
    const wrapper = shallowMount(HerdAnnual, {
      data, localVue
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
    wrapper.find('form').trigger('submit.prevent')
    expect(wrapper.vm.herdSubmit).toBeTruthy()
  })
})
