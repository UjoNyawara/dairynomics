import PeerModel from '@/utils/peerModel/index.js'
import AxiosMockAdapter from 'axios-mock-adapter'
import axios from 'axios'

const mock = new AxiosMockAdapter(axios)

let peerModel
const mockMessages = [{
  id: 1094,
  sender_id: 3554,
  receiver_id: 3564,
  message: 'Something else',
  viewed: 0,
  created_at: '16:00PM June 14, 2019',
  updated_at: '08:48AM June 28, 2019',
  doReverse: false,
  status: true,
  stamp: new Date().getTime()
}, {
  id: 1095,
  sender_id: 3554,
  receiver_id: 3564,
  message: 'Something else',
  viewed: 0,
  created_at: '16:00PM June 14, 2019',
  updated_at: '08:48AM June 28, 2019',
  doReverse: false,
  status: true,
  stamp: new Date().getTime()

}, {
  id: 1095,
  sender_id: 3554,
  receiver_id: 3564,
  message: 'Something else',
  viewed: 0,
  created_at: '16:00PM June 14, 2019',
  updated_at: '08:48AM June 28, 2019',
  doReverse: false,
  status: true,
  stamp: new Date().getTime()

}]

const mockUsers = [{
  'id': '3564',
  'first_name': 'Kristoffer',
  'last_name': 'Stoltenberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'status': 'Offline',
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}, {
  'id': '3554',
  'first_name': 'Jolly',
  'last_name': 'Gratham',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'status': 'Offline',
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}]

describe('Peer model', () => {
  beforeAll(() => {
    mock.reset()
    peerModel = new PeerModel()
    peerModel.commit = jest.fn()
    peerModel.state = {}
  })

  it('should not connect a peer if no connection is open', () => {
    let user = { ...mockUsers[0], peerId: peerModel.getUserId(mockUsers[0]) }
    peerModel.connectPeer(user)
    expect(peerModel.connections).toHaveLength(0)
  })

  it('should open a peer connection', () => {
    const user = { ...mockUsers[1], peerId: peerModel.getUserId(mockUsers[1]) }
    mock.onPost('https://beta.cowsoko.com/api/v1/chat_peers/chat_id').reply(200)
    const savePeerId = jest.fn()
    peerModel.openConnection(user, savePeerId)
    expect(peerModel.peer).not.toBeUndefined()
  })

  it('should update connections', () => {
    const connection = { peerId: `${mockUsers[0].id}-${new Date().getTime()}`, on: jest.fn() }
    peerModel.updatePeerConnections(connection)
    expect(peerModel.connections).toHaveLength(1)
  })

  it('should send a message', () => {
    const peerId = `${mockUsers[0].id}-${new Date().getTime()}`
    const connection = { peerId, send: jest.fn(), on: jest.fn() }
    peerModel.findConnections = jest.fn().mockImplementation(() => [{
      userId: mockUsers[0].id,
      connection
    }])
    peerModel.transport({ ...mockMessages[0], peerId })
    expect(connection.send).toHaveBeenCalled()
  })

  it('should update status', () => {
    peerModel.state.users = mockUsers
    const connection = { peer: `${mockUsers[0].id}-${new Date().getTime()}` }
    peerModel.updateStatus(connection, 'Online')
    expect(peerModel.commit).toHaveBeenCalled()
    expect(peerModel.state.users[0].status).toEqual('Online')
  })

  it('should update status to Online', () => {
    peerModel.state.users = mockUsers
    const connection = { peer: `${mockUsers[0].id}-${new Date().getTime()}` }
    peerModel.updateStatus(connection, 'Online')
    expect(peerModel.commit).toHaveBeenCalled()
    expect(peerModel.state.users[0].status).toEqual('Online')
  })

  it('should keep status as Offline', () => {
    peerModel.state.users = mockUsers
    const connection = { peer: `${mockUsers[0].id}-${new Date().getTime()}` }
    peerModel.updateStatus(connection, 'Online')
    expect(peerModel.commit).toHaveBeenCalled()
    expect(peerModel.state.users[1].status).toEqual('Offline')
  })

  it('should not find user with the given connection', () => {
    mockUsers[0] = { ...mockUsers[0], status: 'Offline' }
    peerModel.state.users = mockUsers
    const connection = { peer: `${1000}-${new Date().getTime()}` }
    peerModel.updateStatus(connection, 'Online')
    expect(peerModel.state.users[0].status).toEqual('Offline')
    expect(peerModel.state.users[1].status).toEqual('Offline')
  })

  it('should find active a user with active connection', () => {
    const pModel = new PeerModel()
    pModel.commit = jest.fn()
    pModel.state = {}
    mockUsers.forEach(user => {
      pModel.updatePeerConnections({
        peerId: `${user.id}-${new Date().getTime()}`,
        on: jest.fn()
      })
    })
    expect(pModel.foundActiveUser(pModel.connections[0])).toBeTruthy()
  })

  it('should add a received message when chat mate id matches sender_id', () => {
    peerModel.state.chatMate = mockUsers[0]
    peerModel.state.messages = [mockMessages[0]]
    const payload = {
      sender_id: mockUsers[0].id,
      message: mockMessages[1]
    }
    peerModel.receiveMessage(payload)
    expect(peerModel.state.messages).toHaveLength(1)
  })

  it('should not add a received message if chat mate id does not match sender_id', () => {
    peerModel.state.chatMate = mockUsers[1]
    peerModel.state.messages = []
    const payload = {
      sender_id: mockUsers[0].id,
      message: mockMessages[1]
    }
    peerModel.receiveMessage(payload)
    expect(peerModel.state.messages).toHaveLength(0)
  })

  it('should prevent adding same received message multiple times', () => {
    peerModel.state.chatMate = mockUsers[0]
    peerModel.state.messages = [mockMessages[0], mockMessages[1]]
    const payload = {
      sender_id: mockUsers[0].id,
      message: mockMessages[1]
    }
    peerModel.receiveMessage(payload)
    expect(peerModel.state.messages).toHaveLength(2)
  })

  it('should ', () => {
    const on = jest.fn().mockImplementation((close, callback) => callback())
    const connection = { on }
    peerModel.addClosedConnectionListener(connection)
    expect(connection.on).toHaveBeenCalled()
  })

  it('should close an open connection', () => {
    peerModel.createPeerInstance(mockUsers[0])
    peerModel.peer.destroy = jest.fn()
    peerModel.closeConnection()
    expect(peerModel.peer.destroy).toHaveBeenCalled()
  })

  it('should return today if date is not provided', () => {
    expect(peerModel.formatDate()).toEqual('Today')
  })

  it('should format a date', () => {
    expect(peerModel.formatDate(mockMessages[0].updated_at)).toEqual('28 June  2019, 08:48am')
  })
})
