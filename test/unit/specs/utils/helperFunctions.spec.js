import Vue from 'vue'
import {
  authenticateUser,
  alertTokenError,
  alertConnectionError,
  findCounty,
  eventBus,
  closeModal,
  openModal,
  getAPIErrorMessage
} from '@/utils/helperFunctions'

const notifyFn = jest.fn()
Vue.notify = notifyFn

describe('helperFunctions', () => {
  describe('authenticateUser function', () => {
    it('should return "inValid: false" when no token is saved in localStorage', () => {
      const localStorageFn = jest.fn()
      Storage.prototype.getItem = localStorageFn
      const result = authenticateUser()
      expect(localStorageFn).toHaveBeenCalledTimes(1)
      expect(result).toEqual({ isValid: false, token: undefined })
    })

    it('should return "inValid: false" if token has expired', () => {
      const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTYwOTc5NzR9.JZGXZ3dI76Eicu9RzqkJ6US6wzoyxnAI2dIUTfMno0c'
      const localStorageFn = jest.fn(() => token)
      Storage.prototype.getItem = localStorageFn
      const result = authenticateUser()
      expect(result).toEqual({ isValid: false, token })
    })
  })

  describe('findCounty function', () => {
    it('should return "undefined" if no second parameter is supplied', () => {
      expect(findCounty([])).toBeUndefined()
    })

    it('findCounty function', () => {
      const county = { county: 'Nairobi' }
      expect(findCounty([county], 'nairobi')).toEqual(county)
    })
  })

  describe('alertTokenError function', () => {
    it('should call "Vue.notify" to alert tokenError"', () => {
      alertTokenError()
      expect(notifyFn).toHaveBeenCalledTimes(1)
    })
  })

  describe('alertConnectionError function', () => {
    it('should call "Vue.notify" to alert ConnectionError"', () => {
      alertConnectionError()
      expect(notifyFn).toHaveBeenCalledTimes(2)
    })
  })

  describe('closeModal function', () => {
    it('should call "eventBus.$emit"', () => {
      const emitFn = jest.fn()
      eventBus.$emit = emitFn
      closeModal()
      expect(emitFn).toHaveBeenCalledTimes(1)
      expect(emitFn).toHaveBeenCalledWith('closeModal')
    })
  })

  describe('open payment Modal function', () => {
    it('should call "eventBus.$emit"', () => {
      const emitFn = jest.fn()
      eventBus.$emit = emitFn
      openModal('receipt', 1)
      expect(emitFn).toHaveBeenCalledTimes(1)
      expect(emitFn).toHaveBeenCalledWith('showPaymentModal', 'receipt', 1)
    })
  })

  describe('open login/signup Modal function', () => {
    it('should call "eventBus.$emit"', () => {
      const emitFn = jest.fn()
      eventBus.$emit = emitFn
      openModal('login')
      expect(emitFn).toHaveBeenCalledTimes(1)
      expect(emitFn).toHaveBeenCalledWith('showModal', 'login')
    })
  })

  describe('getAPIErrorMessage function', () => {
    it('should return the right error message for param "401', () => {
      const result = getAPIErrorMessage(401)
      expect(result).toEqual('You are either not signed in or your token has expired. Please sign in and Try Again')
    })

    it('should return the right error message for param "404', () => {
      const result = getAPIErrorMessage(404)
      expect(result).toEqual('Sorry, We couldn\'t locate the requested information')
    })

    it('should return the right error message for param "500', () => {
      const result = getAPIErrorMessage(500)
      expect(result).toEqual('Sorry, Something went wrong while connecting to server, are you connected?')
    })
  })
})
