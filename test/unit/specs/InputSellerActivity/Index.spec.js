import { shallowMount, createLocalVue } from '@vue/test-utils'
import Index from '@/pages/InputSellerActivity/index.vue'
import Vuex from 'vuex'
const localVue = createLocalVue()
localVue.use(Vuex)
const store = new Vuex.Store({
  state: {
    uploadProductStore: {
      currentComponent: 'ProductSection'
    }
  },
  mutations: {
    CHANGE_COMPONENT: jest.fn()
  }
})

const wrapper = shallowMount(Index, {
  localVue,
  store
})

describe('Index Component', () => {
  it('should render the Index Component properly', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })

  it('should call the changeActiveLink Method', () => {
    const changeActiveLink = jest.spyOn(wrapper.vm, 'changeActiveLink')
    changeActiveLink('MyProducts')
    expect(wrapper.vm.changeActiveLink).toBeCalled()
  })
})
