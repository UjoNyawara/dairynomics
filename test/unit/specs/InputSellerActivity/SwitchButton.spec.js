import { shallowMount, createLocalVue } from '@vue/test-utils'
import SwitchButton from '@/pages/InputSellerActivity/SwitchButton.vue'
import Vuex from 'vuex'
const localVue = createLocalVue()
localVue.use(Vuex)
const store = new Vuex.Store({
  mutations: {
    CHANGE_COMPONENT: jest.fn()
  }
})

const wrapper = shallowMount(SwitchButton, {
  localVue,
  store
})

describe('SwitchButton Component', () => {
  it('should render the Switch Button Component properly', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })

  it('should call the Switcher Method', () => {
    const Switcher = jest.spyOn(wrapper.vm, 'switcher')
    Switcher()
    expect(wrapper.vm.switcher).toBeCalled()
  })
})
