import { shallowMount, createLocalVue } from '@vue/test-utils'
import UploadInputType from '@/pages/InputSellerActivity/UploadInputType'
import Vuex from 'vuex'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(FontAwesomeIcon)

const store = new Vuex.Store({
  state: {
    UserProfileModule: {
      user: {
        inputs_offered: '["hello"]',
        services_offered: 'world'
      }
    },
    uploadInputStore: {
      loading: true,
      error: false
    }
  },
  actions: {
    submitInputs: jest.fn()
  }
})

const wrapper = shallowMount(UploadInputType, {
  localVue,
  store
})

describe('UploadInputType Component', () => {
  it('should render the Upload Input Component properly', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })

  it('should show that getInitialCheck Method is called correctly', () => {
    const getInitialCheckSpy = jest.spyOn(wrapper.vm, 'getInitialCheck')
    getInitialCheckSpy()
    expect(wrapper.vm.getInitialCheck).toBeCalled()
  })

  it('should show that getInitialCheck Method is called correctly2', () => {
    const getInitialCheckSpy = jest.spyOn(wrapper.vm, 'getInitialCheck')
    expect(getInitialCheckSpy('world')).toEqual(['hello'])
  })

  it('should show that handleSubmit Method is called correctly', () => {
    const handleSubmitSpy = jest.spyOn(wrapper.vm, 'handleSubmit')
    wrapper.setData({
      dairymealSelectedValues: [1, 2, 3],
      fodderSelectedValues: [1, 2, 3],
      farmMachinerySelectedValues: [1, 2, 3],
      drugsSelectedValues: [1, 2, 3],
      irrigationSelectedValues: [1, 2, 3]
    })
    handleSubmitSpy()
    expect(wrapper.vm.handleSubmit).toBeCalled()
  })
})
