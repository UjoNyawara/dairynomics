import { shallowMount, createLocalVue } from '@vue/test-utils'
import ProductSection from '@/pages/InputSellerActivity/ProductSection.vue'
import Vuex from 'vuex'
const localVue = createLocalVue()
localVue.use(Vuex)
const store = new Vuex.Store({
  state: {
    uploadProductStore: {
      loading: true,
      products: [1, 2, 3],
      message: 'hello'
    }
  },
  actions: {
    fetchMyProducts: jest.fn()
  }
})

const wrapper = shallowMount(ProductSection, {
  localVue,
  store
})

describe('ProductSection Component', () => {
  it('should render the Product Section Component properly', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })
})
