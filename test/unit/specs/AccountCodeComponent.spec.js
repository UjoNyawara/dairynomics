import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import AccountCode from '@/components/ui/userAuth/AccountCode.vue'
import Vuelidate from 'vuelidate'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Vuelidate)
let actions
let getters
let store
let wrapper

describe('test  AccountCode Component', () => {
  beforeEach(() => {
    actions = { accountCode: jest.fn() }

    getters = {
      getForgotPin: () => {
        return {
          loading: false,
          user_id: '',
          success: null,
          forgotpin: '',
          verification_code: 2345
        }
      }
    }
    store = new Vuex.Store({actions, getters})
    wrapper = shallowMount(AccountCode, {localVue, store})
    wrapper.setData({
      accountCodeForm: {
        code: 2345,
        error: true,
        pin: '5678'
      }
    })
  })
  it('enter code', () => {
    wrapper.find('input').trigger('keyup.enter')
    expect(wrapper.vm.accountCodeForm.code).toBe(2345)
  })

  it('test should code with incorrect input', () => {
    wrapper.find('button').trigger('click')
  })

  it('enter code', () => {
    wrapper.find('input').trigger('keyup.enter')
    expect(wrapper.vm.accountCodeForm.code).toBe(2345)
  })

  it('should set button title to continue', () => {
    wrapper.setMethods({
      getCode: jest.fn()
    })
    wrapper.vm.$options.watch.getForgotPin.call(wrapper.vm, {loading: false, success: false})
    expect(wrapper.vm.accountCodeForm.buttonTitle).toBe('Continue')
  })

  it('should remove the title of the button when loading is true', () => {
    wrapper.vm.$options.watch.getForgotPin.call(wrapper.vm, { loading: true })
    expect(wrapper.vm.accountCodeForm.buttonTitle).toBe('')
  })

  it('should set render the page when the loading is false and success is true', () => {
    wrapper.vm.$options.watch.getForgotPin.call(wrapper.vm, {loading: false, success: true})
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should call get Code when there is no error', () => {
    wrapper.vm.getCode()
    expect(actions.accountCode).toHaveBeenCalled()
  })

  it('should return when the form is invalid', () => {
    wrapper.setData({
      accountCodeForm: {
        code: '',
        pin: 23
      }
    })
    wrapper.vm.getCode()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
