import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Switcher from '@/pages/FarmUpdates/FarmUpdateSwitcher.vue'

jest.mock('@/store')

const $router = {
  push: jest.fn()
}
const $route = {
  name: 'joshua'
}

let localVue = createLocalVue()
localVue.use(Vuex)
const wrapper = shallowMount(Switcher, {
  localVue,
  mocks: {
    $router,
    $route
  }
})

const methodSpy = (method) => jest.spyOn(wrapper.vm, method)

describe('FarmUpdates', () => {
  it('should fetch updates on created', () => {
    const viewProfile = methodSpy('viewProfile')
    viewProfile()
    expect($router.push).toHaveBeenCalled()
  })

  it('viewFarmUpdates method', () => {
    const viewFarmUpdates = methodSpy('viewFarmUpdates')
    viewFarmUpdates()
    expect($router.push).toHaveBeenCalled()
  })

  it('contactFarm method', () => {
    const contactFarm = methodSpy('contactFarm')
    contactFarm()
    expect($router.push).toHaveBeenCalledWith('#')
  })

  it('navItemStyle method', () => {
    const navItemStyle = methodSpy('navItemStyle')
    const result = navItemStyle('joshua')
    expect(result).toEqual('list-item active')
  })

  it('navItemstyle method', () => {
    const navItemStyle = methodSpy('navItemStyle')
    const result = navItemStyle('david')
    expect(result).toEqual('list-item')
  })
})
