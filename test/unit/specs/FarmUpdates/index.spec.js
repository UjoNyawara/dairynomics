import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import FarmUpdates from '@/pages/FarmUpdates/index.vue'

const $route = {
  params: {
    id: 34
  }
}

const $router = {
  push: jest.fn()
}

jest.mock('@/store')

let localVue = createLocalVue()
localVue.use(Vuex)

const getters = {
  GET_FARM_UPDATES: () => [{ update: { user: { first_name: 'derer', last_name: 'dedded', id: 23 } } }],
  GET_CURRENT_PAGE: () => 23,
  GET_LAST_PAGE: () => 23,
  farmInfo: () => ({
    farmDetails: {
      farm_name: 'joshua',
      county: 'joshua'
    }
  })
}
const actions = {
  fetchFarmUpdates: jest.fn(),
  SET_FARM_ID: jest.fn(),
  getFarmDetails: jest.fn()
}

const store = new Vuex.Store({ getters, actions })

const wrapper = shallowMount(FarmUpdates, {
  store,
  localVue,
  mocks: {
    $route,
    $router
  }
})

describe('FarmUpdates', () => {
  it('should fetch updates on created', () => {
    expect(actions.fetchFarmUpdates).toHaveBeenCalled()
  })
  it('should load more updates', () => {
    wrapper.vm.currentPage = 12
    wrapper.vm.lastPage = 14
    wrapper.vm.loadMore()
    expect(actions.fetchFarmUpdates).toHaveBeenCalled()

    wrapper.vm.currentPage = 12
    wrapper.vm.lastPage = 12
    wrapper.vm.loadMore()
    expect(actions.fetchFarmUpdates).toHaveBeenCalled()
  })
  it('watches for state changes', () => {
    let updates = [{update: {user: {first_name: 'derer', last_name: 'dedded', id: 23}}}]
    wrapper.vm.$options.watch.GET_FARM_UPDATES.call(wrapper.vm, updates)
    expect(wrapper.vm.GET_FARM_UPDATES).toEqual(updates)

    wrapper.vm.$options.watch.GET_CURRENT_PAGE.call(wrapper.vm, 23)
    expect(wrapper.vm.GET_CURRENT_PAGE).toEqual(23)

    wrapper.vm.$options.watch.GET_LAST_PAGE.call(wrapper.vm, 23)
    expect(wrapper.vm.GET_LAST_PAGE).toEqual(23)
  })

  it('loadmorecontent computed', () => {
    const result = wrapper.vm.loadMoreContent
    expect(result).toEqual('LOAD MORE')
  })

  it('loadmore action', () => {
    const loadMoreSpy = jest.spyOn(wrapper.vm, 'loadMore')
    loadMoreSpy()
    expect(actions.fetchFarmUpdates).toHaveBeenCalled()
  })

  it('loadmore action', () => {
    wrapper.setData({
      currentPage: 34,
      lastPage: 34
    })

    const loadMoreSpy = jest.spyOn(wrapper.vm, 'loadMore')
    loadMoreSpy()
    expect(actions.fetchFarmUpdates).toHaveBeenCalled()
  })

  it('loadmore action', () => {
    wrapper.setData({
      currentPage: 56,
      lastPage: 34
    })

    actions.fetchFarmUpdates.mockReset()
    const loadMoreSpy = jest.spyOn(wrapper.vm, 'loadMore')
    loadMoreSpy()
    expect(actions.fetchFarmUpdates).not.toHaveBeenCalled()
  })
})
