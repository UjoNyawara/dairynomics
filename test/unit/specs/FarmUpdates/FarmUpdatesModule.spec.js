import FarmUpdatesModule from '@/store/modules/FarmUpdatesModule'
import { mock } from '@/store/modules/mocks/mockInstance'

jest.mock('@/store')

const commit = jest.fn()

const state = {
  farmId: 4
}

describe('FarmUpdatesModule mutations', () => {
  beforeEach(() => {
    mock.reset()
  })

  it('updates state within farm', () => {
    let payload = []
    FarmUpdatesModule.mutations.POPULATE_FARM_UPDATES(FarmUpdatesModule.state, payload)
    expect(FarmUpdatesModule.state.farmUpdateList).toEqual(payload)
  })
  it('updates udates error within farm', () => {
    let payload = 'error'
    FarmUpdatesModule.mutations.POPULATE_FARM_UPDATES_ERROR(FarmUpdatesModule.state, payload)
    expect(FarmUpdatesModule.state.farmUpdatesError).toBe(payload)
  })
  it('updates farm details', () => {
    let payload = {}
    FarmUpdatesModule.mutations.POPULATE_FARM_DETAIL(FarmUpdatesModule.state, payload)
    expect(FarmUpdatesModule.state.farmDetail).toBe(payload)
  })
  it('updates current Page', () => {
    let payload = {}
    FarmUpdatesModule.mutations.POPULATE_CURRENT_PAGE(FarmUpdatesModule.state, payload)
    expect(FarmUpdatesModule.state.currentPage).toBe(payload)
  })
  it('updates last Page', () => {
    let payload = {}
    FarmUpdatesModule.mutations.POPULATE_LAST_PAGE(FarmUpdatesModule.state, payload)
    expect(FarmUpdatesModule.state.lastPage).toBe(payload)
  })
  it('updates  farm id', () => {
    let payload = 12
    FarmUpdatesModule.mutations.CHANGE_FARM_ID(FarmUpdatesModule.state, payload)
    expect(FarmUpdatesModule.state.farmId).toBe(payload)
  })
})
describe('SingleUpdateModule getters', () => {
  it('gets farm updates', () => {
    let state = {
      farmUpdateList: [{id: 23}, {id: 12}]
    }
    let result = FarmUpdatesModule.getters.GET_FARM_UPDATES(state)
    expect(state.farmUpdateList).toEqual(result)
  })
  it('gets farm detail', () => {
    let state = {
      farmDetail: {
      }
    }
    let result = FarmUpdatesModule.getters.GET_FARM_DETAIL(state)
    expect(state.farmDetail).toEqual(result)
  })
  it('gets current page number', () => {
    let state = {
      currentPage: 1
    }
    let result = FarmUpdatesModule.getters.GET_CURRENT_PAGE(state)
    expect(state.currentPage).toEqual(result)
  })
  it('gets last page number', () => {
    let state = {
      lastPage: 1
    }
    let result = FarmUpdatesModule.getters.GET_LAST_PAGE(state)
    expect(state.lastPage).toEqual(result)
  })
})
describe('FarmUpdatesModule actions', () => {
  afterEach(() => {
    mock.reset()
  })
  it('sets UserID', () => {
    let farmId = 12
    let commit = jest.fn()
    FarmUpdatesModule.actions.SET_FARM_ID({commit}, farmId)
    expect(commit).toHaveBeenCalledTimes(1)
  })

  it('fetches and paginates farmUpdates', () => {
    let commit = jest.fn()
    let response = {
      success: true,
      data: {
        updates: []
      },
      farm: [],
      meta: {
        current_page: 1,
        last_page: 2
      }

    }
    let pageNumber = 4
    mock.onGet('https://beta.cowsoko.com/api/v1/farms/4/updates?page=4').reply(200, response)
    FarmUpdatesModule.actions.fetchFarmUpdates({commit, state}, pageNumber).then(() => {
      expect(commit).toHaveBeenCalled()
    })
    // non pageination block
  })

  it('fetches and paginates farmUpdates', () => {
    let response = {
      success: true,
      data: {
        updates: []
      },
      farm: [],
      meta: {
        current_page: 1,
        last_page: 2
      }

    }
    mock.onGet('https://beta.cowsoko.com/api/v1/farms/4/updates').reply(200, response)
    FarmUpdatesModule.actions.fetchFarmUpdates({commit, state}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
    // non pageination block
  })

  it('fetches and paginates farmUpdates', () => {
    let response = {
      success: false,
      data: {
        updates: []
      },
      farm: [],
      meta: {
        current_page: 1,
        last_page: 2
      }

    }
    mock.onGet('https://beta.cowsoko.com/api/v1/farms/4/updates').reply(200, response)
    FarmUpdatesModule.actions.fetchFarmUpdates({commit, state}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
    // non pageination block
  })

  it('SET_FARM_ID test', () => {
    FarmUpdatesModule.actions.SET_FARM_ID({commit}, 34)
    expect(commit).toHaveBeenCalled()
  })
})
