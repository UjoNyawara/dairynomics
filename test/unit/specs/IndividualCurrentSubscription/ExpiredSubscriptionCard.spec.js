import { shallowMount } from '@vue/test-utils'
import ExpiredSubscriptionCard from '@/pages/IndividualCurrentSubscription/ExpiredSubscriptionCard.vue'

const propsData = {
  date: new Date()
}

const wrapper = shallowMount(ExpiredSubscriptionCard, { propsData })

describe('ExpiredSubscription Card test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
