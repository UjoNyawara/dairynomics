import { shallowMount, createLocalVue } from '@vue/test-utils'
import individualLivestockDetails from '@/pages/IndividualLivestock/IndividualLivestockDetails.vue'
import moment from 'vue-moment'

const localVue = createLocalVue()
localVue.use(moment)
jest.mock('@/store')

describe('individualLivestockDetails', () => {
  let wrapper

  it('should render the individualLivestockDetails component', () => {
    wrapper = shallowMount(individualLivestockDetails, {localVue,
      propsData: {
        livestock: {
          weigth: 6,
          fieldTagNum: 3456,
          bodyCondition: 'Medium',
          sex: 'Male',
          description: 'Dairy cow',
          market: '',
          postOn: new Date(),
          breed: '6',
          price: '1200'
        }
      }
    })
    expect(wrapper.vm).toBeTruthy()
  })
})
