import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import DropDown from '@/components/ui/DropDown.vue'
import * as helperFunctions from '@/utils/helperFunctions'
import VueRouter from 'vue-router'
import PeerModel from '@/utils/peerModel/index'
const peerModel = new PeerModel()

describe('@/components/ui/DropDown.vue', () => {
  const mutations = {
    toggleIsLoggedInState: jest.fn(),
    SET_PROFILE_PIC: jest.fn()
  }

  helperFunctions.openModal = jest.fn()

  const localVue = createLocalVue()
  localVue.use(VueRouter)
  localVue.use(Vuex)

  const router = new VueRouter()
  const store = new Vuex.Store({
    mutations,
    getters: {
      peerModel: () => peerModel
    }
  })

  const wrapper = shallowMount(DropDown, {
    propsData: {
      dropdownType: '',
      dropdownMenu: []
    },
    router,
    localVue,
    store
  })

  const spy = jest.spyOn(wrapper.vm.$router, 'push')

  it('should emit "hideDropdown" event', () => {
    wrapper.vm.handleClickAway()
    expect(wrapper.emitted().hideDropdown).toBeTruthy()
  })

  it('calls "openModal" when handleDropdownItemClick is called with "signup" or "login"', () => {
    wrapper.vm.handleDropdownItemClick('login')
    expect(helperFunctions.openModal).toHaveBeenCalledWith('login')
    wrapper.vm.handleDropdownItemClick('signup')
    expect(helperFunctions.openModal).toHaveBeenCalledWith('signup')
  })

  it('redirects when handleDropdownItemClick is called with "profile" or "editProfile"', () => {
    wrapper.vm.handleDropdownItemClick('profile')
    expect(spy).toHaveBeenCalledWith('/user-profile')
    wrapper.vm.handleDropdownItemClick('editProfile')
    expect(spy).toHaveBeenCalledWith('/edit-profile')
  })

  it('should logout when handleDropdownItemClick is called with "logout', () => {
    const localStorageFn = jest.fn()
    const user = { 'id': '3564' }
    const savePeerIdFn = jest.fn().mockImplementation(savePeerId => savePeerId(user.id))
    peerModel.openConnection(user, savePeerIdFn)
    Storage.prototype.clear = localStorageFn
    wrapper.vm.handleDropdownItemClick('logout')
    expect(localStorageFn).toHaveBeenCalledTimes(1)
    expect(mutations.SET_PROFILE_PIC).toHaveBeenCalledTimes(1)
    expect(mutations.toggleIsLoggedInState).toHaveBeenCalledTimes(1)
    expect(spy).toHaveBeenCalledWith('/')
  })

  it('redirects when handleDropdownItemClick is called with "updates" or "comments"', () => {
    wrapper.vm.handleDropdownItemClick('updates')
    expect(spy).toHaveBeenCalledWith('/updates/undefined')
    wrapper.vm.handleDropdownItemClick('comments')
    expect(spy).toHaveBeenCalledWith('/updates/undefined')
  })
})
