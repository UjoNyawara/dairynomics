import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import AppPublication from '@/components/ui/AppPublication.vue'
import Router from 'vue-router'
const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Router)

const wrapper = shallowMount(AppPublication, { localVue })

describe('AppPublication.vue Test', () => {
  it('should render app publication', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })

  it('download publication should emit download_publication', () => {
    const downloadPublicationSpy = jest.spyOn(wrapper.vm, 'downloadPublication')
    downloadPublicationSpy(2, 'joshua')
    expect(wrapper.emitted().download_publication).toEqual([[2, 'joshua']])
  })
})
