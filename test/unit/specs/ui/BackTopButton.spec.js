import { shallowMount } from '@vue/test-utils'
import backTopButton from '@/components/ui/BackTopButton.vue'
describe('@/components/ui/BackTopButton.vue', () => {
  let wrapper = shallowMount(backTopButton)
  it('should be able to render the component', () => {
    wrapper.vm.scrollHandler()
    expect(wrapper.vm.isVisible).toBe(false)
  })
  it('should call scrollTop Methods om click', () => {
    const spy = jest.spyOn(backTopButton.methods, 'scrollTop')
    wrapper = shallowMount(backTopButton, {
      data () {
        return { isVisible: true }
      }
    })
    wrapper.find('div').trigger('click')
    expect(spy).toHaveBeenCalled()
  })
  it('should call beforeDestroy before destroying the component', () => {
    const spy = jest.spyOn(backTopButton, 'beforeDestroy')
    wrapper = shallowMount(backTopButton, {
      data () {
        return { isVisible: true }
      }
    })
    wrapper.destroy()
    expect(spy).toHaveBeenCalled()
  })
})
