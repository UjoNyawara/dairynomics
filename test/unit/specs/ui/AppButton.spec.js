import { mount } from '@vue/test-utils'
import AppButton from '@/components/ui/AppButton.vue'

describe('@components/ui/AppButton.vue', () => {
  const wrapper = mount(AppButton)
  it('contains a button', () => {
    expect(wrapper.contains('button')).toBe(true)
  })
  it('emits a "click" property when button is clicked', () => {
    wrapper.find('button').trigger('click')
    expect(wrapper.emitted('click').length).toEqual(1)
  })
})
