import { createLocalVue, shallowMount } from '@vue/test-utils'
import ConfirmDialog from '@/components/ui/CommentsBlock/ConfirmDialog'

const propsData = {
  title: 'Joshua',
  body: 'David'
}

const localVue = createLocalVue()
const wrapper = shallowMount(ConfirmDialog, {localVue, propsData})

describe('ConfirmDialog', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('confirm method', () => {
    const confirmSpy = jest.spyOn(wrapper.vm, 'confirm')
    confirmSpy('joshua')
    expect(wrapper.emitted('onConfirm')).toBeTruthy()
  })
})
