import { mount } from '@vue/test-utils'
import CommentInput from '@/components/ui/CommentsBlock/CommentInput.vue'

let wrapper

describe('@components/ui/CommentsBlock/CommentInput.vue', () => {
  beforeEach(() => {
    wrapper = mount(CommentInput)
  })
  it('contains the `cancel button`', () => {
    expect(wrapper.contains('.cancel-button')).toBe(true)
  })

  it('contains the `post button`', () => {
    expect(wrapper.contains('.post-button')).toBe(true)
  })

  it('emits a "click" property when `cancel button` is clicked', () => {
    wrapper.setData({ body: 'body' })
    wrapper.find('.cancel-button').trigger('click')
    expect(wrapper.emitted('cancelComment')).toBeTruthy()
  })

  it('emits a "click" property when `post button` is clicked', () => {
    wrapper.setData({ body: 'body' })
    wrapper.find('.post-button').trigger('click')
    expect(wrapper.emitted('submitComment')).toBeTruthy()
  })
})
