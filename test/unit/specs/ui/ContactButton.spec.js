import { shallowMount, createLocalVue } from '@vue/test-utils'
import ContactButton from '@/components/ui/ContactButton.vue'

const localVue = createLocalVue()

describe('404Page', () => {
  let wrapper

  it('should render the ContactButton component', () => {
    wrapper = shallowMount(ContactButton, { localVue })
    expect(wrapper.html()).toContain('<div><button class="contact-seller-button text-center"></button></div>')
  })
})
