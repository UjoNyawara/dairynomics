import { shallowMount } from '@vue/test-utils'
import PerformanceMeter from '@/components/ui/PerformanceMeter.vue'

describe('PerformanceMeter.vue', () => {
  let wrapper
  beforeAll(done => {
    wrapper = shallowMount(PerformanceMeter, {
      propsData: {
        imageName: 'cow-image.png',
        performance: 3,
        strokeWidth: 5,
        size: 150,
        fontSize: 15
      }
    })
    setTimeout(done, 500)
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render correct contents', () => {
    expect(wrapper.contains('svg')).toBe(true)
    expect(wrapper.contains('circle')).toBe(true)
    expect(wrapper.contains('text')).toBe(true)
    expect(wrapper.contains('img')).toBe(true)
  })
  it('should run the minimumPossibleValueToNumber method if the minimumPossibleValue props exists', () => {
    wrapper.setProps({ minimumPossibleValue: '50' })
    expect(wrapper.vm.minimumPossibleValueToNumber).toEqual(50)
  })
  it('should run the reverseValue method if the reverse props exists', () => {
    wrapper.setData({ measure: -1 })
    wrapper.setProps({ reverse: true, maximumPossibleValue: '50', minimumPossibleValue: '10' })
    expect(Math.round(wrapper.vm.reverseValue)).toEqual(-837)
  })
  it('should render calving meter', () => {
    const wrapper = shallowMount(PerformanceMeter, {
      propsData: {
        performance: 3,
        strokeWidth: 5,
        size: 150,
        fontSize: 15,
        imageName: 'calf.svg',
        maximumPossibleValue: 40,
        minimumPossibleValue: 24,
        reverse: true
      }
    })
    expect(wrapper.contains('svg')).toBe(true)
    expect(wrapper.contains('circle')).toBe(true)
    expect(wrapper.contains('text')).toBe(true)
    expect(wrapper.contains('img')).toBe(true)
  })
})
