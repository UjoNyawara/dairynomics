import { shallowMount } from '@vue/test-utils'
import Switcher from '@/components/ui/Switcher.vue'

describe('@components/ui/Switcher.vue', () => {
  const propsObject = {
    propsData: {
      switcherOptionList: [{
        optionName: 'My updates',
        linkName: 'myUpdates'
      }],
      activeLink: 'myUpdates'
    }
  }
  const wrapper = shallowMount(Switcher, propsObject)
  it('emits a "changeIsActiveLinkStatus" event when switcher option is click', () => {
    wrapper.find('.my-activities').trigger('click')
    expect(wrapper.emitted('changeActiveLinkStatus').length).toEqual(1)
  })
})
