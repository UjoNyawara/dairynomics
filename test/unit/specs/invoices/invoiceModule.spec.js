import AllInvoicesModule from '@/store/modules/AllInvoicesModule'
import { mock } from '@/store/modules/mocks/mockInstance'
jest.mock('@/store')
afterEach(() => {
  mock.reset()
})
const state = {
  invoices: [],
  loading: false,
  singleInvoice: {},
  orderList: [],
  billing: {}
}
let invoices
let singleInvoce = {
  orderId: '6bb0e28d-05d7-465b-9d0e-b60a971c69d6',
  amountReceived: '30000',
  paymentStatus: 'unpaid'
}
describe('all invoice vuex state', () => {
  it('should test get all invoice mutations', () => {
    beforeEach(() => {})
    invoices = [
      {
        orderId: '6bb0e28d-05d7-465b-9d0e-b60a971c69d6',
        amountReceived: '30000',
        paymentStatus: 'unpaid'
      },
      {
        orderId: '6bb0e28d-05d7-465b-9d0e-b60a971c69d6',
        amountReceived: '30000',
        paymentStatus: 'unpaid'
      }
    ]
    AllInvoicesModule.mutations.SETSTATTETOINVOICES(state, invoices)
    expect(state).toEqual({ ...state, invoices })
  })
  it('should test the show loading mutation', () => {
    const loading = true
    AllInvoicesModule.mutations.SHOWLOADING(state, loading)
    expect(state).toEqual({ ...state, loading })
  })
  it('should test set single invoice mutations', () => {
    AllInvoicesModule.mutations.SETSINGLEINVOICE(state, singleInvoce)
    expect(state).toEqual({
      invoices: [
        {
          amountReceived: '30000',
          orderId: '6bb0e28d-05d7-465b-9d0e-b60a971c69d6',
          paymentStatus: 'unpaid'
        },
        {
          amountReceived: '30000',
          orderId: '6bb0e28d-05d7-465b-9d0e-b60a971c69d6',
          paymentStatus: 'unpaid'
        }
      ],
      loading: true,
      singleInvoice: {
        amountReceived: '30000',
        orderId: '6bb0e28d-05d7-465b-9d0e-b60a971c69d6',
        paymentStatus: 'unpaid'
      },
      orderList: [],
      billing: {}
    })
  })
  it('should test reset invoice mutation', () => {
    const emptyInvoice = {}
    AllInvoicesModule.mutations.RESETINVOICE(state, emptyInvoice)
    expect(state).toEqual({...state, singleInvoce: emptyInvoice})
  })
  it('should test the orderlist mutation', () => {
    const orderList = []
    AllInvoicesModule.mutations.SETORDERLIST(state, orderList)
    expect(state.orderList).toEqual(orderList)
  })
  it('should test GETINVOICE Getters', () => {
    AllInvoicesModule.getters.GETINVOICES(state)
    expect(state).toEqual({...state, invoices})
  })
  it('should test the billing mutation', () => {
    const billing = {
      fullName: 'anyatibrian',
      address: 'kampala, Uganda',
      phoneNumber: '0781901036'
    }
    AllInvoicesModule.mutations.BIILLINGINFOR(state, billing)
    expect(state).toEqual({...state, billing: billing})
  })
})

describe('TEST ALL ACTIONS', () => {
  it('should test get all invoices in the actions', () => {
    const commit = jest.fn()
    AllInvoicesModule.actions.getAllInvoices({ commit, state })
    expect(commit).toHaveBeenCalledTimes(1)
    expect(commit).toHaveBeenCalledWith('SHOWLOADING', true)
  })
  it('should test get single invoice action', () => {
    const commit = jest.fn()
    AllInvoicesModule.actions.getSingleInvoice({commit, state}, {invoiceId: 1})
    expect(commit).toHaveBeenCalled()
    expect(commit).toHaveBeenCalledWith('SHOWLOADING', true)
  })
  it('should test reset invoice actions', () => {
    const commit = jest.fn()
    AllInvoicesModule.actions.resetInvoice({commit})
    expect(commit).toHaveBeenCalledWith('RESETINVOICE')
  })
})
