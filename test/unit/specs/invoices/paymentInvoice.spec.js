import { createLocalVue, shallowMount } from '@vue/test-utils'
import Vuex from 'vuex'
import PaymentInvoice from '@/components/ui/paymentDetails/PaymentInvoice'
const localVue = createLocalVue()
localVue.use(Vuex)

const PaymentComponent = () => {
  const getters = {
    USER_PROFILE: userprofileState => userprofileState.user
  }
  const state = {
    user: {},
    error: null,
    isLoading: false,
    singleInvoice: {},
    profilePic: require('@/assets/profile.png')
  }
  const orderList = {
    orderItem: {
      product: {
        productName: 'productName'}
    }
  }
  const store = {getters, state}
  return shallowMount(PaymentInvoice, {store,
    localVue,
    propsData: {
      invoice: 'invoice',
      isLoading: 'isLoading',
      billing: 'billing',
      orderList: orderList
    }})
}
describe('Payment Invoices', () => {
  let wrapper
  // let instance

  beforeEach(() => {
    wrapper = PaymentComponent()
    // instance = wrapper.vm
  })
  it('should load without fail', () => {
    expect(wrapper).toMatchSnapshot()
  })
  it('should test the close modal method', () => {
    const closeModalSpy = jest.spyOn(wrapper.vm, 'closeModal')
    closeModalSpy()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
