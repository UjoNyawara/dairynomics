import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'
import Router from 'vue-router'
import mockUpdates from './__mock__/updates'
import mockUser from './__mock__/user'
import testHelpers from '@/utils/testHelpers'
import AllUpdates from '@/pages/Updates/AllUpdates'
import UpdateCard from '@/components/ui/UpdateCard'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Router)
const router = new Router()

describe('AllUpdates Component', () => {
  let wrapper
  let store
  let actions
  let state = {
    user: mockUser,
    mainUpdates: {
      allUpdates: [],
      expertsUpdates: {
        'expert-id': []
      },
      requestStatuses: {
        isFetching: false
      }
    }
  }

  const getters = {
    getAllUpdates: updatesState => updatesState.mainUpdates.allUpdates,
    getExpertsUpdates: updatesState => updatesState.mainUpdates.expertsUpdates,
    getUpdatesRequestStatuses: updatesState =>
      updatesState.mainUpdates.requestStatuses,
    USER_PROFILE: profileState => profileState.user,
    PROFILE_PIC: profileState => profileState.profilePic
  }

  const mountComponent = () => {
    actions = {
      fetchUpdates: jest.fn(),
      GET_USER_PROFILE: jest.fn()
    }

    store = new Vuex.Store({ state, getters, actions })
    wrapper = mount(AllUpdates, { store, localVue, router })
    testHelpers.wrapper = wrapper
    return wrapper
  }

  it('does not show update items when update list is empty', () => {
    state.mainUpdates.allUpdates = []
    mountComponent()
    testHelpers.notSeeElement('.update-card')
    testHelpers.notSeeElement(UpdateCard)
  })

  it('lists updates items when item exist in list ', () => {
    state.mainUpdates.allUpdates = mockUpdates
    wrapper = mountComponent()
    testHelpers.seeElement(UpdateCard)
    testHelpers.seeString(mockUpdates[0].user.first_name)
    testHelpers.seeElement('.update-card')
  })

  it('handle a case when isFetching is true', () => {
    state.mainUpdates.allUpdates = mockUpdates
    state.mainUpdates.requestStatuses.isFetching = true
    wrapper = mountComponent()
    testHelpers.seeElement(UpdateCard)
    testHelpers.seeString(mockUpdates[0].user.first_name)
    testHelpers.seeElement('.update-card')
  })

  it('handles actions called on clicks', async () => {
    wrapper = mountComponent()
    expect(actions.fetchUpdates).toHaveBeenCalled()

    // load more button
    let button = wrapper.find('.btn.big-full')
    await button.trigger('click')
    expect(actions.fetchUpdates).toHaveBeenCalled()
  })
})
