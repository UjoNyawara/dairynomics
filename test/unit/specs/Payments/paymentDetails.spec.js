import { shallowMount, createLocalVue } from '@vue/test-utils'
import PaymentDetails from '@/components/ui/paymentDetails/PaymentDetails'
import Vue from 'vue'
import { eventBus } from '@/utils/helperFunctions'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
const router = new VueRouter()

const EventBus = new Vue()

const GlobalPlugins = {
  install (v) {
    // Event bus
    v.prototype.$bus = EventBus
  }
}

localVue.use(GlobalPlugins)

describe('PaymentDetails', () => {
  let wrapper

  test('should trigger the closePaymentModal method', () => {
    wrapper = shallowMount(PaymentDetails)
    const spy = jest.spyOn(wrapper.vm, 'closePaymentModal')
    wrapper.setData({
      showModal: false,
      content: 'receipt'
    })
    wrapper.vm.closePaymentModal()
    expect(spy).toHaveBeenCalled()
  })

  it('eventBus bus on created should show form', () => {
    eventBus.$emit('showPaymentModal', 'receipt')
    expect(wrapper.vm.content).toEqual('receipt')
  })

  it('eventBus bus on created should not show payment modal', () => {
    eventBus.$emit('closeModal')
    expect(wrapper.vm.showPaymentModal).toEqual(false)
  })
})

describe('Payment Details State', () => {
  const getReceipt = jest.fn()
  const resetReceipt = jest.fn()

  const store = new Vuex.Store({
    modules: {
      IndividualReceipt: {
        state: {
          isLoading: false,
          receipt: []
        },

        actions: { getReceipt, resetReceipt }
      }
    }
  })

  const wrapper = shallowMount(PaymentDetails, {
    localVue,
    store,
    router
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should call "getReceipt" on mounting', () => {
    expect(getReceipt).toHaveBeenCalledTimes(1)
  })

  it('should call "resetReceipt" on mounting', () => {
    expect(resetReceipt).toHaveBeenCalledTimes(1)
  })
})
