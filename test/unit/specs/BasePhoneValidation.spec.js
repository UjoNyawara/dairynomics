import Vuex from 'vuex'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import BasePhoneValidation from '@/components/PhoneValidation/BasePhoneValidation'

const localVue = createLocalVue()
localVue.use(Vuex)
let actions
let store
describe('test BasePhoneValidation Component', () => {
  beforeEach(() => {
    actions = { verifyToken: jest.fn() }
  })
  store = new Vuex.Store({
    actions,
    state: {
      verification: '',
      verificationCode: '',
      token: '',
      erro: null,
      message: '',
      user_id: 1883,
      spinner: false,
      phoneValidation: {erro: true},
      signup: {userAuthSignUp: {userId: ''}}
    }
  })
  it('is a Vue instance', () => {
    const wrapper = shallowMount(BasePhoneValidation, {localVue, store})
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
  it('BasePhoneValidation snapshot', () => {
    const wrapper = shallowMount(BasePhoneValidation, {localVue, store})
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
  it('component should have form', () => {
    const wrapper = shallowMount(BasePhoneValidation, {localVue, store})
    expect(wrapper.findAll('form')).toHaveLength(1)
  })
  it('test onclick should call executeSearch method', () => {
    const executeSearch = jest.fn()
    const iconSpin = jest.fn()
    const store = new Vuex.Store({
      actions,
      state: {
        verification: '',
        verificationCode: '',
        token: '',
        erro: {},
        message: '',
        spinner: true,
        phoneValidation: {erro: true},
        signup: {userAuthSignUp: {userId: ''}}
      }
    })
    const wrapper = mount(BasePhoneValidation, {
      localVue,
      store,
      propsData: {
        executeSearch,
        iconSpin
      }})
    wrapper.find('button').trigger('click')
    expect(wrapper.findAll('div.error-failure')).toHaveLength(1)
  })
  it('test should return missing PIN error message', () => {
    const store = new Vuex.Store({
      actions,
      state: {
        verification: '',
        verificationCode: '',
        token: '',
        erro: {},
        message: '',
        user_id: 1883,
        spinner: false,
        phoneValidation: {erro: {success: false, error: {verification_code: ['PIN MISSING']}}},
        signup: {userAuthSignUp: {userId: ''}}
      }
    })
    const wrapper = mount(BasePhoneValidation, {
      localVue,
      store
    })
    wrapper.find('button').trigger('click')
    expect(wrapper.html()).toContain(`PIN MISSING`)
  })
  it('test should return Invalid PIN for incorrect PIN input', () => {
    const store = new Vuex.Store({
      actions,
      state: {
        verification: '',
        verificationCode: '',
        token: '',
        erro: {},
        message: '',
        user_id: 1883,
        spinner: false,
        phoneValidation: {erro: {success: false, error: 'Invalid PIN'}},
        signup: {userAuthSignUp: {userId: ''}}
      }
    })
    const wrapper = mount(BasePhoneValidation, {
      localVue,
      store
    })
    wrapper.find('button').trigger('click')
    expect(wrapper.html()).toContain(`Invalid or Expired PIN`)
  })
  it('test should return NULL on correct PIN input', () => {
    const store = new Vuex.Store({
      actions,
      state: {
        verification: '',
        verificationCode: '',
        token: '',
        erro: {},
        message: '',
        user_id: 1883,
        spinner: false,
        phoneValidation: {erro: {success: true}},
        signup: {userAuthSignUp: {userId: ''}}
      }
    })
    const wrapper = mount(BasePhoneValidation, {
      localVue,
      store
    })
    wrapper.find('button').trigger('click')
    expect(wrapper.html()).not.toContain('Invalid or Expired PIN')
  })
  it('test should send text SMS on correct PIN input', () => {
    const wrapper = mount(BasePhoneValidation, {
      mocks: {$store: {dispatch: jest.fn(), state: {signup: {userAuthSignUp: {userId: true}}, phoneValidation: {verification: ''}}}}
    })
    const sendMessageMock = jest.fn()
    wrapper.vm.sendMessage = sendMessageMock
    wrapper.find('button').trigger('click')
    expect(wrapper.vm.sendMessage).toBe(true)
  })
  it('test should not send text SMS on incorrect PIN input', () => {
    const wrapper = mount(BasePhoneValidation, {
      mocks: {$store: {dispatch: jest.fn(), state: {signup: {userAuthSignUp: {userId: true}}, phoneValidation: {erro: true}}}}
    })
    const sendMessageMock = jest.fn()
    wrapper.vm.sendMessage = sendMessageMock
    wrapper.find('button').trigger('click')
    expect(wrapper.vm.sendMessage).toBe(false)
  })

  it('executeSearch should call this.store.dispatch when the form is valid', () => {
    const wrapper = mount(BasePhoneValidation, {
      mocks: {$store: {dispatch: jest.fn(), state: {signup: {userAuthSignUp: {userId: null}}, phoneValidation: {erro: true}}}}
    })
    const executeSearchSpy = jest.spyOn(wrapper.vm, 'executeSearch')
    executeSearchSpy()
    expect(wrapper.vm.display_err).toEqual('Currently not registered, please sign up.')
  })

  it('icon spin computed should return the phoneValidation.spinner', () => {
    const wrapper = mount(BasePhoneValidation, {
      mocks: {$store: {dispatch: jest.fn(), state: {signup: {userAuthSignUp: {userId: null}}, phoneValidation: {erro: true, spinner: true}}}}
    })
    expect(wrapper.vm.iconSpin).toBeTruthy()
  })

  it('userpin setter should set query to the value', () => {
    const wrapper = mount(BasePhoneValidation, {
      mocks: {$store: {dispatch: jest.fn(), state: {signup: {userAuthSignUp: {userId: null}}, phoneValidation: {erro: true, spinner: true}}}}
    })
    wrapper.vm.userPin = 'joshua'
    expect(wrapper.vm.userPin).toEqual('joshua')
  })
})
