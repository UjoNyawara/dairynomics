import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import UserProfile from '@/pages/UserProfileTimeline/UserProfile.vue'
import Vuelidate from 'vuelidate'

const localVue = createLocalVue()
localVue.use(Vuelidate)
localVue.use(Vuex)
describe('UserProfile.vue', () => {
  let wrapper
  let actions
  let store
  let getters
  let data
  let watch
  beforeAll(() => {
    actions = {
      editUser: jest.fn(),
      userDetails: jest.fn()
    }
    data = {
      firstName: 'sam',
      lastName: 'rubarema',
      county: 'igara',
      oldPassword: 9999,
      updatedMessage: false,
      password: 4543,
      confirmPassword: 4543,
      accountType: 'I am a dairy farmer',
      imageData: 'https://beta.cowsoko.com/storage/img/avatar_dwe_1554728809_96130.jpeg',
      file: 'sample.txt'
    }
    watch = {
      getUserUpdateStatus: jest.fn()
    }
    getters = { getUserUpdateStatus: () => {
      return { userUpdateStatus: false,
        successMessage: 'well done',
        updateError: null,
        updateSuccess: false,
        getUserDetails: {profile: {pic: ''}, county: 'samuel'}
      }
    }}
    store = new Vuex.Store({
      actions, getters, watch
    })
    wrapper = shallowMount(UserProfile, { store,
      localVue
      // mocks: {
      //   $refs: {
      //     fileInput: {
      //       files: [file]
      //     }
      //   }
      // }
    })
  })

  it('should trigger and throw an error when form is invalid', () => {
    wrapper.setData({
      ...data
    })
    const input = wrapper.find('.file-input')
    input.trigger('input')
    wrapper.find('button[type="button"]').trigger('click')
    expect(actions.editUser).toHaveBeenCalled()
  })

  it('should set update error to empty string when the old password is changing', () => {
    wrapper.vm.$options.watch.oldPassword.call(wrapper.vm, 'new_password')
    expect(wrapper.vm.getUserUpdateStatus.updateError).toEqual('')
  })

  it('should render when there is no success message', () => {
    wrapper.vm.$options.watch.getUserUpdateStatus.call(wrapper.vm, {successMessage: false})
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render where there is no user profile details', () => {
    getters = { getUserUpdateStatus: () => {
      return { userUpdateStatus: null, successMessage: false }
    }}
    store = new Vuex.Store({
      actions, getters, watch
    })
    wrapper = shallowMount(UserProfile, { store,
      localVue})

    wrapper.vm.$options.watch.getUserUpdateStatus.call(wrapper.vm, 'message')

    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render where there is no user profile details', () => {
    getters = { getUserUpdateStatus: () => {
      return { userUpdateStatus: null }
    }}
    store = new Vuex.Store({
      actions, getters, data, watch
    })
    wrapper = shallowMount(UserProfile, { store,
      localVue})

    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('chooseImage should render when the button is clicked', () => {
    const chooseImageSpy = jest.spyOn(wrapper.vm, 'chooseImage')
    chooseImageSpy()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render when render Message function has been called', () => {
    const renderMessageSpy = jest.spyOn(wrapper.vm, 'renderMessage')
    renderMessageSpy()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render when the firstname field has error', () => {
    wrapper.setData({
      firstName: '',
      lastName: 'joshua'
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
