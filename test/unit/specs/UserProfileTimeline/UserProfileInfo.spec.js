import { shallowMount, createLocalVue } from '@vue/test-utils'
import UserProfileInfo from '@/pages/UserProfileTimeline/UserProfileInfo.vue'
import router from '@/router/router'
const localVue = createLocalVue()

jest.mock('@/router/router')

const $router = {
  push: jest.fn()
}
const wrapper = shallowMount(UserProfileInfo, {localVue,
  router,
  mocks: {
    $router
  }})
describe('<UserProfileInfo>', () => {
  it('Component renders correctly', () => {
    wrapper.setProps({
      userNames: 'Joshua',
      county: 'I am diary Farmer',
      userType: 'farmer',
      profile_pic: 'https://joshua.pic'
    })

    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
