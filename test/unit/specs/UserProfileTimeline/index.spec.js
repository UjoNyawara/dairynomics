import { shallowMount } from '@vue/test-utils'
import UserProfileTimeline from '@/pages/UserProfileTimeline/UserProfileTimeline.vue'
import UserProfileTimelineIndex from '@/pages/UserProfileTimeline/index.vue'
import UserProfile from '@/pages/UserProfileTimeline/UserProfile.vue'
import UserReport from '@/pages/UserProfileTimeline/UserReport.vue'
import ProfileSideBarLayout from '@/components/layout/ProfileSideBarLayout.vue'

describe('index page for user profile timeline', () => {
  const wrapper = shallowMount(UserProfileTimelineIndex)

  it('renders the Layout component on load', () => {
    expect(wrapper.find(ProfileSideBarLayout).exists()).toBe(true)
  })

  it('renders the UserProfileTimeline component on page load default', () => {
    expect(wrapper.find(UserProfileTimeline).exists()).toBe(true)
    expect(wrapper.find(UserProfile).exists()).toBe(false)
    expect(wrapper.find(UserReport).exists()).toBe(false)
  })
})
