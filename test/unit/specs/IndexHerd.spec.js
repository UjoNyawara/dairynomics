import HerdAnnual from '@/pages/AnuualHerdGrowthPage/HerdAnnual.vue'
import { shallowMount } from '@vue/test-utils'
import index from '@/pages/AnuualHerdGrowthPage/index.vue'
import HerdGrowth from '@/pages/AnuualHerdGrowthPage/HerdGrowth.vue'

describe('index.vue', () => {
  let wrapper
  beforeAll(() => {
    wrapper = shallowMount(index, {
    })
  })
  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
  it('has recieved property', () => {
    expect(wrapper.vm.calvingPercentage).toEqual('')
    expect(wrapper.vm.numberOfCows).toEqual(0)
  })
  it('should render HerdAnnual only on initial rendering', () => {
    expect(wrapper.findAll(HerdAnnual).length).toBe(1)
    expect(wrapper.findAll(HerdGrowth).length).toBe(0)
  }
  )
  it('should render the intial page only', () => {
    wrapper.find(HerdAnnual).vm.$emit('submit', 10)
    expect(wrapper.findAll(HerdAnnual).length).toBe(0)
    expect(wrapper.findAll(HerdGrowth).length).toBe(1)
  })
  it('should render HerdGrowth only', () => {
    wrapper.find(HerdGrowth).vm.$emit('herd')
    expect(wrapper.findAll(HerdAnnual).length).toBe(0)
    expect(wrapper.findAll(HerdGrowth).length).toBe(1)
  })
}
)
