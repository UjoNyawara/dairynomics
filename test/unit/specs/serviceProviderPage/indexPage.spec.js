import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Vuelidate from 'vuelidate'
import serviceProviderPage from '@/pages/ServiceProviderOnBoarding/index'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Vuelidate)
localVue.use(VueRouter)

describe('serviceProviderPage', () => {
  let wrapper

  it('should call the submitUserDetails method on click of button serviceProviderPage', () => {
    wrapper = shallowMount(serviceProviderPage, {
      localVue,
      stubs: ['router-link', 'router-view']
    })
    const spy = jest.spyOn(wrapper.vm, 'submitUserDetails')
    wrapper.setData({
      serviceProviders: {
        schoolAttended: 'Egerton University',
        expertFrom: 'Animal Production',
        courseStudied: 'Animal Production',
        nationalId: 'Animal Production',
        certification: 'Degree',
        dateDuration: {
          from: new Date(),
          to: new Date()
        }
      }
    })
    wrapper.find('button').trigger('click')
    expect(spy).toHaveBeenCalled()
  })

  it('should return error message when data is not correct', () => {
    wrapper = shallowMount(serviceProviderPage, { localVue, stubs: ['router-link', 'router-view'] })
    // const spy = jest.spyOn(wrapper.vm, 'submitUserDetails')
    wrapper.setData({
      serviceProviders: {
        schoolAttended: 'Egerton University',
        expertFrom: 'Animal Production',
        courseStudied: 'Animal Pro44&*duction',
        nationalId: 'Animal Production',
        certification: 'Degree',
        dateDuration: {
          from: new Date(),
          to: new Date()
        },
        errorMessage: '',
        isDisabled: true
      }
    })
    wrapper.find('button').trigger('click')
    expect(wrapper.vm.errorMessage).toBe('Only alphabets are allowed')
  })

  it('should submit data on click of button serviceProviderPage', () => {
    wrapper = shallowMount(serviceProviderPage, {
      localVue,
      stubs: ['router-link', 'router-view']
    })
    const spy = jest.spyOn(wrapper.vm, 'submitUserDetails')
    wrapper.setData({
      serviceProviders: {
        schoolAttended: 'Egerton University',
        expertFrom: 'Animal Production',
        courseStudied: 'Animal Production',
        nationalId: 'Animal Production',
        certification: 'Degree',
        dateDuration: {
          from: new Date(),
          to: new Date()
        },
        stubs: ['router-link', 'router-view']
      }
    })
    wrapper.find('button').trigger('click')
    expect(spy).toHaveBeenCalled()
  })

  it('should submit data on click of button', () => {
    wrapper = shallowMount(serviceProviderPage, {
      localVue,
      stubs: ['router-link', 'router-view']
    })
    const spy = jest.spyOn(wrapper.vm, 'submitUserDetails')
    wrapper.setData({
      serviceProviders: {
        schoolAttended: 'Egerton University',
        expertFrom: 'Animal Production',
        courseStudied: 'Animal Production',
        nationalId: '12234567',
        certification: 'Degree',
        dateDuration: {
          from: new Date(),
          to: new Date()
        }
      }
    })
    wrapper.find('button').trigger('click')
    expect(spy).toHaveBeenCalled()
  })
  it('should call the updateDate method and pass data of date', () => {
    wrapper = shallowMount(serviceProviderPage, {
      localVue,
      stubs: ['router-link', 'router-view']
    })
    const data = {
      input: 'from',
      model: 'Sun Mar 31 2019 12:53:15 GMT+0100 (West Africa Standard Time)' }
    wrapper.vm.updateDate(data)
  })
})
