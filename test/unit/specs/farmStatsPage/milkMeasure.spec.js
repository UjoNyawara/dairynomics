import { shallowMount } from '@vue/test-utils'
import MilkMeasure from '@/pages/FarmStatsPage/MilkMeasure.vue'
import AppButton from '@/components/ui/AppButton.vue'
import IntegerInput from '@/components/ui/IntegerInput.vue'

describe('MilkMeasure.vue', () => {
  let wrapper
  beforeAll(() => {
    wrapper = shallowMount(MilkMeasure, {
      propsData: {
        milkQuantity: 0
      }
    })
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.findAll(IntegerInput)).toHaveLength(1)
    expect(wrapper.findAll(AppButton)).toHaveLength(1)
  })

  it('has recieved "milkQuantity" property', () => {
    expect(wrapper.vm.milkQuantity).toEqual(0)
  })

  it('should not display error message, disable button and be unable to submit on initial rendering', () => {
    expect(wrapper.find('.error-message').text()).toEqual('')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should display an error, disable button and be unable to submit when input value is empty', () => {
    wrapper.setData({ quantity: '' })
    expect(wrapper.find('.error-message').text())
      .toEqual('Quantity is required')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should display an error, disable button and be unable to submit when input value is "zero"', () => {
    wrapper.setData({ quantity: 0 })
    expect(wrapper.find('.error-message').text())
      .toEqual('Quantity should be greater than 0')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should display an error and disable button when quantity is greater than 50', () => {
    wrapper.setData({ quantity: 55 })

    expect(wrapper.find('.error-message').text())
      .toEqual('Quantity shouldn\'t be greater than 50')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should not display error, should enable button and be able to submit when input has a positive value', () => {
    wrapper.setData({ quantity: 30 })
    expect(wrapper.find('.error-message').text()).toEqual('')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeTruthy()
    expect(wrapper.emitted().submit).toHaveLength(1)
    expect(wrapper.emitted().submit[0]).toEqual([{ milkQuantity: 30 }])
    expect(wrapper.find(AppButton).attributes('disabled')).toBeUndefined()
  })

  it('should emit "submit" event', () => {
    wrapper.setData({ quantity: 10 })
    wrapper.find(AppButton).vm.$emit('click')
    expect(wrapper.emitted().submit).toBeTruthy()
    expect(wrapper.emitted().submit).toHaveLength(2)
    expect(wrapper.emitted().submit[1]).toEqual([{ milkQuantity: 10 }])
  })

  it('should emit "changeState" event', () => {
    wrapper.find('a').trigger('click')
    expect(wrapper.emitted().changeStage).toBeTruthy()
    expect(wrapper.emitted().changeStage).toHaveLength(1)
  })
})
