import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import LivestockListingIndex from '@/pages/LivestockListing/index.vue'

let localVue = createLocalVue()
localVue.use(Vuex)

jest.mock('@/store')

describe('Index page for livestock listing', () => {
  const livestockActions = {
    FETCH_LIVESTOCK_TYPES: jest.fn(),
    FETCH_LIVESTOCK_BREEDS: jest.fn(),
    FILTER_LIVESTOCK: jest.fn(),
    getCounties: jest.fn()
  }
  const expertListActions = {
    getCounties: jest.fn()
  }

  const state = {
    livestockBreeds: ['Friesian', 'Crosses'],
    counties: [
      {
        id: 1,
        county: 'Bungoma'
      }, {
        id: 2,
        county: 'Meru'
      }],
    livestockTypes: ['Beef', 'Dairy']
  }
  const store = new Vuex.Store({
    modules: {
      Livestock: {
        state,
        actions: livestockActions
      },
      expertList: {
        state,
        actions: expertListActions
      }
    }
  })

  const $route = {
    query: {
      market: 'joshua',
      breed: 'joshua',
      animal: 'joshua',
      page: 34
    }
  }

  const propsObject = {
    propsData: {
      livestockListing: [
        {
          id: '3kekke',
          title: '',
          animal: 'Dairy',
          breed: 16,
          price: 70875,
          negotiable: 0
        }
      ],
      livestockMetaDetails: {
        current_page: 2,
        from: 11,
        last_page: 21,
        path: 'https://beta.cowsoko.com/api/v1/livestocks',
        per_page: 10,
        to: 20,
        total: 210
      }
    }
  }

  const wrapper = shallowMount(LivestockListingIndex, {
    ...propsObject,
    store,
    localVue,
    mocks: {
      $route
    }
  })

  it('renders the ProfileSideBarLayout component on load', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
