import { shallowMount } from '@vue/test-utils'
import BaseCalving from '@/pages/FirstCalvingPage/BaseCalving'
import AppButton from '@/components/ui/AppButton.vue'
import IntegerInput from '@/components/ui/IntegerInput.vue'

describe('BaseCalving.vue', () => {
  let wrapper
  beforeAll(() => {
    wrapper = shallowMount(BaseCalving, {
      propsData: { ageOfCalving: '' }
    })
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.findAll(IntegerInput)).toHaveLength(1)
    expect(wrapper.findAll(AppButton)).toHaveLength(1)
  })

  it('has recieved "ageOfCalving" property', () => {
    expect(wrapper.vm.ageOfCalving).toEqual('')
  })

  it('should not display error message, disable button and be unable to submit on initial rendering', () => {
    expect(wrapper.find('.error-message').text()).toEqual('')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should display an error, disable button and be unable to submit when input value is empty', () => {
    wrapper.setData({ calvingResult: '' })
    expect(wrapper.find('.error-message').text())
      .toEqual('')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should display an error and disable button when input value is greater than 40', () => {
    wrapper.setData({ calvingResult: 100 })
    expect(wrapper.find('.error-message').text())
      .toEqual('Age must be between 24-40 months')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeFalsy()
    expect(wrapper.find(AppButton).attributes('disabled')).toBe('true')
  })

  it('should not display error, should enable button and be able to submit when input value is correct', () => {
    wrapper.setData({ calvingResult: 25 })
    expect(wrapper.find('.error-message').text()).toEqual('')
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted().submit).toBeTruthy()
    expect(wrapper.emitted().submit).toHaveLength(1)
    expect(wrapper.emitted().submit[0]).toEqual([25])
    expect(wrapper.find(AppButton).attributes('disabled')).toBeUndefined()
  })

  it('should emit "submit" event', () => {
    wrapper.setData({ calvingResult: 34 })
    wrapper.find(AppButton).vm.$emit('click')
    expect(wrapper.emitted().submit).toBeTruthy()
    expect(wrapper.emitted().submit).toHaveLength(2)
    expect(wrapper.emitted().submit[1]).toEqual([34])
  })
})
