export default {
  push: jest.fn(),
  go: jest.fn(),
  currentRoute: {
    fullPath: '/'
  }
}
