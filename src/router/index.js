import LandingPage from '@/pages/LandingPage/LandingPage.vue'
import UserProfileIndex from '@/pages/UserProfileTimeline'

export default [
  {
    path: '/',
    redirect: '/onboarding'
  },
  {
    path: '/onboarding',
    name: 'LandingPage',
    component: LandingPage,
    meta: {
      isAuth: false
    }
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('@/pages/ProfilePage'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/herds-composition',
    name: 'HerdsComposition',
    component: () => import('@/pages/HerdsCompositionPage')
  },
  {
    path: '/average-milk-production',
    name: 'AverageMilkProductionPage',
    component: () => import('@/pages/AverageMilkProductionPage')
  },
  {
    path: '/individual-product/:productId',
    name: 'individual-product',
    component: () => import('@/pages/IndividualProduct')
  },
  {
    path: '/farms',
    name: 'farm',
    component: () => import('@/pages/Farm'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/blogs',
    name: 'blog',
    component: () => import('@/pages/Blog'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/potential-farmer',
    name: 'FarmStats',
    component: () => import('@/pages/FarmStatsPage')
  },
  {
    path: '/service-provider',
    name: 'ServiceProvider',
    component: () => import('@/pages/ServiceProviderOnBoarding')
  },
  {
    path: '/expert-profile/:id',
    name: 'ExpertProfileIndex',
    component: () => import('@/pages/ExpertProfilePage'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/publications/:id',
    name: 'SinglePublication',
    component: () => import('@/pages/Publications/SinglePublicationIndex'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/daily-milk-production',
    name: 'DailyMilkProduction',
    component: () => import('@/pages/DailyMilkPage')
  },
  {
    path: '/first-calving',
    name: 'FirstCalving',
    component: () => import('@/pages/FirstCalvingPage')
  },
  {
    path: '/herd-annual',
    name: 'AnnualHerdGrowth',
    component: () => import('@/pages/AnuualHerdGrowthPage')
  },
  {
    path: '/user-profile',
    name: 'UserProfileIndex',
    component: UserProfileIndex,
    meta: {
      isAuth: true
    }
  },
  {
    path: '/individual-livestock/:livestockId',
    name: 'IndividualLivestock',
    component: () => import('@/pages/IndividualLivestock'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/input-seller',
    name: 'Input Seller',
    component: () => import('@/pages/InputSuppliersOnboarding')
  },
  {
    path: '/experts/county/:county?',
    name: 'ExpertList',
    component: () => import('@/pages/ExpertListPage'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/publications',
    name: 'AllPublications',
    component: () => import('@/pages/Publications/AllPublications'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/featured-publications',
    name: 'FeaturedPublications',
    component: () => import('@/pages/Publications/FeaturedPublications'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/farms/:id',
    name: 'ViewIndividualFarm',
    component: () => import('@/pages/ViewIndividualFarm'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/updates',
    name: 'AllUpdates',
    component: () => import('@/pages/Updates/AllUpdates'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/edit-profile',
    name: 'UserProfile',
    component: () => import('@/pages/UserProfileTimeline/UserProfile')
  },
  {
    path: '/message/:userId',
    name: 'Message',
    component: () => import('@/pages/Message'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/experts/:id/farms',
    name: 'ExpertFarms',
    component: () => import('@/pages/ExpertProfilePage/ExpertFarms')
  },
  {
    path: '/experts/:id/livestocks',
    name: 'ExpertLivestocks',
    component: () => import('@/pages/ExpertProfilePage/ExpertLivestocks')
  },
  {
    path: '/experts/:id/updates',
    name: 'ExpertUpdates',
    component: () => import('@/pages/ExpertProfilePage/ExpertUpdates')
  },
  {
    path: '/livestock-listing',
    name: 'LivestockListingIndex',
    component: () => import('@/pages/LivestockListing')
  },
  {
    path: '/experts/:id/blogs',
    name: 'ExpertBlogs',
    component: () => import('@/pages/ExpertProfilePage/ExpertBlogs')
  },
  {
    path: '/products',
    name: 'Products',
    component: () => import('@/pages/ProductsPage/Products'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/404',
    component: () => import('@/pages/404Page/404Page')
  },
  {
    path: '/tests',
    name: 'ProfileTests',
    component: () => import('@/pages/ProfileTests')
  },
  {
    path: '/input-seller-activity',
    name: 'Input Seller Activity',
    component: () => import('@/pages/InputSellerActivity'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/updates/:id',
    name: 'ExpertUpdate',
    component: () => import('@/pages/ExpertUpdate/SingleUpdate')
  },
  {
    path: '/service-provider-update',
    name: 'ServiceProviderUpdateIndex',
    component: () => import('@/pages/ServiceProviderUpdates')
  },
  {
    path: '/update-card',
    name: 'updateCard',
    component: () => import('@/components/ui/UpdateCard.vue')
  },
  {
    path: '/farms/:id/updates',
    name: 'FarmUpdates',
    component: () => import('@/pages/FarmUpdates/index.vue'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/blogs/:id',
    name: 'BlogCommentsPage',
    component: () => import('@/pages/BlogCommentsPage')
  },
  {
    path: '/subscription/option',
    name: 'subscriptionoption',
    component: () => import('@/pages/Subscriptionoption'),
    meta: {
      isAuth: true,
      isServiceProvider: true
    }
  },
  {
    path: '/payment',
    name: 'payment',
    component: () => import('@/pages/Subscriptionoption/PaymentProcess.vue'),
    meta: {
      isAuth: true,
      isServiceProvider: true
    }
  },
  {
    path: '/subscription',
    name: 'CurrentSubscription',
    component: () => import('@/pages/IndividualCurrentSubscription'),
    meta: {
      isAuth: true,
      isServiceProvider: true
    }
  },
  {
    path: '/receipts',
    name: 'receipt',
    component: () => import('@/pages/Receipts'),
    meta: {
      isAuth: true
    }
  },
  {
    path: '/invoices',
    name: 'InvoiceView',
    component: () => import('@/pages/Invoices')
  },
  {
    path: '*',
    component: () => import('@/pages/404Page/404Page')
  }
]
