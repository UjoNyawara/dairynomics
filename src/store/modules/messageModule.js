/* eslint-disable operator-linebreak */
/* eslint-disable no-trailing-spaces */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable camelcase */

import AxiosCalls from '@/utils/api/AxiosCalls.js'
import PeerModel from '@/utils/peerModel/index.js'
import { alertError } from '../../utils/helperFunctions'

const config = { 'Content-type': 'application/json' }

export default {
  strict: true,
  state: {
    isFetching: {
      page: false,
      messages: false
    },
    isTyping: false,
    chatMate: {}, // active chat mate
    userPeerConnection: {},
    peerModel: new PeerModel(),
    messages: [],
    users: [],
    messagesMeta: {},
    alertError: alertError
  },
  
  mutations: {
    addMessageMutator: (state, payload) => {
      const { isResend, ...message } = payload
      if (isResend) {
        const messages = [...state.messages]
        const { index } = messages.find((msg, i) => {
          if (msg.stamp === message.stamp) {
            msg.index = i
            return msg
          }
        }) || {}
        if (messages[index]) {
          messages[index] = message
          state.messages = messages
        }
      } else state.messages = [...state.messages, message]
    },

    loadMessagesMutator: (state, messages) => {
      messages.reverse()
      state.messages = [...messages, ...state.messages]
    },

    chatMateMutator: (state, user) => {
      state.chatMate = user
      state.messages = []
    },

    updateUsersMutator: (state, users) => (state.users = [...users]),

    signalMutator: (state, option) => {
      switch (option.type) {
        case state.peerModel.IS_TYPING:
          if (option.userId === state.chatMate.id) {
            state[option.type] = option.signal
          }
      }
    },

    unreadMessagesMutator: (state, payload) => {
      const { users, chatMate } = state
      // Find the user who has an unread message(s)
      const { index, id } = users.find((user, i) => {
        if (Number(user.id) === Number(payload.sender_id)) {
          user.index = i
          return user
        }
      }) || {}

      /**
       * If user is found and receiver is not the current active chat mate,
       * update the unread messages count of this user
       */
      if (users[index] && id !== chatMate.id) {
        users[index].unread_messages_count = payload.value
          ? users[index].unread_messages_count + payload.value
          : 0
        state.users = [...users]
      }
    },

    spinnerMutator: (state, { type, status }) => {
      state.isFetching[type] = typeof status !== 'undefined' ? status : true
    }
  },

  actions: {
    updateUsersAction: ({ commit }, users) => commit('updateUsersMutator', users),
    chatMateAction: ({ commit }, user) => commit('chatMateMutator', user),

    postMessageAction: async ({ state: { peerModel, alertError }, commit }, data) => {
      const { senderId, message, receiverId, peerId, stamp, isResend } = data
      let payload
      try {
        const form = new FormData()
        form.append('message', message)
        form.append('receiver_id', receiverId)

        const { data } = await AxiosCalls.axiosPost('chat_messages', form)
        payload = {
          ...data.data, 
          isResend, 
          peerId,
          stamp, 
          created_at: peerModel.formatDate(data.created_at), 
          status: true
        }

        peerModel.transport(payload)
      } catch (error) {
        data = { ...data, status: false, created_at: peerModel.formatDate() }
        payload = { ...data, sender_id: senderId, peerId, stamp }
        alertError('Message was not sent, please try again')
      }

      commit('addMessageMutator', payload)
      return payload
    },

    loadUsersAction: async ({ state: { peerModel, alertError }, commit }) => {
      commit('spinnerMutator', { type: 'page' })
      try {
        const { data } = await AxiosCalls.awaitGet('chat_peers')
        let users = data
        const form = new FormData()

        users.forEach(({ id }, index) => {
          form.append(`peers[${index}]`, id)
        })
        const response = await AxiosCalls.axiosPost('chat_peers/chat_ids', form)
        const peers = {}

        if (response.data.peer_ids.length > 0) {
          response.data.peer_ids.forEach(peer => {
            peers[peer.user_id] = peer.chat_id
          })
        }

        users = users.map(user => {
          user.peerId = peers[user.id]
          user.last_chat_date = peerModel.formatDate(user.last_chat_date)
          user.status = 'Offline'
          user.hasUnsentMessage = false
          return user
        })

        commit('updateUsersMutator', users)
        commit('spinnerMutator', { type: 'page', status: false })
      } catch (error) {
        alertError('Something went wrong, could not load chat mates')
      }
    },

    loadMessagesAction: async ({ state, commit }, option) => {
      const { showSpinner, page } = option
      const { peerModel, alertError } = state

      if (showSpinner) {
        commit('spinnerMutator', { type: 'messages' })
      }
      let messages = []

      try {
        const { data, meta } = await AxiosCalls.awaitGet(`chat_messages?receiver_id=${state.chatMate.id}/&page=${page}`)

        messages = data.map(message => {
          message.status = true
          message.created_at = peerModel.formatDate(message.created_at)
          return message
        })

        commit('spinnerMutator', { type: 'messages', status: false })
        commit('loadMessagesMutator', messages)
        state.messagesMeta = meta
      } catch (error) {
        alertError('Something went wrong, could not load messages')
      }
    },

    markMessagesAsReadAction: async ({ commit, state }, senderId) => {
      try {
        const users = [...state.users]
        const { unread_messages_count, index } = users.find((user, i) => {
          if (user.id === senderId) {
            user.index = i
            return user
          }
        })
        
        if (unread_messages_count > 0) {
          await AxiosCalls.axiosPatch(`chat_messages/mark_read`, {
            sender_id: senderId
          }, config)

          commit('unreadMessagesMutator', { sender_id: senderId })
          users[index].unread_messages_count = 0
          commit('updateUsersMutator', users)
        }
      } catch (error) {}
    },

    openConnectionAction ({ state, commit }, user) {
      state.peerModel.state = state
      state.peerModel.commit = commit
      
      state.peerModel.openConnection(user, async (peerId) => {
        try {
          const form = new FormData()
          form.append('chat_id', peerId) 
          await AxiosCalls.axiosPost('chat_peers/chat_id', form)
        } catch (error) {}
      })
    },

    connectPeer: ({ state: { peerModel } }, peerId) => {
      peerModel.connectPeer(peerId)
    },

    closeConnection: ({ state: { peerModel } }, peerId) => {
      peerModel.closeConnection(peerId)
    }
  },
  
  getters: {
    allMessages: state => state.messages,
    allUsers: state => state.users,
    isFetching: state => state.isFetching,
    chatMate: state => state.chatMate,
    hasNewMessage: state => {
      const user = state.users.find(user => (user.unread_messages_count > 0))
      return user ? true : false
    },
    peerModel: state => state.peerModel,
    isTyping: state => state.isTyping,
    getMessagesMeta: state => state.messagesMeta
  }
}
