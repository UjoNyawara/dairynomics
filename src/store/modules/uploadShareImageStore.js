import axios from 'axios'

export default {
  state: {
    sharedImage: '',
    shareStatus: '',
    errorMessage: '',
    loading: false
  },
  getters: {
    shareGetterData: (state) => { return { ...state } },
    imageGetterData: state => state.sharedImage
  },
  actions: {
    imageUpload ({commit}, image) {
      commit('FACEBOOK_LOADING', true)
      const CLOUDINARY_URL = 'https://api.cloudinary.com/v1_1/dvixzojo2/image/upload'
      const CLOUDINARY_PRESET = 'dcdwqul3'
      let formData = new FormData()
      formData.append('file', image)
      formData.append('upload_preset', CLOUDINARY_PRESET)
      axios({
        url: CLOUDINARY_URL,
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: formData
      })
        .then((resp) => {
          const status = 'success'
          commit('FACEBOOK_LOADING', false)
          commit('UPLOAD_SHARE_IMAGE', resp.data.secure_url, status)
        })
        .catch((err) => {
          commit('HANDLE_ERRORS', err)
          commit('FACEBOOK_LOADING', false)
        })
    }
  },
  mutations: {
    UPLOAD_SHARE_IMAGE (state, imageUrl, status) {
      state.sharedImage = imageUrl
      state.shareStatus = status
    },
    HANDLE_ERRORS (state, errorMessage) {
      state.errorMessage = errorMessage
    },
    FACEBOOK_LOADING (state, loader) {
      state.loading = loader
    }
  }
}
