import axios from 'axios'
import router from '@/router/router'

export default {
  state: {
    user_id: '',
    forgotpin: '',
    verification_code: '',
    loading: false,
    error: ''
  },
  getters: {
    getForgotPin (state) {
      return { ...state }
    }
  },
  mutations: {
    forgotPinMutator (state, msg) {
      if (state.forgotpin === msg) {
        state.forgotpin = null
      }
      state.forgotpin = msg
    },
    loader (state, payload) {
      state.loading = payload
    },
    error (state) {
      state.error = 'Something went wrong, please try again!'
    },
    updateVerificationCode (state, [code, id]) {
      state.verification_code = code
      state.user_id = id
    }
  },
  actions: {
    sendVerification ({ commit }, phoneDetails) {
      commit('loader', true)
      return axios({
        url: 'https://beta.cowsoko.com/api/v1/send_verification_code',
        method: 'post',
        data: phoneDetails
      })
        .then(result => {
          if (result.data.success) {
            commit('forgotPinMutator', 'accountCode')
            commit('updateVerificationCode', [result.data.verification_code, result.data.user.id])
            commit('loader', false)
          } else {
            commit('error')
            commit('loader', false)
          }
        })
        .catch(() => {
          commit('error')
        })
    },
    loadSendConfirmationForm ({ commit }) {
      commit('forgotPinMutator', 'lostpin')
    },
    loadChangePinForm ({ commit }) {
      commit('forgotPinMutator', 'accountCode')
    },
    accountCode ({ state, commit }, pincode) {
      commit('loader', true)
      return axios.put(`https://beta.cowsoko.com/api/v1/change_password/${state.user_id}`,
        {new_password: pincode}
      )
        .then(response => {
          commit('loader', false)
          if (response.data.success) {
            return router.push('/profile')
          }
          commit('error')
        })
        .catch(() => {
          commit('error')
        })
    }
  }
}
