import AxiosCalls from '@/utils/api/AxiosCalls'

export default {
  state: {
    publicationId: '',
    publication: [],
    error: '',
    download: '',
    loading: false,
    isLoading: true
  },
  getters: {
    publicationGetter: state => state.publication,
    downloadGetter: state => state.download,
    downloadLoader: state => state.loading,
    downloadError: state => state.error,
    isLoading: state => state.isLoading
  },
  mutations: {
    VIEW_SINGLE_PUBLICATION (state, publication) {
      state.publication = publication
    },
    PUBLICATION_ERROR (state, error) {
      state.error = error
    },
    DOWNLOAD_PUBLICATION (state, download) {
      state.download = download
    },
    LOADING_ICON (state, loading) {
      state.loading = loading
    },
    IS_PAGE_LOADING (state, isLoading) {
      state.isLoading = isLoading
    }
  },
  actions: {
    async singlePublication ({ commit }, publicationId) {
      commit('PUBLICATION_ERROR', '')
      commit('IS_PAGE_LOADING', true)
      try {
        let { data } = await AxiosCalls.get(
          `api/v1/publications/${publicationId}`
        )

        commit('IS_PAGE_LOADING', false)
        commit('VIEW_SINGLE_PUBLICATION', data)
      } catch (error) {
        commit('IS_PAGE_LOADING', false)
        commit('PUBLICATION_ERROR', error)
      }
    },
    async downloadPublication ({ commit }, publication) {
      commit('LOADING_ICON', true)
      commit('CHANGE_REQUEST_STATUS', { isDownloading: true })
      commit('PUBLICATION_ERROR', '')

      try {
        const response = await AxiosCalls.get(
          `api/v1/publications/${publication.id}/download`,
          { responseType: 'blob' }
        )
        if (response.data.type !== 'application/json') {
          commit('CHANGE_REQUEST_STATUS', { isDownloading: false })
          const url = window.URL.createObjectURL(new Blob([response.data]))
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', `${publication.title}.pdf`)
          document.body.appendChild(link)
          link.click()
          commit('DOWNLOAD_PUBLICATION', response.data)
          commit('LOADING_ICON', false)
        } else {
          commit('PUBLICATION_ERROR', 'Failed to download')
          commit('CHANGE_REQUEST_STATUS', { isDownloading: false })
          commit('LOADING_ICON', false)
        }
      } catch (error) {
        commit('PUBLICATION_ERROR', error.response.data)
        commit('LOADING_ICON', false)
      }
    }
  }
}
