import AxiosCalls from '@/utils/api/AxiosCalls'
export default {
  state: {
    topics: []
  },
  getters: {
    TOPICS: state => {
      return state.topics
    }
  },
  mutations: {
    SET_TOPICS: (state, payload) => {
      state.topics = payload.topics
    }
  },
  actions: {
    GET_TOPICS: async (context, payload) => {
      try {
        let {data} = await AxiosCalls.get('api/v1/topics', payload.token)
        context.commit('SET_TOPICS', data)
      } catch (error) {
        throw error
      }
    }
  }

}
