import AxiosCalls from '@/utils/api/AxiosCalls'

export default {
  state: {
    products: [],
    counties: [],
    paginationInfo: {
      currentPage: '',
      lastPage: ''
    },
    isLoading: false
  },
  mutations: {
    STORE_COUNTIES: (state, counties) => {
      state.counties = counties
    },
    STORE_PRODUCTS: (state, products) => {
      state.products = products
      state.isLoading = false
    },
    STORE_PAGINATION_INFO: (state, pageMeta) => {
      state.paginationInfo.currentPage = pageMeta.current_page
      state.paginationInfo.lastPage = pageMeta.last_page
    },
    LOADING_PRODUCTS: (state, payload) => {
      state.isLoading = payload
    }
  },
  actions: {
    fetchCounties: async ({ commit }) => {
      try {
        commit('LOADING_PRODUCTS', true)
        const { data: request } = await AxiosCalls.get(`api/v1/counties`)
        const { counties } = request
        commit('STORE_COUNTIES', counties)
      } catch (error) {
        throw error
      }
    },

    fetchProducts: async ({ commit }, { pageNumber, county, selectedType }) => {
      commit('LOADING_PRODUCTS', true)
      const type = selectedType || 'all'
      let value = type !== 'all' ? `filter[product_type]=${type}` : ``

      const request = await AxiosCalls.get(`api/v1/products_filter?page[number]=${pageNumber}&${value}&filter[county]=${county.replace('County', '').trim()} County`)
      const { products } = request.data.data || {}
      const meta = request.data.meta || { current_page: '', last_page: '' }

      commit('STORE_PRODUCTS', products)
      commit('STORE_PAGINATION_INFO', meta)
    }
  },
  getters: {
    showPrevious: state => state.paginationInfo.currentPage > 1,
    showNext: state => state.paginationInfo.currentPage < state.paginationInfo.lastPage
  }
}
