import AxiosCalls from '@/utils/api/AxiosCalls.js'

let token = localStorage.getItem('token')

export default {
  state: {
    UserUpdate: null,
    updateComments: [],
    updateCommentsMeta: {
      current_page: 1,
      from: 1,
      last_page: 1,
      path: '',
      per_page: 10,
      to: 1,
      total: 1
    },
    updateCommentsLoading: false,
    commentId: null,
    updateId: null,
    userId: null,
    replyId: null,
    commentEditError: null,
    replyEditError: null,
    commentDeleteError: null,
    creatorId: null,
    updateCreatorDetails: null,
    creatorError: null,
    updateFetchError: null,
    commentLikeError: null,
    commentingError: null,
    updateLikingError: null,
    commentReplyError: null,
    commentUnlikeError: null,
    updateUnlikeError: null,
    updateReplyUnlikeError: null
  },
  getters: {
    getUpdate (state) {
      return { ...state.UserUpdate }
    },
    getComments (state) {
      return [...state.updateComments]
    },
    getCommentsMeta (state) {
      return state.updateCommentsMeta
    },
    getCommentsLoading (state) {
      return state.updateCommentsLoading
    },
    getSelectedComment (state) {
      return state.commentId
    },
    getSelectedReply (state) {
      return state.replyId
    },
    getUpdateCreator (state) {
      return { ...state.updateCreatorDetails }
    }
  },
  mutations: {
    POPULATE_UPDATE_DATA (state, payload) {
      state.UserUpdate = payload
    },
    POPULATE_UPDATE_ERROR (state, payload) {
      state.updateFetchError = payload
    },
    populateComments (state, payload) {
      if (!payload.meta || payload.meta.current_page === 1) {
        state.updateComments = payload.comments
      } else {
        state.updateComments = [...payload.comments, ...state.updateComments]
      }
      state.updateCommentsMeta = {
        ...state.updateCommentsMeta,
        ...payload.meta
      }
    },
    loadingComments (state, payload) {
      state.updateCommentsLoading = payload
    },
    SET_COMMENTING_ERROR (state, payload) {
      state.commentingError = payload
    },
    SET_UPDATE_LIKING_ERROR (state, payload) {
      state.updateLikingError = payload
    },
    changeCommentEditError (state, payload) {
      state.commentEditError = payload
    },
    changeReplyEditError (state, payload) {
      state.replyEditError = payload
    },
    updateCommentId (state, payload) {
      state.commentId = payload
    },
    setCurrentUpdateId (state, payload) {
      state.updateId = payload
    },
    changeUserId (state, payload) {
      state.userId = payload
    },
    changeReplyId (state, payload) {
      state.replyId = payload
    },
    changeCommentDeleteError (state, payload) {
      state.commentDeleteError = payload
    },
    changeUpdateCreatorId (state, payload) {
      state.creatorId = payload
    },
    POPULATE_CREATOR_DETAILS (state, payload) {
      state.updateCreatorDetails = payload
    },
    POPULATE_CREATOR_ERROR (state, payload) {
      state.creatorError = payload
    },
    COMMENT_LIKE_ERROR (state, payload) {
      state.commentLikeError = payload
    },
    SET_COMMENT_REPLY_ERROR (state, payload) {
      state.commentReplyError = payload
    },
    SET_COMMENT_UNLIKE_ERROR (state, payload) {
      state.commentUnlikeError = payload
    },
    SET_UPDATE_UNLIKE_ERROR (state, payload) {
      state.updateUnlikeError = payload
    },
    SET_UPDATE_REPLY_UNLIKE_ERROR (state, payload) {
      state.updateReplyUnlikeError = payload
    }
  },
  actions: {
    async fetchUpdate ({ commit, dispatch, state }, id) {
      commit('setCurrentUpdateId', id)
      let response = await AxiosCalls.get(`api/v1/updates/${id}`, token)

      if (response.data.success) {
        commit('POPULATE_UPDATE_DATA', response.data.update)
      } else {
        commit('POPULATE_UPDATE_ERROR', response)
      }
    },
    async fetchComments ({ commit, state }, page = 1) {
      commit('loadingComments', true)
      let response = await AxiosCalls.get(
        `api/v1/updates/${state.updateId}/comments?page=${page}`,
        token
      )
      if (response.data.success) {
        const { data } = response
        commit('populateComments', {
          comments: data.data.comments.reverse(),
          meta: data.meta
        })
        commit('loadingComments', false)
      } else {
        commit('populateComments', {
          comments: [],
          meta: {
            current_page: 1,
            from: 1,
            last_page: 1,
            path: '',
            per_page: 10,
            to: 1,
            total: 1
          }
        })
        commit('loadingComments', false)
      }
    },
    async commentOnUpdate ({ commit, dispatch, state }, data) {
      let response = await AxiosCalls.postTest(
        `api/v1/updates/${state.updateId}/comments`,
        data
      )
      if (response.data.success) {
        dispatch('fetchComments')
        dispatch('fetchUpdate', state.updateId)
      } else {
        commit('SET_COMMENTING_ERROR', response.data.error)
      }
    },
    async editUpdateComment ({ commit, dispatch, state }, data) {
      let response = await AxiosCalls.put(
        `api/v1/comments/${state.commentId}`,
        data
      )
      if (response.data.success) {
        dispatch('fetchComments')
      } else {
        commit('changeCommentEditError')
      }
    },
    async likeAnUpdate ({ commit, dispatch, state }, data) {
      let response = await AxiosCalls.put(
        `api/v1/updates/${state.updateId}/like`,
        data
      )
      if (response.data.success) {
        dispatch('fetchUpdate', state.updateId)
      } else {
        commit('SET_UPDATE_LIKING_ERROR', response.data.message)
      }
    },
    async unlikeAnUpdate ({ dispatch, commit, state }, data) {
      let response = await AxiosCalls.put(
        `api/v1/updates/${state.updateId}/unlike`,
        data
      )
      if (response.data.success) {
        dispatch('fetchUpdate', state.updateId)
      } else {
        commit('SET_UPDATE_UNLIKE_ERROR', response.data.message)
      }
    },
    async likeUpdateComment ({ commit, dispatch, state }, userId) {
      let response = await AxiosCalls.put(
        `api/v1/comments/${state.commentId}/like`,
        userId
      )
      if (response.data.success) {
        dispatch('fetchComments')
      } else {
        commit('COMMENT_LIKE_ERROR')
      }
    },
    async unlikeUpdateComment ({ commit, dispatch, state }, userId) {
      let response = await AxiosCalls.put(
        `api/v1/comments/${state.commentId}/unlike`,
        userId
      )
      if (response.data.success) {
        dispatch('fetchComments')
      } else {
        commit('SET_COMMENT_UNLIKE_ERROR', response.data.message)
      }
    },
    async likeUpdateReply ({ commit, dispatch, state }, userId) {
      let response = await AxiosCalls.put(
        `api/v1/comments/${state.replyId}/like`,
        userId
      )
      if (response.data.success) {
        dispatch('fetchComments')
      }
    },
    async unlikeUpdateReply ({ commit, dispatch, state }, userId) {
      let response = await AxiosCalls.put(
        `api/v1/comments/${state.replyId}/unlike`,
        userId
      )
      if (response.data.success) {
        dispatch('fetchComments')
      } else {
        commit('SET_UPDATE_REPLY_UNLIKE_ERROR', response.data.message)
      }
    },
    setCommentId ({ commit, state }, payload) {
      commit('updateCommentId', payload)
    },
    setUserId ({ commit }, userId) {
      commit('changeUserId', userId)
    },
    setSelectedUpdate ({ commit, state }, id) {
      commit('setCurrentUpdateId', id)
    },
    setReplyId ({ commit, state }, id) {
      commit('changeReplyId', id)
    },
    setUpdateCreatorId ({ commit }, creatorId) {
      commit('changeUpdateCreatorId', creatorId)
    },
    async replytoComment ({ commit, dispatch, state }, data) {
      let response = await AxiosCalls.post(
        `api/v1/comments/${state.commentId}/reply`,
        data
      )
      if (response.data.success) {
        dispatch('fetchComments')
      } else {
        commit('SET_COMMENT_REPLY_ERROR', response.data.message)
      }
    },
    async editReplyToComment ({ dispatch, commit, state }, data) {
      let response = await AxiosCalls.put(
        `api/v1/comments/${state.replyId}`,
        data
      )
      if (response.data.success) {
        dispatch('fetchComments')
      } else {
        commit('changeReplyEditError', response)
      }
    },
    async deleteAComment ({ commit, dispatch, state }, data) {
      let response = await AxiosCalls.delete(
        `api/v1/comments/${state.commentId}`,
        data
      )
      if (response.data.success) {
        if (state.updateComments.length > 0) {
          let newComments = state.updateComments.filter(
            comment => comment.id !== state.commentId
          )
          commit('populateComments', { comments: newComments })
        }
        dispatch('fetchUpdate', state.updateId)
      } else {
        commit('changeCommentDeleteError', response.data.message)
      }
    },
    async deleteReply ({ commit, dispatch, state }, data) {
      let response = await AxiosCalls.delete(
        `api/v1/comments/${state.replyId}`,
        data
      )
      if (response.data.success) {
        dispatch('fetchComments')
      } else {
        commit('changeCommentDeleteError', response)
      }
    },
    async fetchUpdateCreatorProfile ({commit, state}) {
      let response = await AxiosCalls.get(`api/v1/experts/${state.creatorId}`)
      if (response.data.success) {
        commit('POPULATE_CREATOR_DETAILS', response.data.data)
      } else {
        commit('POPULATE_CREATOR_ERROR', response)
      }
    }
  }
}
