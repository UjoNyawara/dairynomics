import AxiosCalls from '@/utils/api/AxiosCalls'

export default {
  state: {
    loading: false,
    error: '',
    currentComponent: 'ProductSection',
    selectedInputs: [],
    Inputs: []
  },
  getters: {
    uploadInputDetails (state) {
      return state
    }
  },
  mutations: {
    SAVE_INPUTS (state, payload) {
      state.selectedInputs = payload
    },
    SUBMIT_INPUTS_LOADING (state) {
      state.loading = true
    },
    SUBMIT_INPUTS_SUCCESS (state, payload) {
      state.Inputs = payload
      state.loading = false
    },
    SUBMIT_INPUTS_ERROR (state, payload) {
      state.error = payload
      state.loading = false
    },
    CHANGE_COMPONENT (state, payload) {
      state.currentComponent = payload
    }
  },
  actions: {
    submitInputs: async ({ commit }, payload) => {
      const { data: formData, selectedValues } = payload
      commit('SAVE_INPUTS', selectedValues)
      commit('SUBMIT_INPUTS_LOADING')
      try {
        let { data } = await AxiosCalls.axiosPost('inputs', formData)
        commit('SUBMIT_INPUTS_SUCCESS', data)
        commit('CHANGE_COMPONENT', 'UploadProduct')
      } catch (error) {
        commit('SUBMIT_INPUTS_ERROR', 'The inputs could not be submitted')
      }
    }
  }
}
