export default {
  id: 1,
  name: 'Joshua',
  price: 23,
  description: 'Net Ninja',
  photo: 'https://joshua.jpg',
  product_type: 23,
  quantity: 34,
  availability: true,
  created_at: null
}
