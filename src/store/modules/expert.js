import AxiosCalls from '@/utils/api/AxiosCalls'

export default {
  state: {
    expertUpdates: [],
    expertDetails: [],
    numberOfExpertUpdates: undefined,
    isLoading: false,
    isPosting: false,
    page: {},
    isFetching: false,
    updateError: undefined
  },
  mutations: {
    CREATE_EXPERT_UPDATE: (state, payload) => {
      state.expertUpdates.unshift(payload)
    },
    SET_NUMBER_OF_EXPERT_UPDATES: (state, payload) => {
      state.numberOfExpertUpdates = payload
    },
    TOGGLE_LOADING: (state, payload) => {
      state.isLoading = payload
    },
    TOGGLE_POSTING: (state, payload) => {
      state.isPosting = payload
    },
    SET_PAGE: (state, payload) => {
      state.page = payload
    },
    SET_EXPERT_UPDATES: (state, payload) => {
      state.expertUpdates = payload.data.updates
    },
    UPDATE_EXPERT_UPDATES: (state, payload) => {
      if (payload.meta.current_page !== 1 && !payload.success) {
        state.expertUpdates = [...state.expertUpdates]
      }
      state.expertUpdates = [...state.expertUpdates, ...payload.data.updates]
    },
    UPDATE_REQUEST_STATUS: (state, payload) => {
      state.isFetching = payload
    },
    UPDATE_ERROR: (state, payload) => {
      state.updateError = payload
    }
  },
  actions: {
    POST_EXPERT_UPDATE: async (context, payload) => {
      context.commit('TOGGLE_POSTING', true)
      try {
        let { data } = await AxiosCalls.post(
          'api/v1/updates',
          payload.data,
          payload.token
        )
        context.commit('CREATE_EXPERT_UPDATE', data.data)
        context.commit('TOGGLE_POSTING', false)
      } catch (error) {
        context.commit('TOGGLE_POSTING', false)
        if (error.code === 'ECONNABORTED') {
          context.commit(
            'UPDATE_ERROR',
            'Sorry an error occured, please try again'
          )
        }
        context.commit(
          'UPDATE_ERROR',
          'Sorry an error occured, please try again'
        )
      }
    },
    GET_EXPERT_UPDATES: async (context, params) => {
      let token = localStorage.getItem('token')
      try {
        context.commit('TOGGLE_LOADING', true)
        const response = await AxiosCalls.get(
          `api/v1/experts/${params.expertId}/updates?page=${params.page}`,
          token
        )
        if (response.data !== undefined) {
          let { data } = response
          let page = {
            currentPage: data.meta.current_page,
            lastPage: data.meta.last_page
          }
          context.commit('SET_PAGE', page)
          context.commit('SET_EXPERT_UPDATES', data)
          context.commit('SET_NUMBER_OF_EXPERT_UPDATES', data.number_of_updates)
          context.commit('TOGGLE_LOADING', false)
        } else {
          context.commit('SET_EXPERT_UPDATES', response)
          context.commit('TOGGLE_LOADING', false)
        }
      } catch (error) {
        context.commit('TOGGLE_LOADING', false)
        return error
      }
    },
    UPDATE_EXPERT_UPDATES: async (context, params) => {
      try {
        context.commit('UPDATE_REQUEST_STATUS', true)
        let response = await AxiosCalls.get(
          `api/v1/experts/${params.expertId}/updates?page=${params.page}`
        )
        if (response.data !== undefined) {
          let { data } = response
          let page = {
            currentPage: data.meta.current_page,
            lastPage: data.meta.last_page
          }

          context.commit('SET_PAGE', page)
          context.commit('UPDATE_EXPERT_UPDATES', data)
          context.commit('UPDATE_REQUEST_STATUS', false)
        } else {
          context.commit('SET_EXPERT_UPDATES', response)
          context.commit('TOGGLE_LOADING', false)
        }
      } catch (error) {
        context.commit('TOGGLE_LOADING', false)
        return error
      }
    }
  },
  getters: {
    getExpertUpdates: state => state.expertUpdates,
    getExpertUpdatesPage: state => state.page,
    getIsFetching: state => state.isFetching,
    getNumberOfExpertUpdates: state => state.numberOfExpertUpdates,
    getUpdateError: state => state.updateError,
    EXPERT_UPDATES: state => state.expertUpdates
  }
}
