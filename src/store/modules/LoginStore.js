import axios from 'axios'
import { closeModal } from '@/utils/helperFunctions'
import router from '@/router/router'
export default {
  state: {
    loggingIn: false,
    loginError: null,
    loginSuccessful: false,
    authenticationError: '',
    userId: null,
    isLoggedIn: false
  },
  mutations: {
    loginStart: state => {
      state.loggingIn = true
      return state.loggingIn
    },
    loginStop: (state, errorMessage) => {
      state.loggingIn = false
      state.loginError = errorMessage
      state.loginSuccessful = !errorMessage
    },
    toggleIsLoggedInState: (state, payload) => {
      state.isLoggedIn = payload
      return state.isLoggedIn
    },

    setAuthError: (state, message) => {
      state.authenticationError = message
      return state.authenticationError
    }
  },
  actions: {
    doLogin ({ commit }, loginData) {
      commit('loginStart')
      const { openConnection, ...formData } = loginData
      return axios({
        url: 'https://beta.cowsoko.com/api/v1/login',
        method: 'post',
        data: formData
      })
        .then(response => {
          if (response.data.success) {
            commit('loginStop', null)
            const token = response.data.token
            localStorage.setItem('token', token)
            localStorage.setItem('userId', response.data.user.id)
            commit('toggleIsLoggedInState', true)
            commit('setAuthError', '')
            router.go()
            closeModal()
          } else {
            commit('loginStop', 'Login unsuccessful , Please try again')
          }
        })
        .catch(error => {
          if (
            error.response.data['message'] === 'Wrong phone number or password'
          ) {
            commit('loginStop', 'Wrong code, phone number or password ')
          } else if (
            error.response.data['message'] === 'Account Not Verified'
          ) {
            commit('loginStop', 'Please verify your account')
          } else if (error.response.data['error'] === 'phone number invalid') {
            commit('loginStop', 'Please input valid country code')
          } else {
            commit('loginStop', 'Unable to login')
          }
        })
    }
  },
  getters: {
    LoginDetails (state) {
      return state
    },
    isLoggedIn (state) {
      return { status: state.isLoggedIn }
    },
    authError (state) {
      return { error: state.authenticationError }
    }
  }
}
