import AxiosCalls from '@/utils/api/AxiosCalls'
import getRandomItem from '@/utils/performance/getRandomItem'
import router from '@/router/router'

export default {
  state: {
    updates: [],
    randomUpdate: {}
  },
  getters: {
    UPDATES (state) {
      return state.updates
    },
    RANDOMUPDATE (state) {
      if (state.randomUpdate && state.randomUpdate.number_of_comments) {
        return state.randomUpdate
      }
    }
  },
  mutations: {
    SET_UPDATES (state, payload) {
      state.updates = payload
    },
    SET_RANDOM_UPDATE (state, payload) {
      state.randomUpdate = payload
    }
  },
  actions: {
    GET_UPDATES: ({commit}) => {
      return AxiosCalls.get('api/v1/user_updates')
        .then((response) => {
          const randomUpdate = getRandomItem(response.data.updates)
          commit('SET_UPDATES', response.data.updates)
          commit('SET_RANDOM_UPDATE', randomUpdate)
        })
        .catch((error) => {
          switch (error.response.status) {
            case 404:
              return router.push('/404')
            default:
              return router.push('/')
          }
        })
    }
  }
}
