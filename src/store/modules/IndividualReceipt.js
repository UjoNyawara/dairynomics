import * as FileSaver from 'file-saver'
import axios from 'axios'
import router from '@/router/router'
import { ecommerceBaseUrl } from '@/utils/api/urls'

const url = `${ecommerceBaseUrl}/api/receipts`
const token = localStorage.getItem('token')

export default {
  state: {
    receipt: [],
    orderItems: [],
    billing: [],
    isLoading: false,
    isDownloading: false
  },
  mutations: {
    SET_RECEIPT (state, receipt) {
      state.receipt = receipt
    },
    SET_ORDER_ITEMS (state, orderItems) {
      state.orderItems = orderItems
    },
    SET_BILLING (state, billing) {
      state.billing = billing
    },
    RESET_RECEIPT (state) {
      state.receipt = {
        'invoiceNumber': '------',
        'amountToBePaid': 0
      }
    },
    RESET_ORDER_ITEMS (state) {
      state.orderItems = []
    },
    RESET_BILLING (state) {
      state.billing = []
    },
    SET_SPINNER (state, payload) {
      state.isLoading = payload
    },
    SET_IS_DOWNLOADING (state, payload) {
      state.isDownloading = payload
    }
  },
  actions: {
    getReceipt ({commit}, {receiptId}) {
      commit('SET_SPINNER', true)
      return axios.get(`${url}/${receiptId}`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
        .then((response) => {
          commit('SET_RECEIPT', response.data.receipt)
          commit('SET_ORDER_ITEMS', response.data.receipt.order.orderItems)
          commit('SET_BILLING', response.data.receipt.order.billing)
          commit('SET_SPINNER', false)
        })
        .catch((error) => {
          if (error.response.status === 404) {
            return router.push('/404')
          }
        })
    },
    downloadReceipt ({commit}, receiptId) {
      commit('SET_IS_DOWNLOADING', true)
      return axios.get(`${url}/download/${receiptId}`, {
        headers: {
          Authorization: `Bearer ${token}`
        },
        responseType: 'blob'
      }).then(response => {
        const blob = new Blob([response.data])
        FileSaver.saveAs(blob, `${Date.now()}-dairynomics-receipt.pdf`)
        commit('SET_IS_DOWNLOADING', false)
      })
    },
    resetReceipt ({commit}) {
      commit('RESET_RECEIPT')
    },
    resetOrderItems ({commit}) {
      commit('RESET_ORDER_ITEMS')
    },
    resetBilling ({commit}) {
      commit('RESET_BILLING')
    }
  }
}
