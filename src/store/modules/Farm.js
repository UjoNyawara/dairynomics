import axios from 'axios'

export default {
  state: {
    farms: []
  },
  mutations: {
    setfarms (state, farms) {
      state.farms = farms
    }
  },
  getters: {
    getState (state) {
      return {...state}
    }
  },
  actions: {
    getFarms ({commit}) {
    // const userId = localStorage.getItem('userId')
      axios({
        url: 'https://beta.cowsoko.com/api/v1/farms',
        method: 'get',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(results => {
          commit('setfarms', results.data.data.farms)
        })
    }
  }
}
