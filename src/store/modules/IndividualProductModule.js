import AxiosCalls from '@/utils/api/AxiosCalls'
// import { getToken, alertTokenError } from '@/utils/helperFunctions'
import {
  alertError,
  getAPIErrorMessage
} from '@/utils/helperFunctions'

import router from '@/router/router'

export default {
  state: {
    IndividualProductDetails: {}
  },
  getters: {
    getIndividualProduct (state) {
      return {...state.IndividualProductDetails}
    }
  },
  mutations: {
    INDIVIDUAL_PRODUCT_DETAILS (state, data) {
      const payload = {
        id: data.id,
        name: data.name,
        price: data.price,
        description: data.description,
        photos: data.photo,
        product_type: data.product_type,
        quantity: data.quantity,
        availability: data.availability,
        created_at: data.created_at
      }
      state.IndividualProductDetails = {...payload}
    }
  },
  actions: {
    getIndividualProduct: async (context, id) => {
      const token = localStorage.getItem('token')
      try {
        let data = await AxiosCalls.awaitGet(`products/${id}`, token)
        if (data.error === 404) {
          router.push('/404')
        }
        if (data.error) return alertError(getAPIErrorMessage(data.error))
        context.commit('INDIVIDUAL_PRODUCT_DETAILS', data)
      } catch (error) {
        throw error
      }
    }
  }
}
