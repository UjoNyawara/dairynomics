import AxiosCalls from '@/utils/api/AxiosCalls'
import getRandomItem from '@/utils/performance/getRandomItem'
import herdsImage from '@/assets/herd.svg'
import milkProductionImg from '@/assets/cow2.svg'
import milkSoldImg from '@/assets/milk-carton.svg'
import moneyImage from '@/assets/money-bag.png'
import calfImage from '@/assets/calf.svg'
import router from 'vue-router'

export const userTests = [
  {
    title: 'Herd composition',
    testComponent: 'HerdsComposition',
    image: calfImage,
    imageName: 'calf.svg'
  },
  {
    title: 'Learn how much money you can make from farming',
    testComponent: 'FarmStats',
    image: moneyImage,
    imageName: 'money-bag.png'
  },
  {
    title: 'How much milk does your cow produce per day?',
    testComponent: 'DailyMilkProduction',
    image: milkProductionImg,
    imageName: 'cow2.svg'
  },
  {
    title: 'Milk produced vs sold',
    testComponent: 'AverageMilkProductionPage',
    image: milkSoldImg,
    imageName: 'milk-carton.svg'
  },
  {
    title: 'Here\'s how your total herd will grow yearly',
    testComponent: 'AnnualHerdGrowth',
    image: herdsImage,
    imageName: 'herd.svg'
  },
  {
    title: 'What is the age of first calving?',
    testComponent: 'FirstCalving',
    image: calfImage,
    imageName: 'calf.svg'
  }
]

export default {
  state: {
    randomTest: {},
    userTests,
    userTestResults: [],
    test: {}
  },
  getters: {
    RANDOM_USER_TEST (state) {
      return state.randomTest
    },
    SORTED_TEST_RESULTS (state) {
      const { userTests, userTestResults } = state
      const results = {}
      const formatDate = date => new Date(date).getTime()
      userTests.map(test => {
        results[test.title] = userTestResults.filter(result => result.test_title.includes(test.title)).sort((a, b) => formatDate(b.created_at.date) - formatDate(a.created_at.date))[0]
      })
      return results
    },
    ALL_USER_TESTS (state) {
      return state.userTests
    },
    ALL_USER_RESULTS (state) {
      return state.userTestResults
    }
  },
  mutations: {
    SET_RANDOM_USER_TEST (state) {
      const randomTest = getRandomItem(state.userTests)
      state.randomTest = randomTest
    },
    SET_USER_TEST_RESULTS (state, payload) {
      state.userTestResults = payload
    },
    SET_USER_TEST (state, payload) {
      state.test = payload
    }
  },
  actions: {
    GET_USER_TESTS: ({commit}) => {
      return AxiosCalls.get('api/v1/onboarding_test')
        .then((response) => {
          const randomTest = getRandomItem(response.data)
          commit('SET_RANDOM_USER_TEST', randomTest)
        })
        .catch((error) => {
          switch (error.response.status) {
            case 404:
              return router.push('/404')
            default:
              return router.push('/')
          }
        })
    },
    GET_USER_TEST_RESULTS: ({commit}, userId) => {
      return AxiosCalls.get(`api/v1/user_test/${userId}`)
        .then((response) => {
          commit('SET_USER_TEST_RESULTS', response.data.data.onboarding_results)
        })
        .catch((error) => {
          switch (error.response.status) {
            case 404:
              return router.push('/404')
            default:
              return router.push('/')
          }
        })
    },
    POST_TEST_RESULTS: ({commit}, data) => {
      return AxiosCalls.postTest('api/v1/onboarding_test', data)
        .then((response) => {
          commit('SET_USER_TEST', response.data)
        })
        .catch((error) => {
          switch (error.response) {
            case 404:
              return router.push('/404')
            default:
              return router.push('/')
          }
        })
    }
  }
}
