import axios from 'axios'

export default {
  state: {
    blog: {},
    blogId: '',
    comments: [],
    comment_reply_id: '',
    comment_id: '',
    error: '',
    active_like: true,
    userIds: {},
    likeBlogUserIds: [],
    blogIds: [],
    blogsPerPage: 0,
    buttonIsDisabled: false,
    loadingState: true
  },
  getters: {
    getBlogInfo (state) {
      return {...state}
    }
  },
  mutations: {
    setBlogIds (state, blogsIds) {
      state.blogIds = blogsIds
    },
    updateCommentId (state, replyId) {
      state.comment_id = replyId
    },
    setUserIds (state, payload) {
      state.userIds[String(payload.replyId)] = payload.userIds
    },
    setLikeBlogUserIds (state, payload) {
      state.likeBlogUserIds = payload
    },
    setBlogsPerPage (state, payload) {
      state.blogsPerPage = payload
    },
    deleteReply (state, delReply) {
      state.comments.forEach(comment => {
        if (state.comment_id === comment.id) {
          comment.replies = comment.replies.filter(usr => usr.id !== delReply)
        }
      })
    },
    deleteComment (state, commentId) {
      state.comments = state.comments.filter(com => com.id !== commentId)
    },
    editComment (state, newReply) {
      state.comments.forEach(comment => {
        if (comment.replies.length) {
          return comment.replies.forEach(reply => {
            if (reply.id === state.comment_id) {
              reply.reply = newReply
            }
          })
        }
        if (state.comment_id === comment.id) {
          comment.comment = newReply
        }
      })
    },
    addReplies (state, reply) {
      state.comments.forEach(comment => {
        if (state.comment_id === comment.id) {
          comment.replies.push(reply)
        }
      })
    },
    likeSingleComment (state, payload) {
      const userId = localStorage.getItem('userId')
      const comment = state.comments.filter(comment => {
        return comment.id === payload.id
      })[0]
      comment.likes += 1
      comment.user_likes.push(userId)
    },
    unlikeSingleComment (state, payload) {
      const userId = localStorage.getItem('userId')
      const comment = state.comments.filter(comment => {
        return comment.id === payload.id
      })[0]
      const likesIndex = comment.user_likes.findIndex((id) => userId === id)
      comment.likes -= 1
      comment.user_likes.splice(likesIndex, 1)
    },
    updateReplyLikes (state, payload) {
      if (state.userIds[payload.replyId]) {
        state.userIds[payload.replyId].push(payload.userId)
      } else {
        state.userIds = {...state.userIds, [payload.replyId]: [payload.userId]}
      }

      // Map through each comment, then filter through the replies to get the likes
      const findCommentWithId = state.comments.map(
        comment => comment.replies.filter(reply => reply.id === payload.replyId)
      ).filter(replies => replies.length > 0)

      const replies = findCommentWithId[0][0]
      replies.likes += 1
      replies.user_likes = [payload.userId]
    },
    updateReplyUnLikes (state, payload) {
      // Map through each comment, then filter through the replies to get the likes
      const findCommentWithId = state.comments.map(
        comment => comment.replies.filter(reply => reply.id === payload.replyId)
      ).filter(replies => replies.length > 0)

      const replies = findCommentWithId[0][0]
      replies.likes -= 1
      const likesIndex = replies.user_likes.findIndex((id) => payload.userId === id)
      replies.user_likes.splice(likesIndex, 1)
    },
    updateBlogLikes (state, userId) {
      state.likeBlogUserIds.push(userId)
      ++state.blog.likes
    },
    removeUserIdFromBlogLikes (state, userId) {
      let filtered = state.likeBlogUserIds.filter(usr => usr !== userId)
      state.likeBlogUserIds = filtered
      --state.blog.likes
    },
    setBlog (state, blog) {
      state.blog = blog
    },
    updateComments (state, comment) {
      state.comments.push(comment)
    },
    updateCommentReplyId (state, commentReplyId) {
      state.comment_reply_id = commentReplyId
    },
    setComments (state, comments) {
      state.comments = comments
      state.loadingState = false
    },
    setError (state) {
      state.error = 'Sorry something went wrong, please try again!'
    },
    setBlogId (state, id) {
      state.blogId = id
    }
  },
  actions: {
    async getSingleBlog ({commit, state, dispatch}, {blogId}) {
      const singleBlogRequest = await axios.get(`https://beta.cowsoko.com/api/v1/blogs/${blogId}`
      )
      const {blogIds} = state
      const blogIdsLength = blogIds.length
      state.buttonIsDisabled = false
      // Check if blog id is the last id in the blog id list
      if (blogIds[blogIdsLength - 1] === blogId) {
        state.buttonIsDisabled = true
      }
      commit('setBlog', singleBlogRequest.data.blog)
      dispatch('getBlogLikes')
      if (singleBlogRequest.data.success) {
        const commentsRequest = await axios({
          url: `https://beta.cowsoko.com/api/v1/blogs/${blogId}/comments`,
          method: 'get',
          headers: {
            authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        if (Object.keys(commentsRequest.data).length > 0) {
          commit('setComments', commentsRequest.data.data.comments)
        } else {
          commit('setComments', [])
        }
      }
    },
    editCommentAction ({commit}, {commentId, comment}) {
      const userId = localStorage.getItem('userId')
      axios({
        url: `https://beta.cowsoko.com/api/v1/comments/${commentId}`,
        method: 'put',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {comment, user_id: userId}
      })
        .then(results => {
          commit('editComment', results.data.comment.comment)
        })
        .catch(results => {
          if (results) {
            commit('setError')
          }
        })
    },
    postComment ({commit, state}, comment) {
      axios({
        url: `https://beta.cowsoko.com/api/v1/blogs/${state.blogId}/comments`,
        method: 'post',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {comment: comment, user_id: localStorage.getItem('userId')}
      })
        .then(results => {
          const {comment} = results.data
          Object.assign(comment, {replies: []})
          commit('updateComments', results.data.comment)
        })
        .catch(results => {
          commit('setError')
        })
    },
    postReply ({commit, state}, reply) {
      axios({
        url: `https://beta.cowsoko.com/api/v1/comments/${state.comment_reply_id}/reply`,
        method: 'post',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {comment: reply, user_id: localStorage.getItem('userId')}
      })
        .then(results => {
          let newReply = {
            id: results.data.comment.id,
            reply: results.data.comment.comment,
            created_by: results.data.comment.created_by,
            likes: results.data.comment.likes,
            number_of_replies: results.data.comment.number_of_replies,
            updated_at: results.data.comment.updated_at,
            created_at: results.data.comment.created_at
          }
          commit('addReplies', newReply)
        })
        .catch(results => {
          commit('setError')
        })
    },
    likeSingleReply ({commit}, comId) {
      const userId = localStorage.getItem('userId')
      axios({
        url: `https://beta.cowsoko.com/api/v1/comments/${comId}/like`,
        method: 'put',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {user_id: userId}
      })
      commit('updateReplyLikes', {userId, replyId: comId})
    },
    unlikeSingleReply ({commit}, comId) {
      const userId = localStorage.getItem('userId')
      axios({
        url: `https://beta.cowsoko.com/api/v1/comments/${comId}/unlike`,
        method: 'put',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {user_id: userId}
      })
      commit('updateReplyUnLikes', {userId, replyId: comId})
    },
    getReplyLikes ({commit}, replyId) {
      axios({
        url: `https://beta.cowsoko.com/api/v1/comments/${replyId}/likes`,
        method: 'get',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(results => {
          commit('setUserIds', {replyId, userIds: results.data.users.filter(usr => usr.user).map(usr => usr.user.id)})
        })
    },
    getBlogLikes ({state, commit}) {
      axios({
        url: `https://beta.cowsoko.com/api/v1/blogs/${state.blogId}/likes`,
        method: 'get',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(results => {
          commit('setLikeBlogUserIds', results.data.users.filter(usr => usr.user).map(usr => usr.user.id))
        })
    },
    likeSingleComment ({commit}, commentId) {
      const userId = localStorage.getItem('userId')
      axios({
        url: `https://beta.cowsoko.com/api/v1/comments/${commentId}/like`,
        method: 'put',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {user_id: userId}
      })
        .then(result => {
          commit('likeSingleComment', result.data.comment)
        })
    },
    unLikeSingleComment ({commit}, commentId) {
      const userId = localStorage.getItem('userId')
      axios({
        url: `https://beta.cowsoko.com/api/v1/comments/${commentId}/unlike`,
        method: 'put',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {user_id: userId}
      })
        .then(result => {
          commit('unlikeSingleComment', result.data.comment)
        })
    },
    likeSingleBlog ({commit}, blogId) {
      const userId = localStorage.getItem('userId')
      axios({
        url: `https://beta.cowsoko.com/api/v1/blogs/${blogId}/like`,
        method: 'put',
        data: {user_id: userId},
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      commit('updateBlogLikes', userId)
    },
    unLikeSingleBlog ({commit}, blogId) {
      const userId = localStorage.getItem('userId')
      axios({
        url: `https://beta.cowsoko.com/api/v1/blogs/${blogId}/unlike`,
        method: 'put',
        data: {user_id: userId},
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      commit('removeUserIdFromBlogLikes', userId)
    },

    removeReply ({commit}, replyId) {
      commit('deleteReply', replyId)
      axios({
        url: `https://beta.cowsoko.com/api/v1/comments/${replyId}`,
        method: 'delete',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {user_id: localStorage.getItem('userId')}
      })
    },
    removeComment ({commit}, commentId) {
      commit('deleteComment', commentId)
      axios({
        url: `https://beta.cowsoko.com/api/v1/comments/${commentId}`,
        method: 'delete',
        headers: {
          authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {user_id: localStorage.getItem('userId')}
      })
    },
    getAllBlogs ({commit, state}, {pageNumber}) {
      state.loadingState = true
      return axios({
        url: `https://beta.cowsoko.com/api/v1/blogs?page=${pageNumber}`,
        method: 'get'
      })
        .then(results => {
          const { meta } = results.data
          commit('setBlogsPerPage', meta.per_page)
          const allBlogsIds = results.data.data.blogs.map(blog => blog.id)
          // Generate a list of unique blog ids
          const uniqueBlogIds = Array.from(new Set([...state.blogIds, ...allBlogsIds]))
          commit('setBlogIds', uniqueBlogIds)
        })
    }
  }
}
