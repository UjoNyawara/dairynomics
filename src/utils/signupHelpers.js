import router from '@/router/router'
export function signupHelper (accountType) {
  let accountData = null
  let path = 'register'

  if (accountType === 'I am an input seller') {
    const inputServices = JSON.parse(localStorage.getItem('InputSupplierInfo'))
    if (!inputServices) return router.push('/input-seller')

    /*  the specific path and accountData are being selected for an input seller for the axios call**/
    path = 'input_seller/register'
    accountData = {
      inputs_offered: JSON.stringify(inputServices.inputs_offered),
      target_animals: inputServices.target_animals,
      services_offered: inputServices.services_offered
    }
    localStorage.removeItem('InputSupplierInfo')
  }

  if (accountType === 'I am a service provider') {
    const serviceProvider = JSON.parse(localStorage.getItem('serviceProviderDetails'))
    if (!serviceProvider) return router.push('/service-provider')

    /*  the specific path and accountData are being selected for a service provider for the axios call**/
    path = 'experts/register'
    accountData = {
      national_id: serviceProvider.nationalId,
      expert_category: serviceProvider.expertIn,
      academic_qualification: serviceProvider.certification,
      institution: serviceProvider.schoolAttended,
      start_date: serviceProvider.dateDuration.from,
      finish_date: serviceProvider.dateDuration.to,
      skills: serviceProvider.skill,
      course_studied: serviceProvider.courseStudied,
      training_level: serviceProvider.certification
    }
    localStorage.removeItem('serviceProviderDetails')
  }
  return {path, accountData}
}
