const serviceProviderUpdateValidator = {
  clearError (errorsObject, errorProperty) {
    errorsObject[errorProperty] = ''
  },
  emptyFieldValidator (errorsObject, errorProperty, field, errorMessage) {
    if (!field) {
      errorsObject[errorProperty] = errorMessage
      return
    }
    this.clearError(errorsObject, errorProperty)
    return true
  }
}

export default serviceProviderUpdateValidator
