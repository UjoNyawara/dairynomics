import jwtDecode from 'jwt-decode'
import Vue from 'vue'

export const eventBus = new Vue()

export function authenticateUser () {
  const token = localStorage.getItem('token')
  return {
    isValid: !!token && Date.now() / 1000 < jwtDecode(token).exp,
    token
  }
}

export function decodeToken (token) {
  try {
    const decoded = jwtDecode(token)
    return decoded
  } catch (error) {
    return error
  }
}

export function getUserId () {
  const UserId = localStorage.getItem('userId')
  return UserId
}

export function alertError (message) {
  Vue.notify({
    group: 'warning-alert',
    type: 'warn',
    text: message
  })
}

export function openModal (payload, paymentId) {
  if (payload === 'receipt' || payload === 'invoice') {
    eventBus.$emit('showPaymentModal', payload, paymentId)
  } else {
    eventBus.$emit('showModal', payload)
  }
}
export function closeModal () {
  eventBus.$emit('closeModal')
}

export function alertTokenError () {
  alertError(
    'You are either not signed in or your token has expired. Please sign in and Try Again'
  )
}

export function alertConnectionError () {
  alertError(
    "Sorry, we couldn't make this connection for any of these reasons: you are not connected, or your connection ins't strong or our server is down"
  )
}

export function findCounty (countyList, countyName) {
  if (!countyName) return undefined
  const regex = /[^a-z]/gi
  const desiredCounty = countyName.replace(regex, '').toLowerCase()
  return countyList.find(
    countyObj =>
      desiredCounty === countyObj.county.replace(regex, '').toLowerCase()
  )
}

export function getAPIErrorMessage (error) {
  switch (error) {
    case 401:
      return 'You are either not signed in or your token has expired. Please sign in and Try Again'
    case 404:
      return "Sorry, We couldn't locate the requested information"
    default:
      return 'Sorry, Something went wrong while connecting to server, are you connected?'
  }
}
export function previewImage (file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    let fileImage = ''
    reader.readAsDataURL(file)
    reader.onload = () => {
      const dataUrl = reader.result
      fileImage = dataUrl
      resolve(fileImage)
    }
  })
}

export function removePreviewedImage (index, imageArray) {
  imageArray.splice(index, 1)
}
