/* eslint-disable no-trailing-spaces */
/* eslint-disable no-unused-vars */
import Peer from 'peerjs'

export default class PeerModel {
  /**
   * @description Creates an instance of PeerModel.
   * @memberof PeerModel
   */
  constructor () {
    this.connections = []
    this.IS_TYPING = 'isTyping'
  }

  /**
   * @description Listens for a closed/desconnection event for a peer connection instance
   *
   * @param {Object} connection the peer connection instance
   * @memberof PeerModel
   */
  addClosedConnectionListener (connection) {
    connection.on('close', () => {
      this.watchConnectionStatus(connection)
    })
  }

  /**
   * @description Adds data listeer to a connection
   *
   * @param {Peer} connection the peer connection instance
   * @memberof PeerModel
   */
  addDataListener (connection) {
    if (connection) {
      connection.on('data', payload => {
        const { senderId, message } = payload
        if (message) {
          this.receiveMessage(payload)
        } else {
          this.commit('signalMutator', payload)
        }
      })
    }
  }

  /** 
   * @description Closes peer connection 
   * 
   * @memberof PeerModel
   */
  closeConnection () {
    this.peer.destroy()
  }

  /**
   * @description Connects peers
   *
   * @param {String} peerId connection id
   * @memberof PeerModel
   */
  connectPeer (user) {
    let peerConnection
    const attemptConnection = setInterval(() => {
      peerConnection = this.peer.connect(user.peerId, { reliable: true })
      if (peerConnection) {
        clearInterval(attemptConnection)
        peerConnection.on('open', () => {
          this.updatePeerConnections(peerConnection)
          this.updateStatus(peerConnection, 'Online')
        })
      }
    }, 500)
  }

  /**
   * @description Creates a new Peer instance
   *
   * @param {Object} user The authenticated user
   * @memberof PeerModel
   */
  createPeerInstance (user) {
    return new Peer(this.getPeerId(user), {
      config: {
        'iceServers': [
          { urls: 'stun:stun.l.google.com:19302' },
          { urls: 'turn:numb.viagenie.ca:3478',
            credential: 'muazkh',
            username: 'webrtc@live.com'
          }
        ],
        iceTransportPolicy: 'all'
      }
    })
  }

  /**
   * @description Finds and return all connection instances  that matches 
   * the a particular user id 
   * 
   * @param {String} peerId the peerId
   * @memberof PeerModel
   */
  findConnections (peerId) {
    return this.connections.filter(conn => (conn.userId === this.getUserId(peerId)))
  }

  /**
   * @desciption Formats a date
   *  
   * @param {String} date the date to format
   * @memberof PeerModel
   */
  formatDate (date) {
    if (date) {
      const year = date.split(',')[1]
      const dates = date.split(',')[0].split(' ')
      return `${dates[2]} ${dates[1]} ${year}, ${dates[0].toLowerCase()}`
    }
    return 'Today'
  }

  /**
   * @description Checks if a user still has an active connection
   * 
   * @param {WebRTCConnecction} connection the connection instance
   * @returns {Boolean} 
   * @memberof PeerModel
   */
  foundActiveUser (connection) {
    return this.findConnections(connection.peer).length > 1
  }

  /** 
   * @description Creates a peer Id
   * 
   * @param {Object} user the user insatance
   * @memberof PeerModel
   */
  getPeerId (user) {
    return `${user.id}-${new Date().getTime()}`
  }

  /**
   * @description Gets a user Id from the peerId
   * 
   * @returns {String} the user Id
   * @memberof PeerModel
   */
  getUserId (peerId) {
    return `${peerId}`.split('-')[0]
  }

  /**
   * @description Opens connection for peers to connect
   *
   * @param {Object} user 
   * @param {Function} savePeerId callback function to save peer id
   * @memberof PeerModel
   */
  openConnection (user, savePeerId) {
    this.peer = this.createPeerInstance(user)
    if (this.peer) {
      this.peer.on('open', async (id) => {
        await savePeerId(id)

        this.peer.on('connection', connection => {
          this.updatePeerConnections(connection)
          this.updateStatus(connection, 'Online')
        })
      })
    }
  }

  /**
   * @description Processes received message
   *
   * @param {Object} payload the message payload
   * @memberof PeerModel
   */
  receiveMessage (payload) {
    if (Number(this.state.chatMate.id) === Number(payload.sender_id)) {
      if (this.state.messages.length > 1) {
        /**
         * We need to prevent incomig message from being committed multiple times
         * So, we compare the time stamp of the last committed message;
         * if they are the same, we won't commit the incomming message
         */
        const lastMessage = this.state.messages[this.state.messages.length - 1]
        if (lastMessage.stamp !== payload.stamp) {
          this.commit('addMessageMutator', payload)
        }
      } else {
        this.commit('addMessageMutator', payload)
      }
    }
    this.commit('unreadMessagesMutator', { ...payload, value: 1 })
  }

  /** 
   * @description Sends a signal
   * 
   * @param {Object} payload the signal payload
   * @memberof PeerModel
   */
  sendSignal (payload) {
    this.transport(payload)
  }

  /**
   * @description Sends a message to the connected peer
   *
   * @param {Object} payload
   * @memberof PeerModel
   */
  transport (payload) {
    const conns = this.findConnections(payload.peerId)
    if (conns.length > 0) {
      conns.forEach(({ connection }) => connection.send(payload))
    }
  }

  /**
   * @description Adds a new connection to the array of connections
   *
   * @param {WebRTCConnecction} connection the connection instance
   * @memberof PeerModel
   */
  updatePeerConnections (connection) {
    this.addDataListener(connection)
    this.addClosedConnectionListener(connection)
    this.connections.push({
      userId: this.getUserId(connection.peer),
      connection
    })
  }

  /**
   * @description Updates offline/oline status
   *
   * @param {WebRTCConnecction} connection the connection instance
   * @param {String} status the status
   * @memberof PeerModel
   */
  updateStatus (connection, status) {
    const users = [...this.state.users]
    // Find the user/peer whose connection needs to be updated
    // Then reset his status accordingly
    const { index } = users.find((user, i) => {
      if (user.id === this.getUserId(connection.peer)) {
        user.index = i
        return user
      }
    }) || {}

    if (users[index]) {
      users[index].status = status
      this.commit('updateUsersMutator', users)
    }
  }

  /**
   * @description Watch for a close connection
   * 
   * @param {WebRTCConnection} conn the connection instance
   */
  watchConnectionStatus (conn) {
    // Check if user has no active connection else where
    // User may have connected in more than one browser with the same credentials
    if (!this.foundActiveUser(conn)) {
      this.updateStatus(conn, 'Offline')
    }
    // Update connections array by filtering out the closed connection
    this.connections = this.connections.filter(({ 
      connection: { peer: peerId }
    }) => (conn.peer !== peerId))
  }
}
