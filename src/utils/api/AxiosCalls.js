import axios from 'axios'
import store from '@/store'
import { openModal } from '@/utils/helperFunctions'

export const baseUrl = 'https://beta.cowsoko.com'
const baseURL = 'https://beta.cowsoko.com/api/v1/'
const ecommerceBackendURL = 'https://dairynomics.herokuapp.com/api'

export const interceptor = axios.interceptors.request.use(
  config => {
    // Do something before request is sent
    const token = localStorage.getItem('token')
    config.headers.Authorization = `Bearer ${token}`
    config.headers.Accept = `application/json`
    return config
  },
  () => {}
)

axios.interceptors.response.use(
  response => {
    return response
  },
  error => {
    if (error.response.status === 401) {
      store.commit('setAuthError', 'Please Log in to continue!')
      openModal('login')
    }
    return Promise.reject(error)
  }
)
class AxiosCalls {
  static getHeaders = config => {
    return {
      headers: {
        Accept: 'application/json'
      },
      ...config
    }
  };
  static get (path, config = {}) {
    return new Promise((resolve, reject) => {
      return axios
        .get(`${baseUrl}/${path}`, this.getHeaders(config))
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }

  static put (path, config) {
    return axios
      .put(`${baseUrl}/${path}`, config)
      .then(response => {
        return response
      })
      .catch(error => {
        return error.response
      })
  }

  static async awaitGet (path) {
    try {
      const { data } = await axios(`${baseURL}${path}`)
      return data
    } catch ({ response }) {
      return response
        ? { error: response.status, message: response.data.message }
        : { error: 500, message: 'Connection Error' }
    }
  }

  static axiosPost (path, payload) {
    return new Promise((resolve, reject) => {
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
      return axios
        .post(`${baseURL}${path}`, payload, config)
        .then(response => {
          return resolve(response)
        })
        .catch(error => {
          let errorResponse = null
          if (error.response && error.response.status === 422) {
            errorResponse = {
              error: 422,
              message: error.response.data.message
            }
            return reject(errorResponse)
          }
          errorResponse = { error: 503, message: 'Service Unavailable' }
          return reject(errorResponse)
        })
    })
  }

  static axiosPatch (path, payload, config) {
    return new Promise((resolve, reject) => {
      return axios
        .patch(`${baseURL}${path}`, payload, config)
        .then(response => {
          return resolve(response)
        })
        .catch(error => {
          let errorResponse = null
          if (error.response && error.response.status === 422) {
            errorResponse = {
              error: 422,
              message: error.response.data.message
            }
            return reject(errorResponse)
          } else if (error.request) {
            errorResponse = { error: 503, message: 'Service Unavailable' }
            return reject(errorResponse)
          } else {
            errorResponse = { error: 500, message: 'Connection Error' }
            return reject(errorResponse)
          }
        })
    })
  }

  static delete (path, config) {
    return axios({
      method: 'delete',
      url: `${baseUrl}/${path}`,
      data: config
    })
      .then(response => {
        return response
      })
      .catch(error => {
        return error.response
      })
  }
  static async post (path, data, token) {
    const headerConfig = AxiosCalls.getHeaders()
    headerConfig.headers['Content-Type'] = 'application/json';
    try {
      const response = await axios.post(`${baseUrl}/${path}`, data, {
        ...headerConfig
      })
      return response
    } catch (error) {
      return error
    }
  }
  static async postTest (path, data, token) {
    try {
      const response = await axios.post(`${baseUrl}/${path}`, data, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      return response
    } catch (error) {
      return error
    }
  }

  static async getEcommerce (path) {
    return new Promise((resolve, reject) => {
      return axios
        .get(`${ecommerceBackendURL}/${path}`)
        .then(response => {
          resolve(response.data)
        })
        .catch(error => {
          reject(error.response.data)
        })
    })
  }

  static async postEcommerce (path, payload = {}) {
    return new Promise((resolve, reject) => {
      return axios
        .post(`${ecommerceBackendURL}/${path}`, payload)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}
export default AxiosCalls
