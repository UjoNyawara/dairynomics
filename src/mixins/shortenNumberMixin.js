const pow = Math.pow
const floor = Math.floor
const abs = Math.abs
const log = Math.log

const round = (n, precision) => {
  var prec = Math.pow(10, precision)
  return Math.round(n * prec) / prec
}

export default {
  methods: {
    shortenNumber (number) {
      const base = floor(log(abs(number)) / log(1000))
      const suffix = 'kmb'[base - 1]
      return suffix ? round(number / pow(1000, base), 2) + suffix : '' + number
    }
  }
}
