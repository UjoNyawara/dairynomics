import moment from 'moment'

export const amountAndDateFormatterMixin = {
  methods: {
    getAmount (amount = 0) {
      return `KES ${amount.toLocaleString()}`
    },
    getDate (date) {
      return moment(date).format('MMM Do, Y')
    }
  }
}
